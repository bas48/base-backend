# Introduction

**Root User**

| User ID  | Password  |
| ------------ | ------------ |
| root  | 1234  |

**Permission**
- The system compares and combines **user permission** and **role permission** of the user,
- User permission has **higher priority** than role permission,
- When request on API or resource, the system compares the combined user permission and the resource required permission,
- If the combined user permission cannot fulfill the requirement of the resource, the request is failed and the response is **permission denied**.

**Role**
- Role determines what resource can the user retrieves, it is determined by the role permissions

**Resouce**
- **API** : GET, POST, PATCH, DELETE, SOCKET, EVENT, etc.,
- **File** : css, ejs, image file,
- A certain resource can be assigned with permission(s), only allow the user with the permission can request that resource.

**Translation**
- The translation of **app**, **cms**, **web** and **backend** can be edited in translation management.

**Message**
- A message should be created before creating a new notification,
- **Handler** : what infomation should be inputed when creating the notification.

**Notification**
- Run **yarn job-queue** first,
- 4 types of notification: **SMS**, **Email**, **Inbox**, and **Push Notification**
- 6 types of target: **All**, **All User**, **All Guest**, **User**, **FCM Key**, and **Role**

# Set Up
**Install Docker:**
Download link: https://www.docker.com/get-started

**Install Redis:**
```bash
docker pull redis:latest
docker run -itd --name redis-test -p 6379:6379 redis
 ```

**Install MySQL**
```bash
docker pull mysql:5.7
docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
```

**Install MySQL Workbrench:**
Download link: https://dev.mysql.com/downloads/workbench/
1. Setup new connection
2. Set **Connection Name**
3. Change hostname to **localhost**, port number unchange
4. Username is **root**, and input the password with **Store in Keychain**
5. **Test Connection** of the database, click **ok** if the test is success

**Environment Setup:**
1. Copy **.env.default** file
2. Change the file name to **.env**
3. Check if the values of **DB_USER** and **DB_PASSWORD** are the same as localhost 
4. Run **yarn server** to check can the backend run successfully

**First Time Run Server**
```bash
yarn #Install library
yarn migrate:refresh #Setup database
yarn dev #Start server in development mode
 ```


# Command
|Command|Description|
| ------------ | ------------ |
|build|Build typescript as javascript in build folder|
|yarn|Install library|
|yarn start|Start server|
|yarn migrate|Update migration|
|yarn migrate:undo|Undo migration|
|yarn migrate:refresh|Refresh database|
|yarn dev:migrate|Update migration in development mode|
|yarn dev|Start server in development mode|
|yarn dev:job-queue|Start job-queue in development mode|

# Add Table
Reference: https://sequelize.org/master/
1. Add new model in **/server/database/models**
2. Add new migration in **/server/database/migration**
3. Define model interface in **/server/database/interface**

####  Model
Reference: https://sequelize.org/master/manual/model-instances.html
Same as database table definition in backend

# Add API
1. Add Controller in **/server/http/controllers**
2. Add API definition in **/server/http/routes**

#### Controller Function
**Example**
```javascript
import { HttpContext } from '@http/interface';

export default {
  controllerFunction: async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { firstName, lastName } = validatedData?.body;
      ctx.body = { fullName: `${firstName} ${lastName}` };
    },
};
```

#### Context (ctx)
Most important parameter

|Key|Description|
| ------------ | ------------ |
|languageId |Request language|
|translate|Translation handler|
|models|Datebase model for acces database data|
|currentSession|Request session|
|currentUser|Request user|

#### Api Validator
Reference: https://joi.dev/api/

Use for validate request data structure, such as **body**, **params**, **query** or **cookie**

**Example**
```javascript
import Joi from 'joi';
import compose from 'koa-compose';
import apiValidator from '@http/middleware/api-validator';
import { HttpContext } from '@http/interface';

export default {
  controllerFunction: compose([
    apiValidator.body({
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { firstName, lastName } = validatedData?.body;
      ctx.body = { fullName: `${firstName} ${lastName}` };
    },
  ]),
};
```
|Key|Description|
| ------------ | ------------ |
|compose |Combine middlewares|
|validatedData |Will be inserted to **ctx**(context), if previous middleware is **apiValidator**|

# Add Job Queue
Reference: https://github.com/OptimalBits/bull/blob/develop/REFERENCE.md

Add new job in **/job-queue/jobs**

# Upload File
```bash
{
  method: 'post',
  path: '/file/temporary',
  controllerFunc: FileController.uploadTemporary,
},
```
1. Call this API to upload the image to **upload/temporary** directory,
2. The API returns the **path of the uploaded image** as response,

```bash
async transferFiles(ctx: Context, filePath: string, targetDir: string, action: 'move' | 'copy' = 'move', sizes ?: ResizeOption[]): Promise<string> { ... }
```

1. Use **transferFiles()** to move the file from temporary to desired directory

# Pagination
Common action provided a function **getList** to handle data access from different tables and provide the pagination function.
Common action can be loaded from **@actions/common**.

**getList** parameters:

|Name|Type|Attribute|Description|
|------------|------------|------------| ------------|
|main|object|required|Table setting|
|sorts|object|optional|Define sortable table columns|
|search|string[]|optional|Define searchable table columns|
|filter|object|optional|Define table column filter|
|withCount|boolean|optional, default: true|Set counting result or not|
|extra|object|optional|Extra query parameter setting|
|transaction|object|optional|Transaction to run query under|
|afterQuery|function|optional|Function to run after finished the get list function|
|exportSetting|object|optional|Export setting to define and enable the export function|

##Main table setting (parameter: main)
**main** parameters:

|Name|Type|Attribute|Description|
|------------|------------|------------| ------------|
|model|string|required|Table name|
|showDeletedEnable|boolean|optional|Get data with soft deleted record|
|as|string|optional|Redefine table name in current query|
|scope|string|optional|Apply scope setting defined in model|
|condition|object|optional|Get data with condition|
|joins|object[]|optional|Get record combine with other tables|
|withTranslation|object|optional|Load record with translation record|
|attributes|object|optional|Table attributes setting|

##Sub table setting (parameter: main/joins)
**join** parameters:

|Name|Type|Attribute|Description|
|------------|------------|------------| ------------|
|model|string|required|Table name|
|on|object|required|Define connection between upper table and current table|
|showDeletedEnable|boolean|optional|Get data with soft deleted record|
|as|string|optional|Redefine table name in current query|
|scope|string|optional|Apply scope setting defined in model|
|condition|object|optional|Get data with condition|
|joins|object[]|optional|Get record combine with other tables|
|required|boolean|optional|If set to **true** and no record found, the related record in other layout will be ignored, otherwise only the current record will be empty|
|withTranslation|object|optional|Load record with translation record|
|attributes|object|optional|Table attributes setting|
|association|object|optional|Define new sequelize association betweem upper and current tables if not defined in model|

**Example**
```javascript
import Joi from 'joi';
import commonAction from '@actions/common';

getAppList: commonAction.getList({
    main: {
      model: 'TableA',
      joins: [
        {
          model: 'TableB',
          on: {
            left: 'TableA.id',
            right: 'TableB.aId',
          },
        },
      ],
    },
    sorts: {
      colA1: 'TableA.col1',
      colA2: 'TableA.col2',
      colB1: 'TableB.col1',
      colB2: 'TableB.col2',
    },
    search: ['TableA.col1', 'TableB.col2'],
    filter: {
      key1: {
        validator: Joi.string(),
        mapper: 'TableA.col1',
      },
      key2: {
        validator: Joi.boolean(),
        mapper: 'TableB.col1',
      },
    },
  }),
```

### Condition

**condition** parameters:

|Name|Type|Attribute|Description|
|------------|------------|------------| ------------|
|query|string|required|SQL query string|
|getQueryValues|function|optional|Return the variable value in query string|

**Example**
```javascript
condition: {
  query: 'TableA.col1 = :var1 AND TableA.col2 = var2 AND TableA.col3 > 5',
  getQueryValues: async (ctx: HttpContext) => {
    // ... get var1, var2 value
    return { var1, var2 }; // return variable value
  },
},
```

### Translation

**withTranslation** parameters:

|Name|Type|Attribute|Description|
|------------|------------|------------| ------------|
|presetLanguage|boolean|optional|Whether to get selected language data only, or return all languages data|
|required|boolean|optional|If set to **true** and no record found, the related record in other layout will be ignored, otherwise only the current record will be empty|
|sourceKey|string|optional|The key defined in translation table's **reference** column|
|condition|object|optional|Get data with condition|

**Example**
```javascript
condition: {
  query: 'TableA.col1 = :var1 AND TableA.col2 = var2 AND TableA.col3 > 5',
  getQueryValues: async (ctx: HttpContext) => {
    // ... get var1, var2 value
    return { var1, var2 }; // return variable value
  },
},
```

### Attributes

Same as the parameter ***attributes*** of findAll function in **sequelize**

Reference: https://sequelize.org/master/class/lib/model.js~Model.html#static-method-findAll

### Association

Association provide an option to the models have not set association in the model definition.

**Example**
```javascript
association: (ctx: HttpContext) => {
  return TableA.hasMany(TableB,
    {
      foreignKey: 'TableACol1', sourceKey: 'TableBCol2',
    });
},
```

### Export Data

Export function will convert the data as **CSV** format and provide two different method to export the CSV file.

**exportSetting** parameters:

|Name|Type|Attribute|Description|
|------------|------------|------------| ------------|
|type|'download'/'email'|required|**download** means directly return the csv file to the client side/ **email** means send the csv file to selected email|
|fileName|string|optional|Filename as exported|
|sendTo|function|required|Function to get the email for sending the csv file, which is required when type set to **email**|
|optionId|string|optional|Select option in export option list, this is required when type is email|

To provide the export function in **getList**, optionId should be added to **export-option-list** in utils folder.

**Example**
```javascript
exportSetting: {
  type: 'email',
  sendTo: (ctx: HttpContext) => {
    const { currentUser } = ctx;
    return currentUser.email;
  },
},
```