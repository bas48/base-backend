FROM node:14

WORKDIR /usr/app

COPY package.json yarn.lock /usr/app/

RUN yarn

COPY . /usr/app

RUN yarn build

EXPOSE 4000
