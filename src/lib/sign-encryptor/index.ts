import jose from 'node-jose';
import { KeyIncorrect } from '@utils/errors';

class SignEncryptor {
  privateKey: string;

  constructor(_privateKey: string) {
    this.privateKey = _privateKey;
  }

  async signMessage(message: string) {
    const key = await jose.JWK.asKey(this.privateKey, 'pem', { kid: 'private' });
    const signedMessage: any = await jose.JWS.createSign({ format: 'compact' }, key).update(message).final();
    return signedMessage;
  }

  async verifyMessage(message: string, key: string) {
    try {
      const verifyKey = await jose.JWK.asKey(key, 'pem', { kid: 'verifyKey' });
      const verifiedMessage = await jose.JWS.createVerify(verifyKey).verify(message);
      return verifiedMessage.payload.toString();
    } catch (error) {
      console.log('Verify Message Error:', error);
      throw new KeyIncorrect();
    }
  }

  async encryptMessage(message: string, key: string) {
    const encryptKey = await jose.JWK.asKey(key, 'pem', { kid: 'encryptKey' });
    const encryptedMessage = await jose.JWE.createEncrypt({ format: 'compact' }, encryptKey).update(message).final();
    return encryptedMessage;
  }

  async decryptMessage(message: string) {
    try {
      const key = await jose.JWK.asKey(this.privateKey, 'pem', { kid: 'private' });
      const decryptedMessage = await jose.JWE.createDecrypt(key).decrypt(message);
      return decryptedMessage.payload.toString();
    } catch (error) {
      console.log('Decrypt Message Error:', error);
      throw new KeyIncorrect();
    }
  }

  async getEncodeMessage(message: string, targetKey: string) {
    const signedMessage = await this.signMessage(message);
    const encryptedMessage = await this.encryptMessage(signedMessage, targetKey);
    return encryptedMessage;
  }

  async getDecodeMessage(message: string, targetKey: string) {
    const decryptedMessage = await this.decryptMessage(message);
    const verifiedMessage = await this.verifyMessage(decryptedMessage, targetKey);
    return verifiedMessage;
  }
}

export default SignEncryptor;
