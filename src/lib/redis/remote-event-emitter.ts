import { EventEmitter } from 'events';
import RedisClient from '@lib/redis/connection';

class RemoteEventEmitter extends EventEmitter {
  subClient: RedisClient;

  pubClient: RedisClient;

  // eslint-disable-next-line no-unused-vars
  broadcast: (...arg: any[]) => any;

  // eslint-disable-next-line no-unused-vars
  transfer: (...arg: any[]) => any;

  constructor(redisChannel = 'events') {
    super();
    this.subClient = new RedisClient();
    this.pubClient = this.subClient.duplicate();
    this.subClient.subscribe(redisChannel);
    this.transfer = this.emit;
    this.broadcast = async (channel, message) => {
      if (channel === redisChannel) {
        try {
          message = JSON.parse(message);
          try {
            const { event, value } = message;
            this.transfer(event, value);
          } catch (e) {
            console.warn('error', e);
          }
        } catch (e) {
          console.warn('got invalid non json message');
        }
      }
    };
    this.emit = (event, value) => {
      this.pubClient.publish(redisChannel, JSON.stringify({
        event,
        value,
      }));
      return true;
    };
    this.subClient.on('message', this.broadcast);
  }

  dispose() {
    this.subClient.off('message', this.broadcast);
    this.subClient.disconnect();
    this.pubClient.disconnect();
  }
}

export default RemoteEventEmitter;
