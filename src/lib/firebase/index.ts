import admin from 'firebase-admin';
import serviceAccount = require('@lib/firebase/firebase-account-key.json');

const firebaseApp = admin.initializeApp({
  credential: admin.credential.cert({
    projectId: serviceAccount.project_id,
    clientEmail: serviceAccount.client_email,
    privateKey: serviceAccount.private_key,
  }),
});

export default firebaseApp;
