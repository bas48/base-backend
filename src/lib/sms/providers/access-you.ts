import axios from 'axios';
import parse, { Document } from 'xml-parser';
import SmsProvider from '@lib/sms/SmsProvider';

const { SMS_ACCESS_YOU_AC_NO, SMS_ACCESS_YOU_USER, SMS_ACCESS_YOU_PWD } = process.env;

const getCode = (result: Document) => {
  try { return result.root.children[0].children[0].content; } catch (e) { return 0; }
};
const getCodeDescription = (result: Document) => {
  try { return result.root.children[0].children[1].content; } catch (e) { return 'unknown'; }
};

class AccessYouFailError extends Error {
  code: string;

  status: number;

  response: any;

  constructor(response: any) {
    super(getCodeDescription(response));
    this.code = 'AccessYouFailError';
    this.status = 500;
    this.response = response;
  }
}

class AccessYouSmsProvider extends SmsProvider {
  async sendSms(phone: string, message: string) {
    phone = phone.replace(/ /g, ''); // remove space
    const res = await axios.get('https://api.accessyou.com/sms/sendsms-vercode.php?', {
      params: {
        accountno: SMS_ACCESS_YOU_AC_NO,
        user: SMS_ACCESS_YOU_USER,
        pwd: SMS_ACCESS_YOU_PWD,
        msg: message,
        phone: phone[0] === '+' ? phone.substr(1) : phone,
      },
    });
    const resultData = parse(res.data);
    const code = getCode(resultData);
    if (code !== '100') {
      throw new AccessYouFailError(resultData);
    }
    return resultData;
  }
}

export default AccessYouSmsProvider;
