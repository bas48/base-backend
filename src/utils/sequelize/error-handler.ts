import Sequelize from 'sequelize';
import { InfoAlreadyUsed, NotFoundError } from '@utils/errors';
import { Context } from '@utils/interface';

const sequelizeErrorHandler = (ctx: Context, error: any) => {
  let newError = error;
  if (error instanceof Sequelize.ValidationError) {
    for (const errorContent of error.errors) {
      const { translate } = ctx;
      const { path, type } = errorContent;
      switch (type) {
        case 'unique violation':
          newError = new InfoAlreadyUsed({ info: translate(path) });
          break;
        default:
          break;
      }
    }
  }
  if (error instanceof Sequelize.DatabaseError) {
    if (error instanceof Sequelize.ForeignKeyConstraintError) {
      const { translate } = ctx;
      const { table } = error;
      newError = new NotFoundError({ resource: translate(table) });
    }
  }
  return newError;
};

export default sequelizeErrorHandler;
