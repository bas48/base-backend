import { Op } from 'sequelize';
import fs from 'fs';
import path from 'path';
import models from '@database/manager/load-models';
import sequelize from '@database';

const searchAllFile = (sourcePath: string) => {
  const truePath = path.resolve(__dirname, `../../..${sourcePath}`);
  let filePaths: string[] = [];
  if (fs.existsSync(truePath)) {
    const contents = fs.readdirSync(truePath);
    for (const content of contents) {
      if (content !== 'temporary') {
        const contentPath = `${sourcePath}/${content}`;
        const trueContentPath = path.resolve(__dirname, `../../..${contentPath}`);
        const stats = fs.lstatSync(trueContentPath);
        if (stats.isDirectory()) {
          const subFilePaths = searchAllFile(contentPath);
          filePaths = [...filePaths, ...subFilePaths];
        } else if (stats.isFile()) {
          filePaths.push(contentPath);
        }
      }
    }
  }
  return filePaths;
};

const getExistFile = () => {
  const resourcePath = '/resources';
  const filePaths = searchAllFile(resourcePath);
  const resourceList = [];
  for (const filePath of filePaths) {
    resourceList.push({
      path: filePath,
      method: 'GET',
      type: 'file',
    });
  }
  return resourceList;
};

const updateResourceList = async (apiList: any[]) => {
  let resourceList = [...apiList.map((api) => ({
    ...api,
    method: api.method.toUpperCase(),
    type: 'api',
  }))];
  const fileList = getExistFile();
  resourceList = [...resourceList, ...fileList];
  // Get current resource list
  await sequelize.transaction(async (transaction) => {
    const { Resource } = models;
    const existResources = await Resource.findAll({ transaction });
    // Update exist resource status
    const activeResources = [];
    const inactiveResources = [];
    for (const existResource of existResources) {
      let found = false;
      for (let i = 0; i < resourceList.length; i += 1) {
        const { method } = resourceList[i];
        if (resourceList[i].path === existResource.path && method === existResource.method) {
          found = true;
          break;
        }
      }
      if (!existResource.active && found) {
        activeResources.push(existResource.id);
      } else if (existResource.active && !found) {
        inactiveResources.push(existResource.id);
      }
    }
    if (activeResources.length) {
      await Resource.update(
        {
          active: true,
        }, {
          where: {
            id: {
              [Op.in]: activeResources,
            },
          },
          transaction,
        },
      );
    }
    if (inactiveResources.length) {
      await Resource.update(
        {
          active: false,
        }, {
          where: {
            id: {
              [Op.in]: inactiveResources,
            },
          },
          transaction,
        },
      );
    }

    // Insert New Resource
    for (const existResource of existResources) {
      for (let i = 0; i < resourceList.length; i += 1) {
        const { method } = resourceList[i];
        if (resourceList[i].path === existResource.path && method === existResource.method) {
          resourceList.splice(i, 1);
          i -= 1;
        }
      }
    }
    if (resourceList.length) {
      await Resource.bulkCreate(resourceList, { transaction });
    }
  });
};

export default updateResourceList;
