import path from 'path';
import translationAction from '@actions/translation';
import i18next from 'i18next';
import fileAction from '@actions/file';
import { convertTrueObjToObj } from '@utils/object-convertor';

const localResourcePath = path.join(__dirname, 'resources');
const localFiles = fileAction.loadFolderFiles(localResourcePath);

const initTranslationSource = async () => {
  await translationAction.setDefaultTranslations('backend', localFiles);
};

const getTranslationSource = async (languageId?: string) => {
  const defaultTranslations: any = await translationAction.getDefaultTranslations(['backend']);
  const dbSources = await translationAction.getDBSource(['backend']);
  const translationSource: any = convertTrueObjToObj({ ...defaultTranslations.backend, ...dbSources.backend });
  if (languageId) {
    return translationSource[languageId];
  }
  return translationSource;
};

const initI18n = (language: string, resources: any) => {
  const i18n = i18next.createInstance();
  i18n.init({
    lng: language,
    debug: false,
    resources,
  }, (err) => {
    if (err) {
      console.log(err);
    }
  });
  const translate = (key: string, option ?: any) => (i18n.t(key, {
    defaultValue: i18n.t(key, { lng: process.env.DEFAULT_LANGUAGE, ...option }),
    ...option,
  }));
  return translate;
};

export {
  initTranslationSource,
  getTranslationSource,
  initI18n,
};
