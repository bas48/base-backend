export default {
  translation: {
    company: 'Base Project',
    project_name: 'Base Project',
    resource: 'Resource',
    resource_path: 'Resource Path',
    file: 'File',
    verification_subject: 'Verification',
    send_verification_code: 'Your verification code is {{code}}.',
    session: 'Session',
    two_factor: 'Two Factor',
    not_verified: 'Not verified',
    not_related_user: 'Not related user',
    not_related_verification: 'Not related verification',
    copyright: 'Copyright © {{year}} {{company}} All right reserved.',
    handler: 'Handler',
    user: 'User',
    verification: 'Verification',
    constant: 'Constant',
    message: 'Message',
    query: 'Query',
    notification: 'Notification',
    export_email_title: 'Export Data',
    export_email_content: 'Exported data has been attached for your review',
    event: 'Event',
    sms: 'SMS',
    email: 'Email',
    phone: 'Phone',
  },
  handler: {
    message: {},
  },
  error: {
    BaseError: 'Base error',
    BadRequestError: 'Bad request',
    ConflictError: 'Conflict error',
    ForbiddenError: 'Forbidden error',
    InternalServerError: 'Internal server error',
    NotFoundError: '{{resource}} not found',
    UnauthorizedError: 'Unauthorized error',
    // Customize Error
    VerificationCodeAlreadyVerified: 'Verification code already verified',
    InfoAlreadyUsed: '{{info}} already used',
    InvalidFileType: 'Invalid file type {{type}}',
    InvalidValidation: 'Invalid validation',
    IncorrectPassword: 'Incorrect password',
    VerificationCodeExpired: 'Verification code expired',
    ResourceAlreadyExists: '{{resource}} already exists',
    NotImplemented: 'Not implemented',
    PermissionDenied: 'Permission denied',
    AccessTokenExpired: 'Access token expired',
    TwoFactorRequired: 'Two factor token is required',
    IncorrectToken: 'Incorrect Token',
    VerificationRequired: 'Verification is required',
    JobAlreadyExecuted: 'Job {{jobId}} already executed',
    FileCannotRemove: 'Resource file cannot remove: {{path}}',
    SocialLoginError: 'Social login error',
    FileTransferError: 'File transfer error',
    NotificationAlreadyInProcess: 'Notification already in process',
    PrivateFileProtected: 'Private file is protected',
    UserAlreadyBanned: 'User already banned',
    LoginRequired: 'Need to login',
    FileUploadError: 'File upload error: {{message}}',
    ExportNotSupport: 'Export not support',
    VerificationTokenExpired: 'Verification token expired',
    TooManyRequestError: 'Too many request error',
    VerificationCodeIncorrect: 'Verification code incorrect',
    RequestTooMuch: '{{type}} request too much',
    // extra error message
    user_without_any_permission: 'User without any permission',
    system_message_protect: 'Cannot delete system message',
  },
  joi_errors: {
    alternatives:
    {
      all: '"{{#label}}" does not match all of the required types',
      any: '"{{#label}}" does not match any of the allowed types',
      match: '"{{#label}}" does not match any of the allowed types',
      one: '"{{#label}}" matches more than one allowed type',
      types: '"{{#label}}" must be one of {{#types}}',
    },
    any:
    {
      custom:
        '"{{#label}}" failed custom validation because {{#error.message}}',
      default: '"{{#label}}" threw an error when running default method',
      failover: '"{{#label}}" threw an error when running failover method',
      invalid: '"{{#label}}" contains an invalid value',
      only:
        '"{{#label}}" must be {if(#valids.length == 1, "", "one of ")}{{#valids}}',
      ref:
        '"{{#label}}" {{#arg}} references "{{#ref}}" which {{#reason}}',
      required: '"{{#label}}" is required',
      unknown: '"{{#label}}" is not allowed',
    },
    array:
    {
      base: '"{{#label}}" must be an array',
      excludes: '"{{#label}}" contains an excluded value',
      hasKnown:
        '"{{#label}}" does not contain at least one required match for type "{#patternLabel}"',
      hasUnknown: '"{{#label}}" does not contain at least one required match',
      includes: '"{{#label}}" does not match any of the allowed types',
      includesRequiredBoth:
        '"{{#label}}" does not contain {{#knownMisses}} and {{#unknownMisses}} other required value(s)',
      includesRequiredKnowns: '"{{#label}}" does not contain {{#knownMisses}}',
      includesRequiredUnknowns:
        '"{{#label}}" does not contain {{#unknownMisses}} required value(s)',
      length: '"{{#label}}" must contain {{#limit}} items',
      max:
        '"{{#label}}" must contain less than or equal to {{#limit}} items',
      min: '"{{#label}}" must contain at least {{#limit}} items',
      orderedLength: '"{{#label}}" must contain at most {{#limit}} items',
      sort: '"{{#label}}" must be sorted in {#order} order by {{#by}}',
      sparse: '"{{#label}}" must not be a sparse array item',
      unique: '"{{#label}}" contains a duplicate value',
    },
    binary:
    {
      base: '"{{#label}}" must be a buffer or a string',
      length: '"{{#label}}" must be {{#limit}} bytes',
      max:
        '"{{#label}}" must be less than or equal to {{#limit}} bytes',
      min: '"{{#label}}" must be at least {{#limit}} bytes',
    },
    boolean: { base: '"{{#label}}" must be a boolean' },
    date:
    {
      base: '"{{#label}}" must be a valid date',
      format:
        '"{{#label}}" must be in {msg("date.format." + #format) || #format} format',
      greater: '"{{#label}}" must be greater than "{{#limit}}"',
      less: '"{{#label}}" must be less than "{{#limit}}"',
      max: '"{{#label}}" must be less than or equal to "{{#limit}}"',
      min: '"{{#label}}" must be larger than or equal to "{{#limit}}"',
    },
    function:
    {
      arity: '"{{#label}}" must have an arity of {{#n}}',
      class: '"{{#label}}" must be a class',
      maxArity: '"{{#label}}" must have an arity lesser or equal to {{#n}}',
      minArity: '"{{#label}}" must have an arity greater or equal to {{#n}}',
    },
    object:
    {
      and:
        '"{{#label}}" contains {{#presentWithLabels}} without its required peers {{#missingWithLabels}}',
      assert:
        '"{{#label}}" is invalid because {if(#subject.key, `"` + #subject.key + `" failed to ` + (#message || "pass the assertion test"), #message || "the assertion failed")}',
      base: '"{{#label}}" must be of type {{#type}}',
      instance: '"{{#label}}" must be an instance of "{{#type}}"',
      length:
        '"{{#label}}" must have {{#limit}} key{if(#limit == 1, "", "s")}',
      max:
        '"{{#label}}" must have less than or equal to {{#limit}} key{if(#limit == 1, "", "s")}',
      min:
        '"{{#label}}" must have at least {{#limit}} key{if(#limit == 1, "", "s")}',
      missing:
        '"{{#label}}" must contain at least one of {{#peersWithLabels}}',
      nand:
        '"{{#mainWithLabel}}" must not exist simultaneously with {{#peersWithLabels}}',
      oxor:
        '"{{#label}}" contains a conflict between optional exclusive peers {{#peersWithLabels}}',
      pattern:
        { match: '"{{#label}}" keys failed to match pattern requirements' },
      refType: '"{{#label}}" must be a Joi reference',
      rename:
      {
        multiple:
          '"{{#label}}" cannot rename "{{#from}}" because multiple renames are disabled and another key was already renamed to "{{#to}}"',
        override:
          '"{{#label}}" cannot rename "{{#from}}" because override is disabled and target "{{#to}}" exists',
      },
      schema: '"{{#label}}" must be a Joi schema of {{#type}} type',
      unknown: '"{{#label}}" is not allowed',
      with:
        '"{{#mainWithLabel}}" missing required peer "{{#peerWithLabel}}"',
      without:
        '"{{#mainWithLabel}}" conflict with forbidden peer "{{#peerWithLabel}}"',
      xor:
        '"{{#label}}" contains a conflict between exclusive peers {{#peersWithLabels}}',
    },
    number:
    {
      base: '"{{#label}}" must be a number',
      greater: '"{{#label}}" must be greater than {{#limit}}',
      infinity: '"{{#label}}" cannot be infinity',
      integer: '"{{#label}}" must be an integer',
      less: '"{{#label}}" must be less than {{#limit}}',
      max: '"{{#label}}" must be less than or equal to {{#limit}}',
      min: '"{{#label}}" must be larger than or equal to {{#limit}}',
      multiple: '"{{#label}}" must be a multiple of {{#multiple}}',
      negative: '"{{#label}}" must be a negative number',
      port: '"{{#label}}" must be a valid port',
      positive: '"{{#label}}" must be a positive number',
      precision:
        '"{{#label}}" must have no more than {{#limit}} decimal places',
      unsafe: '"{{#label}}" must be a safe number',
    },
    string:
    {
      alphanum: '"{{#label}}" must only contain alpha-numeric characters',
      base: '"{{#label}}" must be a string',
      base64: '"{{#label}}" must be a valid base64 string',
      creditCard: '"{{#label}}" must be a credit card',
      dataUri: '"{{#label}}" must be a valid dataUri string',
      domain: '"{{#label}}" must contain a valid domain name',
      email: '"{{#label}}" must be a valid email',
      empty: '"{{#label}}" is not allowed to be empty',
      guid: '"{{#label}}" must be a valid GUID',
      hex: '"{{#label}}" must only contain hexadecimal characters',
      hexAlign:
        '"{{#label}}" hex decoded representation must be byte aligned',
      hostname: '"{{#label}}" must be a valid hostname',
      ip:
        '"{{#label}}" must be a valid ip address with a {{#cidr}} CIDR',
      ipVersion:
        '"{{#label}}" must be a valid ip address of one of the following versions {{#version}} with a {{#cidr}} CIDR',
      isoDate: '"{{#label}}" must be in iso format',
      isoDuration: '"{{#label}}" must be a valid ISO 8601 duration',
      length: '"{{#label}}" length must be {{#limit}} characters long',
      lowercase: '"{{#label}}" must only contain lowercase characters',
      max:
        '"{{#label}}" length must be less than or equal to {{#limit}} characters long',
      min:
        '"{{#label}}" length must be at least {{#limit}} characters long',
      normalize:
        '"{{#label}}" must be unicode normalized in the {{#form}} form',
      token:
        '"{{#label}}" must only contain alpha-numeric and underscore characters',
      pattern:
      {
        base:
          '"{{#label}}" with value "{[.]}" fails to match the required pattern: {{#regex}}',
        name:
          '"{{#label}}" with value "{[.]}" fails to match the {{#name}} pattern',
        invert: {
          base: '"{{#label}}" with value "{[.]}" matches the inverted pattern: {{#regex}}',
          name: '"{{#label}}" with value "{[.]}" matches the inverted {{#name}} pattern',
        },
      },
      trim: '"{{#label}}" must not have leading or trailing whitespace',
      uri: '"{{#label}}" must be a valid uri',
      uriCustomScheme:
        '"{{#label}}" must be a valid uri with a scheme matching the {{#scheme}} pattern',
      uriRelativeOnly: '"{{#label}}" must be a valid relative uri',
      uppercase: '"{{#label}}" must only contain uppercase characters',
    },
    symbol:
    {
      base: '"{{#label}}" must be a symbol',
      map: '"{{#label}}" must be one of {{#map}}',
    },
    other: {
      phone: 'Must be correct phone format',
    },
  },
};
