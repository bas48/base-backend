export default {
  translation: {
    company: '基础项目',
    project_name: '基础项目',
    resource: '资源',
    resource_path: '资源路径',
    file: '档案',
    verification_subject: '认证',
    send_verification_code: '你的验证码是{{code}}.',
    session: '认证',
    two_factor: '双重认证',
    not_verified: '尚未认证',
    not_related_user: '不相关的用户',
    not_related_verification: '不相关的认证',
    copyright: '版权由 © {{year}} {{company}}保留所有权利。',
    handler: '处理器',
    user: '用户',
    verification: '认证',
    constant: '常数',
    message: '讯息',
    query: '查询',
    notification: '通知',
    export_email_title: '导出资料',
    export_email_content: '导出资料已附加在电邮附件中',
    event: '事件',
    sms: '短讯',
    email: '电邮',
    phone: '电话',
  },
  handler: {
    message: {},
  },
  error: {
    BaseError: '错误',
    BadRequestError: '请求错误',
    ConflictError: '冲突错误',
    ForbiddenError: '禁止',
    InternalServerError: '内部服务器错误',
    NotFoundError: '找不到{{resource}}',
    UnauthorizedError: '未经授权',
    // Customize Error
    VerificationCodeAlreadyVerified: '验证码已被验证',
    InfoAlreadyUsed: '{{info}}已被使用',
    InvalidFileType: '错误的档案格式{{type}}',
    InvalidValidation: '无效验证',
    IncorrectPassword: '密码错误',
    VerificationCodeExpired: '验证码已过期',
    ResourceAlreadyExists: '{{resource}}已经存在',
    NotImplemented: '未实现',
    PermissionDenied: '没有权限',
    AccessTokenExpired: '认证已过期',
    TwoFactorRequired: '必须使用双重认证',
    IncorrectToken: '错误认证',
    VerificationRequired: '必须使用认证',
    JobAlreadyExecuted: '作业队列{{jobId}}已执行',
    FileCannotRemove: '资源档案已被删除{{path}}',
    SocialLoginError: '社交媒体登入错误',
    FileTransferError: '档案转移错误',
    NotificationAlreadyInProcess: '通知已准备推送',
    PrivateFileProtected: '私人档案已被保护',
    UserAlreadyBanned: '用户已被禁用',
    LoginRequired: '需要登入',
    FileUploadError: '档案上传错误：{{message}}',
    ExportNotSupport: '不支援资料导出',
    VerificationTokenExpired: '认证已过期',
    TooManyRequestError: '请求过多',
    VerificationCodeIncorrect: '认证码错误',
    RequestTooMuch: '{{type}}发送太频密',
    // extra error message
    user_without_any_permission: '用户没有相关权限',
    system_message_protect: '系统讯息不能删除',
  },
  joi_errors: {
    alternatives:
    {
      all: '"{{#label}}"不符合所有需要的种类',
      any: '"{{#label}}"不符合任何允许的种类',
      match: '"{{#label}}"不符合任何允许的种类',
      one: '"{{#label}}"符合多过一个种类',
      types: '"{{#label}}"必须是{{#types}}',
    },
    any:
    {
      custom:
        '由于{{#error.message}}，"{{#label}}"自定验证失败',
      default: '运行默认时"{{#label}}"抛出异常',
      failover: '运行故障转移时"{{#label}}"抛出异常',
      invalid: '"{{#label}}"包含无效值',
      only:
        '"{{#label}}"必须是{{#valids}}{if(#valids.length == 1, "", "其中一个")}',
      ref:
        '"{{#label}}"{{#arg}}参考"{{#ref}}"，{{#reason}}',
      required: '"{{#label}}"是必须的',
      unknown: '"{{#label}}"不被允许',
    },
    array:
    {
      base: '"{{#label}}"必须是阵列(Array)',
      excludes: '"{{#label}}"包含不接纳的值',
      hasKnown:
        '"{{#label}}"不包含至少一个需要种类"{#patternLabel}"',
      hasUnknown: '"{{#label}}"不包含至少一个需要的配对',
      includes: '"{{#label}}"不符合任何允许的种类',
      includesRequiredBoth:
        '"{{#label}}"不包含{{#knownMisses}}及{{#unknownMisses}}其他需要的值',
      includesRequiredKnowns: '"{{#label}}"不包含{{#knownMisses}}',
      includesRequiredUnknowns:
        '"{{#label}}"不包含{{#unknownMisses}}需要的值',
      length: '"{{#label}}"必须包含{{#limit}}个物件',
      max:
        '"{{#label}}"必须包含少于或等于{{#limit}}个物件',
      min: '"{{#label}}"必须包含最少{{#limit}}个物件',
      orderedLength: '"{{#label}}"必须包含最多{{#limit}}个物件',
      sort: '"{{#label}}"必须按{#order}顺序及按{{#by}}排序',
      sparse: '"{{#label}}"不可以是稀疏矩阵物件',
      unique: '"{{#label}}"包含重复的值',
    },
    binary:
    {
      base: '"{{#label}}"必须是缓冲区或字符',
      length: '"{{#label}}"必须{{#limit}}字节',
      max:
        '"{{#label}}"必须少于或等于{{#limit}}个字节',
      min: '"{{#label}}"必须最少{{#limit}}个字节',
    },
    boolean: { base: '"{{#label}}"必须是布林值(Boolean)' },
    date:
    {
      base: '"{{#label}}"必须是有效日期',
      format:
        '"{{#label}}"必须是{msg("date.format." + #format) || #format}格式',
      greater: '"{{#label}}"必须大于"{{#limit}}"',
      less: '"{{#label}}"必须少"{{#limit}}"',
      max: '"{{#label}}"必须少于或等于"{{#limit}}"',
      min: '"{{#label}}"必须大于或等于"{{#limit}}"',
    },
    function:
    {
      arity: '"{{#label}}"的元数(Arity)必须是{{#n}}',
      class: '"{{#label}}"必须是类别(Class)',
      maxArity: '"{{#label}}"的元数(Arity)必须是少于或等于{{#n}}',
      minArity: '"{{#label}}"的元数(Arity)必须是大于或等于{{#n}}',
    },
    object:
    {
      and:
        '"{{#label}}"包含{{#presentWithLabels}}没有其所需的同行{{#missingWithLabels}}',
      assert:
        '"{{#label}}"是无效值 is invalid 因为because {if(#subject.key, `"` + #subject.key + `"无法通过` + (#message || "断言测试"), #message || "断言测试失败")}',
      base: '"{{#label}}"必须是{{#type}}类型',
      instance: '"{{#label}}"必须是一个实例"{{#type}}"',
      length:
        '"{{#label}}"必须有{{#limit}} 键{if(#limit == 1, "", "s")}',
      max:
        '"{{#label}}"必须小于或等于 {#limit}}键{if(#limit == 1, "", "s")}',
      min:
        '"{{#label}}"必须至少有{{#limit}}键{if(#limit == 1, "", "s")}',
      missing:
        '"{{#label}}"必须至少包含{{#peersWithLabels}}之一',
      nand:
        '"{{#mainWithLabel}}"不能与{{#peersWithLabels}}同时存在',
      oxor:
        '“{{#label}}”包含可选独占对等体之间的冲突{{#peersWithLabels}}',
      pattern:
        { match: '“{{#label}}”键无法匹配模式要求' },
      refType: '"{{#label}}"必须是Joi引用',
      rename:
      {
        multiple:
          '“{{#label}}”无法重命名“{{#from}}”，因为多次重命名被禁用并且另一个键已经重命名为“{{#to}}”',
        override:
          '“{{#label}}”无法重命名“{{#from}}”，因为复盖已禁用且目标“{{#to}}”存在',
      },
      schema: '"{{#label}}"必须是{{#type}}类型的Joi模式',
      unknown: '不允许使用“{{#label}}”',
      with:
        '“{{#mainWithLabel}}”缺少必需的对等方“{{#peerWithLabel}}”',
      without:
        '“{{#mainWithLabel}}”与被禁止的节点“{{#peerWithLabel}}”冲突',
      xor:
        '“{{#label}}”包含独占对等体之间的冲突{{#peersWithLabels}}',
    },
    number:
    {
      base: '“{{#label}}”必须是数字',
      greater: '“{{#label}}”必须大于{{#limit}}',
      infinity: '“{{#label}}”不能是无穷大',
      integer: '“{{#label}}”必须是整数',
      less: '“{{#label}}”必须小于{{#limit}}',
      max: '“{{#label}}”必须小于或等于{{#limit}}',
      min: '“{{#label}}”必须大于或等于{{#limit}}',
      multiple: '"{{#label}}"必须是{{#multiple}}的倍数',
      negative: '"{{#label}}"必须是负数',
      port: '"{{#label}}"必须是有效的端口',
      positive: '"{{#label}}"必须是正数',
      precision:
        '"{{#label}}"小数位数不得超过{{#limit}}',
      unsafe: '"{{#label}}"必须是安全号码',
    },
    string:
    {
      alphanum: '"{{#label}}"只能包含字母数字字符',
      base: '"{{#label}}"必须是字符串',
      base64: '"{{#label}}"必须是有效的base64字符串',
      creditCard: '"{{#label}}"必须是信用卡',
      dataUri: '"{{#label}}"必须是有效的资料URI字符串',
      domain: '"{{#label}}"必须包含有效的域名',
      email: '"{{#label}}"必须是有效的电子邮件',
      empty: '"{{#label}}"不允许为空',
      guid: '"{{#label}}"必须是有效的GUID',
      hex: '"{{#label}}"只能包含十六进製字符',
      hexAlign:
        '"{{#label}}"十六进制解码表示必须字节对齐',
      hostname: '"{{#label}}"必须是有效的主机名',
      ip:
        '"{{#label}}"必须是一个有效的IP地址{{#cidr}} CIDR',
      ipVersion:
        '"{{#label}}"必须是以下版本之一的有效 IP 地址{{#version}}和{{#cidr}} CIDR',
      isoDate: '"{{#label}}"必须是iso格式',
      isoDuration: '"{{#label}}"必须是有效的 ISO 8601 持续时间',
      length: '"{{#label}}"长度必须是{{#limit}}长字符',
      lowercase: '"{{#label}}"只能包含小写字符',
      max:
        '"{{#label}}"长度必须小于或等于{{#limit}}长字符',
      min:
        '"{{#label}}"长度最小为{{#limit}}长字符',
      normalize:
        '"{{#label}}"必须在{{#form}}表单',
      token:
        '"{{#label}}"只能包含字母数字和下划线字符',
      pattern:
      {
        base:
          '"{{#label}}"及定值"{[.]}"不符合必须的格式：{{#regex}}',
        name:
          '"{{#label}}"及定值"{[.]}"不符合{{#name}}格式',
        invert: {
          base: '"{{#label}}"及定值"{[.]}"符合倒转的格式：{{#regex}}',
          name: '"{{#label}}"及定值"{[.]}"符合倒转的{{#name}}格式',
        },
      },
      trim: '"{{#label}}"不得有前导或尾随空格',
      uri: '"{{#label}}"必须为正确的识别码',
      uriCustomScheme:
        '"{{#label}}"必须是相对识别码并匹配的有效的{{#scheme}}格式',
      uriRelativeOnly: '"{{#label}}"必须是有效的相对识别码',
      uppercase: '"{{#label}}"只能包含大写字符',
    },
    symbol:
    {
      base: '"{{#label}}"必须为标誌',
      map: '"{{#label}}"必须为{{#map}}中的一个',
    },
    other: {
      phone: '必须为正确的电话格式',
    },
  },
};
