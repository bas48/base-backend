export default {
  translation: {
    company: '基礎項目',
    project_name: '基礎項目',
    resource: '資源',
    resource_path: '資源路徑',
    file: '檔案',
    verification_subject: '認證',
    send_verification_code: '你的驗證碼是{{code}}.',
    session: '認證',
    two_factor: '雙重認證',
    not_verified: '尚未認證',
    not_related_user: '不相關的用戶',
    not_related_verification: '不相關的認證',
    copyright: '版權由 © {{year}} {{company}}保留所有權利。',
    handler: '處理器',
    user: '用戶',
    verification: '認證',
    constant: '常數',
    message: '訊息',
    query: '查詢',
    notification: '通知',
    export_email_title: '導出資料',
    export_email_content: '導出資料已附加在電郵附件中',
    event: '事件',
    sms: '短訊',
    email: '電郵',
    phone: '電話',
  },
  handler: {
    message: {},
  },
  error: {
    BaseError: '錯誤',
    BadRequestError: '請求錯誤',
    ConflictError: '衝突錯誤',
    ForbiddenError: '禁止',
    InternalServerError: '內部服務器錯誤',
    NotFoundError: '找不到{{resource}}',
    UnauthorizedError: '未經授權',
    // Customize Error
    VerificationCodeAlreadyVerified: '驗證碼已被驗證',
    InfoAlreadyUsed: '{{info}}已被使用',
    InvalidFileType: '錯誤的檔案格式{{type}}',
    InvalidValidation: '無效驗證',
    IncorrectPassword: '密碼錯誤',
    VerificationCodeExpired: '驗證碼已過期',
    ResourceAlreadyExists: '{{resource}}已經存在',
    NotImplemented: '未實現',
    PermissionDenied: '沒有權限',
    AccessTokenExpired: '認證已過期',
    TwoFactorRequired: '必須使用雙重認證',
    IncorrectToken: '錯誤認證',
    VerificationRequired: '必須使用認證',
    JobAlreadyExecuted: '作業隊列{{jobId}}已執行',
    FileCannotRemove: '資源檔案已被刪除{{path}}',
    SocialLoginError: '社交媒體登入錯誤',
    FileTransferError: '檔案轉移錯誤',
    NotificationAlreadyInProcess: '通知已準備推送',
    PrivateFileProtected: '私人檔案已被保護',
    UserAlreadyBanned: '用戶已被禁用',
    LoginRequired: '需要登入',
    FileUploadError: '檔案上傳錯誤：{{message}}',
    ExportNotSupport: '不支援資料導出',
    VerificationTokenExpired: '認證已過期',
    TooManyRequestError: '請求過多',
    VerificationCodeIncorrect: '認證碼錯誤',
    RequestTooMuch: '{{type}}發送太頻密',
    // extra error message
    user_without_any_permission: '用戶沒有相關權限',
    system_message_protect: '系統訊息不能刪除',
  },
  joi_errors: {
    alternatives:
    {
      all: '"{{#label}}"不符合所有需要的種類',
      any: '"{{#label}}"不符合任何允許的種類',
      match: '"{{#label}}"不符合任何允許的種類',
      one: '"{{#label}}"符合多過一個種類',
      types: '"{{#label}}"必須是{{#types}}',
    },
    any:
    {
      custom:
        '由於{{#error.message}}，"{{#label}}"自定驗證失敗',
      default: '運行默認時"{{#label}}"拋出異常',
      failover: '運行故障轉移時"{{#label}}"拋出異常',
      invalid: '"{{#label}}"包含無效值',
      only:
        '"{{#label}}"必須是{{#valids}}{if(#valids.length == 1, "", "其中一個")}',
      ref:
        '"{{#label}}"{{#arg}}參考"{{#ref}}"，{{#reason}}',
      required: '"{{#label}}"是必須的',
      unknown: '"{{#label}}"不被允許',
    },
    array:
    {
      base: '"{{#label}}"必須是陣列(Array)',
      excludes: '"{{#label}}"包含不接納的值',
      hasKnown:
        '"{{#label}}"不包含至少一個需要種類"{#patternLabel}"',
      hasUnknown: '"{{#label}}"不包含至少一個需要的配對',
      includes: '"{{#label}}"不符合任何允許的種類',
      includesRequiredBoth:
        '"{{#label}}"不包含{{#knownMisses}}及{{#unknownMisses}}其他需要的值',
      includesRequiredKnowns: '"{{#label}}"不包含{{#knownMisses}}',
      includesRequiredUnknowns:
        '"{{#label}}"不包含{{#unknownMisses}}需要的值',
      length: '"{{#label}}"必須包含{{#limit}}個物件',
      max:
        '"{{#label}}"必須包含少於或等於{{#limit}}個物件',
      min: '"{{#label}}"必須包含最少{{#limit}}個物件',
      orderedLength: '"{{#label}}"必須包含最多{{#limit}}個物件',
      sort: '"{{#label}}"必須按{#order}順序及按{{#by}}排序',
      sparse: '"{{#label}}"不可以是稀疏矩陣物件',
      unique: '"{{#label}}"包含重覆的值',
    },
    binary:
    {
      base: '"{{#label}}"必須是緩衝區或字符',
      length: '"{{#label}}"必須{{#limit}}字節',
      max:
        '"{{#label}}"必須少於或等於{{#limit}}個字節',
      min: '"{{#label}}"必須最少{{#limit}}個字節',
    },
    boolean: { base: '"{{#label}}"必須是布林值(Boolean)' },
    date:
    {
      base: '"{{#label}}"必須是有效日期',
      format:
        '"{{#label}}"必須是{msg("date.format." + #format) || #format}格式',
      greater: '"{{#label}}"必須大於"{{#limit}}"',
      less: '"{{#label}}"必須少"{{#limit}}"',
      max: '"{{#label}}"必須少於或等於"{{#limit}}"',
      min: '"{{#label}}"必須大於或等於"{{#limit}}"',
    },
    function:
    {
      arity: '"{{#label}}"的元數(Arity)必須是{{#n}}',
      class: '"{{#label}}"必須是類別(Class)',
      maxArity: '"{{#label}}"的元數(Arity)必須是少於或等於{{#n}}',
      minArity: '"{{#label}}"的元數(Arity)必須是大於或等於{{#n}}',
    },
    object:
    {
      and:
        '"{{#label}}"包含{{#presentWithLabels}}沒有其所需的同行{{#missingWithLabels}}',
      assert:
        '"{{#label}}"是無效值 is invalid 因為because {if(#subject.key, `"` + #subject.key + `"無法通過` + (#message || "斷言測試"), #message || "斷言測試失敗")}',
      base: '"{{#label}}"必須是{{#type}}類型',
      instance: '"{{#label}}"必須是一個實例"{{#type}}"',
      length:
        '"{{#label}}"必須有{{#limit}} 鍵{if(#limit == 1, "", "s")}',
      max:
        '"{{#label}}"必須小於或等於 {#limit}}鍵{if(#limit == 1, "", "s")}',
      min:
        '"{{#label}}"必須至少有{{#limit}}鍵{if(#limit == 1, "", "s")}',
      missing:
        '"{{#label}}"必須至少包含{{#peersWithLabels}}之一',
      nand:
        '"{{#mainWithLabel}}"不能與{{#peersWithLabels}}同時存在',
      oxor:
        '“{{#label}}”包含可選獨占對等體之間的衝突{{#peersWithLabels}}',
      pattern:
        { match: '“{{#label}}”鍵無法匹配模式要求' },
      refType: '"{{#label}}"必須是Joi引用',
      rename:
      {
        multiple:
          '“{{#label}}”無法重命名“{{#from}}”，因為多次重命名被禁用並且另一個鍵已經重命名為“{{#to}}”',
        override:
          '“{{#label}}”無法重命名“{{#from}}”，因為覆蓋已禁用且目標“{{#to}}”存在',
      },
      schema: '"{{#label}}"必須是{{#type}}類型的Joi模式',
      unknown: '不允許使用“{{#label}}”',
      with:
        '“{{#mainWithLabel}}”缺少必需的對等方“{{#peerWithLabel}}”',
      without:
        '“{{#mainWithLabel}}”與被禁止的節點“{{#peerWithLabel}}”衝突',
      xor:
        '“{{#label}}”包含獨占對等體之間的衝突{{#peersWithLabels}}',
    },
    number:
    {
      base: '“{{#label}}”必須是數字',
      greater: '“{{#label}}”必須大於{{#limit}}',
      infinity: '“{{#label}}”不能是無窮大',
      integer: '“{{#label}}”必須是整數',
      less: '“{{#label}}”必須小於{{#limit}}',
      max: '“{{#label}}”必須小於或等於{{#limit}}',
      min: '“{{#label}}”必須大於或等於{{#limit}}',
      multiple: '"{{#label}}"必須是{{#multiple}}的倍數',
      negative: '"{{#label}}"必須是負數',
      port: '"{{#label}}"必須是有效的端口',
      positive: '"{{#label}}"必須是正數',
      precision:
        '"{{#label}}"小數位數不得超過{{#limit}}',
      unsafe: '"{{#label}}"必須是安全號碼',
    },
    string:
    {
      alphanum: '"{{#label}}"只能包含字母數字字符',
      base: '"{{#label}}"必須是字符串',
      base64: '"{{#label}}"必須是有效的base64字符串',
      creditCard: '"{{#label}}"必須是信用卡',
      dataUri: '"{{#label}}"必須是有效的資料URI字符串',
      domain: '"{{#label}}"必須包含有效的域名',
      email: '"{{#label}}"必須是有效的電子郵件',
      empty: '"{{#label}}"不允許為空',
      guid: '"{{#label}}"必須是有效的GUID',
      hex: '"{{#label}}"只能包含十六進製字符',
      hexAlign:
        '"{{#label}}"十六進制解碼表示必須字節對齊',
      hostname: '"{{#label}}"必須是有效的主機名',
      ip:
        '"{{#label}}"必須是一個有效的IP地址{{#cidr}} CIDR',
      ipVersion:
        '"{{#label}}"必須是以下版本之一的有效 IP 地址{{#version}}和{{#cidr}} CIDR',
      isoDate: '"{{#label}}"必須是iso格式',
      isoDuration: '"{{#label}}"必須是有效的 ISO 8601 持續時間',
      length: '"{{#label}}"長度必須是{{#limit}}長字符',
      lowercase: '"{{#label}}"只能包含小寫字符',
      max:
        '"{{#label}}"長度必須小於或等於{{#limit}}長字符',
      min:
        '"{{#label}}"長度最小為{{#limit}}長字符',
      normalize:
        '"{{#label}}"必須在{{#form}}表單',
      token:
        '"{{#label}}"只能包含字母數字和下劃線字符',
      pattern:
      {
        base:
          '"{{#label}}"及定值"{[.]}"不符合必須的格式：{{#regex}}',
        name:
          '"{{#label}}"及定值"{[.]}"不符合{{#name}}格式',
        invert: {
          base: '"{{#label}}"及定值"{[.]}"符合倒轉的格式：{{#regex}}',
          name: '"{{#label}}"及定值"{[.]}"符合倒轉的{{#name}}格式',
        },
      },
      trim: '"{{#label}}"不得有前導或尾隨空格',
      uri: '"{{#label}}"必須為正確的識別碼',
      uriCustomScheme:
        '"{{#label}}"必須是相對識別碼並匹配的有效的{{#scheme}}格式',
      uriRelativeOnly: '"{{#label}}"必須是有效的相對識別碼',
      uppercase: '"{{#label}}"只能包含大寫字符',
    },
    symbol:
    {
      base: '"{{#label}}"必須為標誌',
      map: '"{{#label}}"必須為{{#map}}中的一個',
    },
    other: {
      phone: '必須為正確的電話格式',
    },
  },
};
