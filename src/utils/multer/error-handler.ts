import multer from 'multer';
import { FileUploadError } from '@utils/errors';
import { Context } from '@utils/interface';

const sequelizeErrorHandler = (ctx: Context, error: any) => {
  let newError = error;
  if (error instanceof multer.MulterError) {
    const { name, code, message, field } = error;
    switch (code) {
      default:
        newError = new FileUploadError({ message });
        break;
    }
    console.log('Multer Error: ', name, code, message, field);
  }
  return newError;
};

export default sequelizeErrorHandler;
