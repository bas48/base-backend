import '@utils/config';
import models from '@database/manager/load-models';
import { getTranslationSource, initI18n } from '@utils/translation';
import { SessionInstance } from '@database/models/Session';
import { UserInstance } from '@database/models/User';

const { DEFAULT_LANGUAGE } = process.env;

interface Option {
  languageId?: string;
  currentSession?: SessionInstance;
  currentUser?: UserInstance;
}

const initContext = async (ctx?: any, option: Option = {}): Promise<any> => {
  const { languageId, currentSession, currentUser } = option;
  const currentLanguage = languageId || DEFAULT_LANGUAGE || 'en-US';
  const translationsSource = await getTranslationSource();
  const translate = initI18n(currentLanguage, translationsSource);
  if (ctx) {
    ctx.languageId = currentLanguage;
    ctx.translate = translate;
    ctx.models = models;
    ctx.currentSession = currentSession;
    ctx.currentUser = currentUser;
    return ctx;
  }
  return {
    languageId: currentLanguage,
    translate,
    models,
    currentSession,
    currentUser,
  };
};

export default initContext;
