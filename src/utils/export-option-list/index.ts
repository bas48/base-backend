import { Option } from '@actions/list';
import { resourceListOption } from '@http/controllers/resource';

const optionList: {
  [key: string]: Option;
} = {
  resourceListOption,
};

export default optionList;
