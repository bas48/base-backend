import BaseError from '@utils/errors/base-error';

class ForbiddenError extends BaseError {
  constructor(extras?: any) {
    super('ForbiddenError', 403, extras);
    Error.captureStackTrace(this, ForbiddenError);
  }
}

export default ForbiddenError;
