import BaseError from '@utils/errors/base-error';

class NotFoundError extends BaseError {
  constructor(extras?: any) {
    super('NotFoundError', 404, extras);
    Error.captureStackTrace(this, NotFoundError);
  }
}

export default NotFoundError;
