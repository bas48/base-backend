import BaseError from '@utils/errors/base-error';
import BadRequestError from '@utils/errors/bad-request-error';
import ConflictError from '@utils/errors/conflict-error';
import ForbiddenError from '@utils/errors/forbidden-error';
import InternalServerError from '@utils/errors/internal-server-error';
import NotFoundError from '@utils/errors/not-found-error';
import RequestLimitError from '@utils/errors/request-limit-error';
import UnauthorizedError from '@utils/errors/unauthorized-error';

class InfoAlreadyUsed extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'InfoAlreadyUsed';
  }
}

class InvalidFileType extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'InvalidFileType';
  }
}

class ResourceAlreadyExists extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'ResourceAlreadyExists';
  }
}

class FileTransferError extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'FileTransferError';
  }
}

class FileCannotRemove extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'FileCannotRemove';
  }
}

class SocialLoginError extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'SocialLoginError';
  }
}

class NotificationAlreadyInProcess extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'NotificationAlreadyInProcess';
  }
}

class JobAlreadyExecuted extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'JobAlreadyExecuted';
  }
}

class VerificationCodeAlreadyVerified extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'VerificationCodeAlreadyVerified';
  }
}

class FileUploadError extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'FileUploadError';
  }
}

class RequestTooMuch extends ConflictError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'RequestTooMuch';
  }
}
// ////////////////////////////////////////////////////////////////////////
class InvalidValidation extends BadRequestError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'InvalidValidation';
  }
}

class VerificationRequired extends BadRequestError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'VerificationRequired';
  }
}

class ExportNotSupport extends BadRequestError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'ExportNotSupport';
  }
}
// ////////////////////////////////////////////////////////////////////////
class AccessTokenExpired extends ForbiddenError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'AccessTokenExpired';
  }
}

class VerificationTokenExpired extends ForbiddenError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'VerificationTokenExpired';
  }
}

class VerificationCodeExpired extends ForbiddenError {
  constructor() {
    super();
    this.code = 'VerificationCodeExpired';
  }
}

// ////////////////////////////////////////////////////////////////////////
class UserAlreadyBanned extends UnauthorizedError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'UserAlreadyBanned';
  }
}

class PermissionDenied extends UnauthorizedError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'PermissionDenied';
  }
}
class LoginRequired extends UnauthorizedError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'LoginRequired';
  }
}
class PrivateFileProtected extends UnauthorizedError {
  constructor(extras?: any) {
    super(extras);
    this.code = 'PrivateFileProtected';
  }
}
class IncorrectPassword extends UnauthorizedError {
  constructor() {
    super();
    this.code = 'IncorrectPassword';
  }
}
class VerificationCodeIncorrect extends UnauthorizedError {
  constructor() {
    super();
    this.code = 'VerificationCodeIncorrect';
  }
}

class KeyIncorrect extends UnauthorizedError {
  constructor() {
    super();
    this.code = 'KeyIncorrect';
  }
}
// ////////////////////////////////////////////////////////////////////////
class TooManyRequestError extends InternalServerError {
  constructor() {
    super();
    this.code = 'NotImplemented';
  }
}

export {
  BaseError,
  BadRequestError,
  ConflictError,
  ForbiddenError,
  InternalServerError,
  NotFoundError,
  RequestLimitError,
  UnauthorizedError,
  InfoAlreadyUsed,
  InvalidFileType,
  ResourceAlreadyExists,
  SocialLoginError,
  VerificationCodeAlreadyVerified,
  VerificationCodeExpired,
  InvalidValidation,
  VerificationRequired,
  LoginRequired,
  AccessTokenExpired,
  VerificationTokenExpired,
  UserAlreadyBanned,
  PermissionDenied,
  PrivateFileProtected,
  IncorrectPassword,
  VerificationCodeIncorrect,
  TooManyRequestError,
  FileTransferError,
  FileCannotRemove,
  NotificationAlreadyInProcess,
  JobAlreadyExecuted,
  ExportNotSupport,
  FileUploadError,
  RequestTooMuch,
  KeyIncorrect,
};
