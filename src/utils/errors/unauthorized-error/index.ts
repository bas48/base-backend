import BaseError from '@utils/errors/base-error';

class UnauthorizedError extends BaseError {
  constructor(extras?: any) {
    super('UnauthorizedError', 401, extras);
    this.code = 'UnauthorizedError';
    Error.captureStackTrace(this, UnauthorizedError);
  }
}

export default UnauthorizedError;
