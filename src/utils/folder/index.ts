import fs from 'fs-extra';
import util from 'util';
import path from 'path';

const exec = util.promisify(require('child_process').exec);
const { rootPath, folderPaths } = require('./paths');

export default {
  async folderSetup() {
    for (const pathName of Object.keys(folderPaths)) {
      try {
        await fs.ensureDir(path.resolve(rootPath, folderPaths[pathName]));
      } catch (err) {
        console.error(err);
      }
    }
  },
  async clearTemporary() {
    // https://unix.stackexchange.com/questions/194863/delete-files-older-than-x-days
    // keep 3 days
    const { stdout, stderr } = await exec(`find ${path.resolve(rootPath, folderPaths.temporary)} -mindepth 1 -mtime +3 -delete`);
    console.log(stdout);
    console.error(stderr);
  },
};
