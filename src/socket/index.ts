import { Server } from 'socket.io';
import { createAdapter } from 'socket.io-redis';
import RedisClient from '@lib/redis/connection';
import initContext from '@utils/context';
import { router } from '@socket/routes';
import { SocketContext } from '@socket/interface';
import ServerEvent from '@server-event';
import { apisDetail, findMatchApi } from '@server-event/routes';

const ioApp = {
  async init(io: Server) {
    const pubClient = new RedisClient();
    const subClient = pubClient.duplicate();
    io.adapter(createAdapter({ pubClient, subClient }));

    for (const event of apisDetail) {
      const { path, uniqueKey } = event;
      ServerEvent.on(path, (data) => {
        let eventPath = path;
        if (uniqueKey) {
          eventPath = `${path}/${data.uniqueKey}`;
          delete data.uniqueKey;
        }
        io.to(eventPath).emit(eventPath, data);
      });
    }

    io.on('connection', async (socket: SocketContext) => {
      console.log('User Connected');
      socket.io = io;
      socket = await initContext(socket);
      router.init(socket);
      socket.on('disconnect', () => {
        console.log('User Disconnected');
      });
      socket.on('disconnecting', async () => {
        console.log('User Disconnecting', socket.rooms);
        console.log('room:', Object.keys(socket.rooms)[0]);
        for (const room of Object.keys(socket.rooms)) {
          const currentEvent = findMatchApi(room);
          if (currentEvent) {
            const { leaveController } = currentEvent;
            if (leaveController) {
              socket.connectAction = 'leave';
              await leaveController(socket);
            }
          }
        }
      });
      socket.on('error', (err: Error) => {
        console.log('Socket Error Start:', err);
      });
    });
  },
};

export default ioApp;
