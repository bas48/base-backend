import Joi, { Schema } from 'joi';
import joiAction from '@actions/joi';
import { SocketContext } from '@socket/interface';

const validate = async (socket: SocketContext, params: any, schema: { [key: string]: Schema }) => {
  const { languageId } = socket;
  const result = await joiAction.validate(socket, languageId, Joi.object({ ...schema }), params);
  const { validatedData } = result;
  return validatedData;
};

export default validate;
