import { PermissionDenied, LoginRequired } from '@utils/errors';
import permissionAction from '@actions/permission';
import { SocketContext, ApiRecord } from '@socket/interface';

const permissionHandler = async (socket: SocketContext, apiDetail: ApiRecord) => {
  const { currentSession, currentUser, translate } = socket;
  const { path, method, sessionRequire } = apiDetail;
  if (sessionRequire && !socket.currentSession) {
    throw new LoginRequired();
  }
  const resourcePermissions = await permissionAction.getResourcePermissions(socket, path, method);
  if (resourcePermissions?.length) {
    const userId = currentUser?.id;
    const session: any = currentSession;
    if (!session || !userId) {
      throw new LoginRequired();
    }
    const userPermissions = await permissionAction.getUserAllPermission(socket, userId);
    if (!userPermissions || !Object.keys(userPermissions).length) {
      throw new PermissionDenied(translate('error:user_without_any_permission'));
    }
    let sessionPermissions;
    if (session.type === 'login') {
      sessionPermissions = userPermissions;
    } else {
      sessionPermissions = permissionAction.removeOverSessionPermission(session.session_permissions, userPermissions);
    }
    const needPermissions = [];
    let success = false;
    for (const resourcePermission of resourcePermissions) {
      const result = permissionAction.comparePermission(resourcePermission, sessionPermissions);
      if (result.success) {
        success = true;
        break;
      } else {
        needPermissions.push(result.needPermission);
      }
    }
    if (!success) {
      throw new PermissionDenied({ needPermissions });
    }
  }
};

export default permissionHandler;
