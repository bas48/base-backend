import Joi from 'joi';
import { findMatchApi } from '@server-event/routes';
import socketValidator from '@socket/middleware/socket-validator';
import permissionHandler from '@socket/middleware/permission-handler';
import { NotFoundError } from '@utils/errors';
import { SocketContext } from '@socket/interface';

export default {
  async subscribe(socket: SocketContext, params: any) {
    const validatedData = await socketValidator(socket, params, {
      event: Joi.string().required(),
      parameters: Joi.object(),
    });
    const { translate } = socket;
    const { event, parameters } = validatedData;
    const currentEvent = findMatchApi(event);
    if (!currentEvent) {
      throw new NotFoundError({ type: 'event', resource: translate('event') });
    }
    const { joinController } = currentEvent;
    await permissionHandler(socket, currentEvent);
    socket.connectAction = 'join';
    socket.params = currentEvent.params;
    let response;
    socket.join(event);
    if (joinController) {
      response = await joinController(socket, parameters);
    }
    let message = `Subscribe to ${event}`;
    if (parameters) {
      message += ` with parameters:\n${JSON.stringify(parameters)}`;
    }
    return response || message;
  },
  async unsubscribe(socket: SocketContext, params: any) {
    const validatedData = await socketValidator(socket, params, {
      event: Joi.string().required(),
      parameters: Joi.object(),
    });
    const { translate } = socket;
    const { event, parameters } = validatedData;
    const currentEvent = findMatchApi(event);
    if (!currentEvent) {
      throw new NotFoundError({ type: 'event', resource: translate('event') });
    }
    const { leaveController } = currentEvent;
    socket.connectAction = 'leave';
    socket.params = currentEvent.params;
    let response;
    socket.leave(event);
    if (leaveController) {
      response = await leaveController(socket, parameters);
    }
    return response || `Unsubscribe to ${event}`;
  },
};
