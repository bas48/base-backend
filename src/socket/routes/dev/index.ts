import { API } from '@socket/interface';
import DevController from '@socket/controllers/dev';

const apiList: API[] = [
  {
    path: '/dev',
    controllerFunc: DevController.dev,
  },
];

export default apiList;
