import Koa from 'koa';
import { Context, PermissionGroup } from '@utils/interface';

export interface ValidatedData {
  body?: any;
  query?: any;
  params?: any;
  cookies?: any;
}

export interface HttpContext extends Koa.Context, Context {
  validatedData ?: ValidatedData,
}

export interface API {
  method: 'get' | 'post' | 'patch' | 'delete';
  path: string;
  controllerFunc: Function;
  sessionRequire?: boolean;
  permissions?: PermissionGroup[];
}

export interface ApiRecord {
  method: 'get' | 'post' | 'patch' | 'delete';
  path: string;
  sessionRequire?: boolean;
  permissions?: PermissionGroup[];
}
