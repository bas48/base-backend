import { API } from '@http/interface';
import FormController from '@http/controllers/form';

const apiList: API[] = [
  {
    method: 'get',
    path: '/form/input/type',
    controllerFunc: FormController.getInputType,
  },
];

export default apiList;
