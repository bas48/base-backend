import { API } from '@http/interface';
import MessageController from '@http/controllers/message';

const apiList: API[] = [
  {
    method: 'get',
    path: '/message/handler',
    permissions: [{ message: 'read' }],
    controllerFunc: MessageController.getMessageHandlers,
  },
  {
    method: 'get',
    path: '/message',
    permissions: [{ message: 'read' }],
    controllerFunc: MessageController.getMessages,
  },
  {
    method: 'get',
    path: '/message/list',
    permissions: [{ message: 'read' }],
    controllerFunc: MessageController.getUMessageList,
  },
  {
    method: 'post',
    path: '/message',
    permissions: [{ message: 'write' }],
    controllerFunc: MessageController.createMessage,
  },
  {
    method: 'patch',
    path: '/message/:messageId',
    permissions: [{ message: 'write' }],
    controllerFunc: MessageController.updateMessage,
  },
  {
    method: 'delete',
    path: '/message/:messageId',
    permissions: [{ message: 'write' }],
    controllerFunc: MessageController.deleteMessage,
  },
];

export default apiList;
