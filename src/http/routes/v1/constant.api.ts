import { API } from '@http/interface';
import ConstantController from '@http/controllers/constant';

const apiList: API[] = [
  {
    method: 'get',
    path: '/constant/list',
    permissions: [{ constant: 'read' }],
    controllerFunc: ConstantController.getConstantList,
  },
  {
    method: 'patch',
    path: '/constant/:constantKey',
    permissions: [{ constant: 'write' }],
    controllerFunc: ConstantController.updateConstant,
  },
];

export default apiList;
