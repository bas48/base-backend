import { API } from '@http/interface';
import NotificationController from '@http/controllers/notification';

const apiList: API[] = [
  {
    method: 'get',
    path: '/notification/user-query',
    permissions: [{ notification: 'read' }],
    controllerFunc: NotificationController.getUserQueries,
  },
  {
    method: 'get',
    path: '/notification/target-types',
    permissions: [{ notification: 'read' }],
    controllerFunc: NotificationController.getTargetTypes,
  },
  {
    method: 'get',
    path: '/notification/content-types',
    permissions: [{ notification: 'read' }],
    controllerFunc: NotificationController.getContentTypes,
  },
  {
    method: 'get',
    path: '/notification/list',
    permissions: [{ notification: 'read' }],
    controllerFunc: NotificationController.getNotificationList,
  },
  {
    method: 'post',
    path: '/notification',
    permissions: [{ notification: 'write' }],
    controllerFunc: NotificationController.createNotification,
  },
  {
    method: 'patch',
    path: '/notification/:notificationId',
    permissions: [{ notification: 'write' }],
    controllerFunc: NotificationController.updateNotification,
  },
  {
    method: 'delete',
    path: '/notification/:notificationId',
    permissions: [{ notification: 'write' }],
    controllerFunc: NotificationController.deleteNotification,
  },
];

export default apiList;
