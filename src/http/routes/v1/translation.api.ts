import { API } from '@http/interface';
import LanguageController from '@http/controllers/language';
import TranslationController from '@http/controllers/translation';

const apiList: API[] = [
  {
    method: 'get',
    path: '/language',
    controllerFunc: LanguageController.getLanguages,
  },
  {
    method: 'get',
    path: '/translation/support-platform',
    controllerFunc: TranslationController.getSupportedPlatforms,
  },
  {
    method: 'get',
    path: '/translation/source',
    controllerFunc: TranslationController.getTranslationSource,
  },
  {
    method: 'get',
    path: '/translation/list',
    permissions: [{ translation: 'read' }],
    controllerFunc: TranslationController.getTranslationsList,
  },
  {
    method: 'post',
    path: '/translation/source',
    controllerFunc: TranslationController.updateDefaultTranslationSource,
  },
  {
    method: 'patch',
    path: '/translation/:platform/:field',
    permissions: [{ translation: 'write' }],
    controllerFunc: TranslationController.updateTranslation,
  },
];

export default apiList;
