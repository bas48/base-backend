import { API } from '@http/interface';
import UserController from '@http/controllers/user';

const apiList: API[] = [
  {
    method: 'patch',
    path: '/user/refresh-token',
    controllerFunc: UserController.refreshToken,
  },
  {
    method: 'get',
    path: '/user/list',
    permissions: [{ user: 'read' }],
    controllerFunc: UserController.getUserList,
  },
  {
    method: 'post',
    path: '/user',
    permissions: [{ user: 'write' }],
    controllerFunc: UserController.createUser,
  },
  {
    method: 'post',
    path: '/user/login',
    controllerFunc: UserController.login,
  },
  {
    method: 'patch',
    path: '/user/personal',
    controllerFunc: UserController.changePersonalInfo,
    sessionRequire: true,
  },
  {
    method: 'post',
    path: '/user/social/login',
    controllerFunc: UserController.socialLogin,
  },
  {
    method: 'post',
    path: '/user/two-factor',
    controllerFunc: UserController.createTwoFactorSecret,
    sessionRequire: true,
  },
  {
    method: 'patch',
    path: '/user/two-factor',
    controllerFunc: UserController.updateTwoFactor,
    sessionRequire: true,
  },
  {
    method: 'get',
    path: '/user/personal',
    controllerFunc: UserController.getPersonalInfo,
    sessionRequire: true,
  },
  {
    method: 'patch',
    path: '/user/reset-password',
    controllerFunc: UserController.resetPassword,
  },
  {
    method: 'patch',
    path: '/user/:userId',
    permissions: [{ user: 'write' }],
    controllerFunc: UserController.updateUser,
  },
  {
    method: 'delete',
    path: '/user/logout',
    controllerFunc: UserController.logout,
    sessionRequire: true,
  },
  {
    method: 'delete',
    path: '/user/:userId',
    controllerFunc: UserController.deleteUser,
  },
  {
    method: 'get',
    path: '/user/permissions',
    controllerFunc: UserController.getUserPermissions,
    sessionRequire: true,
  },
];

export default apiList;
