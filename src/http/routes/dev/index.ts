import { API } from '@http/interface';
import DevController from '@http/controllers/dev';

const apiList: API[] = [
  {
    method: 'get',
    path: '/dev',
    controllerFunc: DevController.getDev,
  },
  {
    method: 'post',
    path: '/dev',
    controllerFunc: DevController.postDev,
  },
];

export default apiList;
