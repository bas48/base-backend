import { BaseError, NotFoundError, InternalServerError } from '@utils/errors';
import { Next } from 'koa';
import sequelizeErrorHandler from '@utils/sequelize/error-handler';
import multerErrorHandler from '@utils/multer/error-handler';
import { HttpContext } from '@http/interface';

export default () => async (ctx: HttpContext, next: Next) => {
  try {
    await next();
    if (!ctx.body) {
      throw new NotFoundError({ type: 'resource_path', resource: ctx.translate('resource_path') });
    }
  } catch (defaultError) {
    const { translate } = ctx;
    let error = defaultError;
    error = sequelizeErrorHandler(ctx, error);
    error = multerErrorHandler(ctx, error);
    if (!(error instanceof BaseError)) {
      error = new InternalServerError();
      console.log('Internal Server Error:', defaultError);
    }
    const { status, code, extras, message } = error;
    ctx.body = {
      code,
      message: message || `${translate(`error:${code}`, extras)}`,
      extras,
    };
    ctx.status = status;
  }
};
