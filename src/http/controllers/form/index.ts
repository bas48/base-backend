import Joi from 'joi';
import { HttpContext } from '@http/interface';

const supportedTypes = ['string', 'date', 'number', 'boolean'];

export default {
  getInputType: async (ctx: HttpContext) => {
    ctx.body = Object.keys(
      Joi.types(),
    ).filter(
      (type: string) => supportedTypes.find((supportedType: string) => supportedType === type),
    );
  },
};
