import { HttpContext } from '@http/interface';
import Language from '@database/models/Language';

export default {
  getLanguages: async (ctx: HttpContext) => {
    const languages = await Language.findAll();
    ctx.body = languages;
  },
};
