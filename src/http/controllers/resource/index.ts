import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import sequelize from '@database';
import commonAction from '@actions/common';
import resourceAction from '@actions/resource';
import { Transaction } from 'sequelize/types';
import { Option } from '@actions/list';
import cJoi from '@utils/custom-joi';

export const resourceListOption: Option = {
  main: {
    model: 'Resource',
  },
  exportSetting: {
    type: 'email',
    sendTo: (ctx: HttpContext) => ctx.currentUser.email,
    optionId: 'resourceListOption',
    fileName: 'resource-list',
  },
  sorts: {
    id: 'Resource.id',
    path: 'Resource.path',
    method: 'Resource.method',
    type: 'Resource.type',
    active: 'Resource.active',
    createdAt: 'Resource.createdAt',
    updatedAt: 'Resource.updatedAt',
  },
  search: ['Resource.id', 'Resource.path', 'Resource.method', 'Resource.type'],
  filter: {
    path: {
      validator: Joi.string(),
      mapper: 'Resource.path',
    },
    method: {
      validator: Joi.string(),
      mapper: 'Resource.method',
    },
    type: {
      validator: Joi.string().valid('api', 'file'),
      mapper: 'Resource.type',
    },
    active: {
      validator: Joi.bool(),
      mapper: 'Resource.active',
    },
    createdAt: {
      validator: cJoi.timeRange(),
      mapper: 'Resource.createdAt',
    },
    updatedAt: {
      validator: cJoi.timeRange(),
      mapper: 'Resource.updatedAt',
    },
  },
  afterQuery: (ctx: HttpContext, results: any) => results,
};

export default {
  getResourceList: commonAction.getList(resourceListOption),

  updateResource: compose([
    apiValidator.body({
      active: Joi.boolean(),
      permissions: Joi.array().items(
        Joi.object().pattern(/[-\w]+/,
          Joi.string().valid('write', 'read').required()),
      ),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData } = ctx;
      const { resourceId } = params;
      const body = validatedData?.body;
      return sequelize.transaction(async (transaction: Transaction) => {
        const resource = await resourceAction.updateResource(ctx, resourceId, body, transaction);
        ctx.body = resource;
      });
    },
  ]),

  deleteResource: async (ctx: HttpContext) => {
    const { params } = ctx;
    const { resourceId } = params;
    return sequelize.transaction(async (transaction) => {
      await resourceAction.deleteResource(ctx, resourceId, transaction);
      ctx.body = {};
    });
  },
};
