import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';

export default {
  getDev: async (ctx: HttpContext) => { ctx.body = {}; },

  postDev: compose([
    apiValidator.body({
      filePath: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      ctx.body = {};
    },
  ]),
};
