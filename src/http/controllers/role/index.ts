import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import sequelize from '@database';
import Role, { RoleInstance } from '@database/models/Role';
import UserRole from '@database/models/UserRole';
import { Transaction } from 'sequelize/types';
import commonAction from '@actions/common';
import translationAction from '@actions/translation';
import { RemoteCache } from '@utils/cache';
import cJoi from '@utils/custom-joi';

export default {
  getRoles: commonAction.getList({
    main: {
      model: 'Role',
      withTranslation: {},
    },
    withCount: false,
    sorts: {
      title: 'RoleTranslation.title',
    },
  }),

  getRoleList: commonAction.getList({
    main: {
      model: 'Role',
      withTranslation: {},
    },
    sorts: {
      id: 'Role.id',
      title: 'RoleTranslation.title',
      createdAt: 'Role.createdAt',
      updatedAt: 'Role.updatedAt',
    },
    search: ['Role.id', 'RoleTranslation.title'],
    filter: {
      createdAt: {
        validator: cJoi.timeRange(),
        mapper: 'Role.createdAt',
      },
      updatedAt: {
        validator: cJoi.timeRange(),
        mapper: 'Role.updatedAt',
      },
    },
  }),

  createRole: compose([
    apiValidator.body({
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string().required(),
      }).required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { translations } = validatedData?.body;
      return sequelize.transaction(async (transaction: Transaction) => {
        const role: RoleInstance = await Role.create({}, { transaction });
        await translationAction.upsertTranslation(ctx, {
          model: 'Role',
          referenceId: role.id,
        }, translations, transaction);
        ctx.body = role;
      });
    },
  ]),

  updateRole: compose([
    apiValidator.body({
      translations: Joi.object().pattern(/[-w]+/, {
        title: Joi.string().required(),
      }).required(),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData } = ctx;
      const { translations } = validatedData?.body;
      const { roleId } = params;
      return sequelize.transaction(async (transaction: Transaction) => {
        await translationAction.upsertTranslation(ctx, {
          model: 'Role',
          referenceId: roleId,
        }, translations, transaction);
        ctx.body = {};
      });
    },
  ]),

  assignUserRoles: compose([
    apiValidator.body({
      roles: Joi.array().items(Joi.number()).required(),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData } = ctx;
      const { roles } = validatedData?.body;
      const { userId } = params;
      return sequelize.transaction(async (transaction: Transaction) => {
        await UserRole.destroy({
          where: {
            userId,
          },
          force: true,
        });
        const userRoles = [];
        for (const roleId of roles) {
          const userRole = {
            userId,
            roleId,
          };
          userRoles.push(userRole);
        }
        await UserRole.bulkCreate(userRoles, { transaction });
        await RemoteCache.set('user-role', userId, roles, { expire: 60 });
        ctx.body = {};
      });
    },
  ]),

  deleteRole: async (ctx: HttpContext) => {
    const { params } = ctx;
    const { roleId } = params;
    return sequelize.transaction(async (transaction) => {
      await commonAction.deleteRecord(ctx, 'Role', {
        id: roleId,
      }, undefined, transaction);
      await translationAction.deleteTranslation({
        model: 'Role',
        referenceId: roleId,
      }, transaction);
      ctx.body = {};
    });
  },
};
