import compose from 'koa-compose';
import Joi from 'joi';
import moment from 'moment';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import sequelize from '@database';
import cJoi from '@utils/custom-joi';
import User from '@database/models/User';
import Verification from '@database/models/Verification';
import { NotFoundError, ResourceAlreadyExists, VerificationCodeAlreadyVerified, VerificationCodeExpired, VerificationCodeIncorrect, RequestTooMuch } from '@utils/errors';
import verificationAction from '@actions/verification';
import { Transaction } from 'sequelize/types';
import { RemoteCache } from '@utils/cache';
import Jobs from '@job-queue/job-define';

const { VERIFICATION_TOKEN_EXPIRE_TIME, RESEND_LIMIT } = process.env;

export default {
  sendCode: compose([
    apiValidator.body({
      type: Joi.string().valid('email', 'phone').required(),
      email: Joi.string().when('type', { is: 'email', then: Joi.string().email().required() }),
      phone: cJoi.phone().when('type', { is: 'phone', then: cJoi.phone().required() }),
      isGuest: Joi.boolean().default(false),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, translate, languageId } = ctx;
      const body = validatedData?.body;
      const { type, isGuest } = validatedData?.body;
      const reference = body[type];
      const isLocked = await RemoteCache.get('verification-lock', reference);
      if (isLocked) {
        throw new RequestTooMuch({ type: translate(type) });
      }
      await RemoteCache.set('verification-lock', reference, true, {
        expire: (Number(RESEND_LIMIT) * 60) || 60,
      });
      // Check whether user is existed or not
      const filter: {[key: string]: any} = {};
      filter[type] = reference;
      const user = await User.findOne({
        where: filter,
      });
      if (!isGuest && !user) {
        throw new NotFoundError({ resource: translate('user') });
      } else if (isGuest && user) {
        throw new ResourceAlreadyExists({ resource: translate(type) });
      }
      // Create or update verification record
      return sequelize.transaction(async (transaction: Transaction) => {
        const verification = await verificationAction.createVerification(ctx, type, reference, (user?.id) || null, transaction);
        const content = translate('send_verification_code', { code: verification.authCode });
        if (type === 'phone') {
          Jobs.send_sms.add({
            phone: reference,
            message: content,
          });
        } else if (type === 'email') {
          Jobs.send_email.add({
            receivers: [reference],
            subject: translate('verification_subject'),
            content,
            languageId,
          });
        }
        if (process.env.NODE_ENV === 'development') {
          ctx.body = { code: verification.authCode };
        } else {
          ctx.body = {};
        }
      });
    },
  ]),

  verifyCode: compose([
    apiValidator.body({
      type: Joi.string().valid('email', 'phone').required(),
      email: Joi.string().when('type', { is: 'email', then: Joi.string().email().required() }),
      phone: cJoi.phone().when('type', { is: 'phone', then: cJoi.phone().required() }),
      code: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, translate } = ctx;
      const { type, code } = validatedData?.body;
      const verification = await Verification.findOne({
        where: {
          type,
          reference: validatedData.body[type],
        },
      });
      if (!verification) {
        throw new NotFoundError({ resource: translate('verification') });
      }
      if (verification.isVerified) {
        throw new VerificationCodeAlreadyVerified();
      }
      if (moment().isAfter(moment(verification.expiresAt))) {
        throw new VerificationCodeExpired();
      }
      if (verification.authCode !== code) {
        throw new VerificationCodeIncorrect();
      }
      verification.isVerified = true;
      verification.expiresAt = moment().add(VERIFICATION_TOKEN_EXPIRE_TIME, 'minute').toDate();
      await verification.save();
      await RemoteCache.del('verification-lock', validatedData.body[type]);
      ctx.body = verification.token;
    },
  ]),
};
