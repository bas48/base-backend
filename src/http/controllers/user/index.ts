import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import moment from 'moment';
import userAction from '@actions/user';
import commonAction from '@actions/common';
import sessionAction from '@actions/session';
import encryptAction from '@actions/encrypt';
import permissionAction from '@actions/permission';
import twoFactorAction from '@actions/two-factor';
import socialAction from '@actions/social';
import sequelize from '@database';
import { RemoteCache } from '@utils/cache';
import Session from '@database/models/Session';
import Verification from '@database/models/Verification';
import User from '@database/models/User';
import SocialDefinition from '@database/models/Social';
import { NotFoundError, VerificationRequired, IncorrectPassword, UserAlreadyBanned, VerificationTokenExpired, ResourceAlreadyExists } from '@utils/errors';
import { Transaction } from 'sequelize/types';
import cJoi from '@utils/custom-joi';

const socialTypes = SocialDefinition.rawAttributes.type.values;

const { SESSION_EXPIRE_TIME } = process.env;

const sessionExpireTime = Number(SESSION_EXPIRE_TIME) || 30; // Minute

export default {
  refreshToken: compose([
    apiValidator.body({
      refreshToken: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { refreshToken } = validatedData?.body;
      return sequelize.transaction(async (transaction) => {
        const session = await sessionAction.refreshToken(ctx, refreshToken, transaction);
        ctx.body = {
          accessToken: session.accessToken,
          expiresAt: session.expiresAt,
        };
      });
    },
  ]),

  getUserList: commonAction.getList({
    main: {
      model: 'User',
      withTranslation: {},
      joins: [
        {
          model: 'UserRole',
          on: { left: 'UserRole.userId', right: 'User.id' },
          joins: [
            {
              model: 'Role',
              on: { left: 'UserRole.roleId', right: 'Role.id' },
              withTranslation: {},
            },
          ],
        },
        {
          model: 'Language',
          on: { left: 'Language.id', right: 'User.languageId' },
        },
      ],
    },
    sorts: {
      id: 'User.id',
      name: 'User.name',
      email: 'User.email',
      phone: 'User.phone',
      language: 'Language.name',
      createdAt: 'User.createdAt',
      updatedAt: 'User.updatedAt',
      roles: 'RoleTranslation.title',
    },
    search: ['User.id', 'User.name', 'User.email', 'User.phone'],
    filter: {
      createdAt: {
        validator: cJoi.timeRange(),
        mapper: 'User.createdAt',
      },
      updatedAt: {
        validator: cJoi.timeRange(),
        mapper: 'User.updatedAt',
      },
      roleId: {
        validator: Joi.number(),
        mapper: 'UserRole.roleId',
      },
    },
    afterQuery: async (ctx: HttpContext, data: { count: number, rows: any[] }) => {
      const { languageId } = ctx;
      const newData = { ...data };
      newData.rows = newData.rows.map((record) => {
        const roles = record.user_roles?.map((userRole: any) => {
          const { title } = userRole?.role;
          return {
            title: (title?.[languageId]?.value) || userRole.roleId,
            id: userRole.roleId,
          };
        });
        const { language } = record;
        const newRecord = { ...record, roles, language: language?.name };
        delete newRecord.user_roles;
        return newRecord;
      });
      return newData;
    },
  }),

  createUser: compose([
    apiValidator.body({
      name: Joi.string().required(),
      icon: cJoi.temporaryFile().required(),
      email: Joi.string().email().required(),
      phone: cJoi.string().required(),
      password: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const { name, icon, email, phone, password } = validatedData?.body;
      return sequelize.transaction(async (transaction: any) => {
        const user = await userAction.createUser({
          name,
          icon,
          email,
          phone,
          password,
        }, transaction);
        ctx.body = user;
      });
    },
  ]),

  updateUser: compose([
    apiValidator.body({
      name: Joi.string(),
      icon: cJoi.temporaryFile(),
      email: Joi.string(),
      phone: Joi.string(),
      password: Joi.string(),
      banned: Joi.boolean(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, params } = ctx;
      const { userId } = params;
      return sequelize.transaction(async (transaction: any) => {
        const user = await userAction.updateUser(ctx, userId, validatedData?.body, transaction);
        const userSessions = await Session.findAll({
          where: {
            userId,
          },
        });
        if (userSessions.length) {
          await RemoteCache.del('sessions', userSessions.map((userSession) => userSession.accessToken));
        }
        ctx.body = user;
      });
    },
  ]),

  socialLogin: compose([
    apiValidator.body({
      token: Joi.string().required(),
      type: Joi.string().valid(...(socialTypes || [])).required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, request, userAgent, languageId } = ctx;
      const { token, type } = validatedData?.body;
      const { ip } = request;
      return sequelize.transaction(async (transaction) => {
        let user;
        switch (type) {
          case 'google':
            user = await socialAction.google(ctx, token, transaction);
            break;
          case 'facebook':
            user = await socialAction.facebook(ctx, token, transaction);
            break;
          default:
            break;
        }
        if (user) {
          if (user.banned) {
            throw new UserAlreadyBanned();
          }
          if (user.languageId !== languageId) {
            user.languageId = languageId;
            await user.save({ transaction });
          }
          const session = await Session.create({
            userId: user.id,
            type: 'login',
            browser: userAgent.browser,
            os: userAgent.os,
            ip: ip.replace('::ffff:', ''),
            expiresAt: new Date(Date.now() + (sessionExpireTime * 60 * 1000)),
          }, { transaction });
          if (ctx.cookies) {
            ctx.cookies.set(
              'Authorization',
              session.accessToken,
              {
                domain: process.env.APP_HOST || 'localhost',
                path: '/',
                maxAge: 10 * 60 * 1000,
                expires: new Date(),
                httpOnly: false,
                overwrite: true,
              },
            );
          }
          ctx.body = {
            accessToken: session.accessToken,
            refreshToken: session.refreshToken,
            expiresAt: session.expiresAt,
          };
        }
      });
    },
  ]),

  login: compose([
    apiValidator.body({
      userId: Joi.string().required(),
      password: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { translate, validatedData, userAgent, request, languageId } = ctx;
      const { ip, query } = request;
      const { userId, password } = validatedData?.body;
      const { twoFAToken } = query;
      const UserAll = User.scope('all');
      const user = await UserAll.findOne({
        where: {
          id: userId,
        },
      });
      if (user?.twoFAEnable) {
        if (!twoFAToken) {
          throw new VerificationRequired({
            missingToken: [{
              type: 'twoFA',
              verifyRequired: false,
            }],
          });
        } else {
          twoFactorAction.verifyToken(user.twoFASecret, Array.isArray(twoFAToken) ? twoFAToken[0] : twoFAToken);
        }
      }
      if (user) {
        const result = await encryptAction.verify(password, user.password);
        if (!result) {
          throw new IncorrectPassword();
        }
        if (user.banned) {
          throw new UserAlreadyBanned();
        }
      } else {
        throw new NotFoundError({ type: 'user', resource: translate('user') });
      }
      return sequelize.transaction(async (transaction: Transaction) => {
        if (user?.languageId !== languageId) {
          user.languageId = languageId;
          await user.save({ transaction });
        }
        if (process.env.SINGLE_LOGIN === 'true') {
          await sessionAction.clearUserSessions(ctx, userId, 'login', transaction);
        }
        const sessionData = {
          userId,
          type: 'login',
          browser: userAgent.browser,
          os: userAgent.os,
          ip: ip.replace('::ffff:', ''),
        };
        const creatSession = await Session.create({
          expiresAt: new Date(Date.now() + (sessionExpireTime * 60 * 1000)),
          ...sessionData,
        }, { transaction });
        if (ctx.cookies) {
          ctx.cookies.set(
            'Authorization',
            creatSession.accessToken,
            {
              domain: process.env.APP_HOST || 'localhost',
              path: '/',
              maxAge: 10 * 60 * 1000,
              expires: new Date(),
              httpOnly: false,
              overwrite: true,
            },
          );
        }
        ctx.body = {
          accessToken: creatSession.accessToken,
          refreshToken: creatSession.refreshToken,
          expiresAt: creatSession.expiresAt,
        };
      });
    },
  ]),

  logout: async (ctx: HttpContext) => {
    const { currentSession } = ctx;
    if (currentSession) {
      const { accessToken } = currentSession;
      sequelize.transaction(async (transaction: Transaction) => {
        await commonAction.deleteRecord(ctx, 'Session', { accessToken }, undefined, transaction);
        await RemoteCache.del('sessions', accessToken);
        ctx.cookies.set(
          'Authorization',
          '',
          {
            domain: process.env.APP_HOST || 'localhost',
            path: '/',
            maxAge: 10 * 60 * 1000,
            expires: new Date(),
            httpOnly: false,
            overwrite: true,
          },
        );
        console.log('ctx cookies: ', ctx.cookies);
      });
    }
    ctx.body = {};
  },

  createTwoFactorSecret: async (ctx: HttpContext) => {
    const { currentUser, translate } = ctx;
    if (currentUser?.twoFAEnable) {
      throw new ResourceAlreadyExists({ resource: translate('two-factor') });
    }
    if (currentUser) {
      const qrCode = await twoFactorAction.createSecret(ctx, currentUser.id);
      ctx.body = { qrCode };
    }
  },

  updateTwoFactor: compose([
    apiValidator.body({
      token: Joi.string().required(),
      enable: Joi.boolean().required(),
    }),
    async (ctx: HttpContext) => {
      const { currentUser, currentSession, translate, validatedData } = ctx;
      if (currentUser && currentSession) {
        const { token, enable } = validatedData?.body;
        const UserAll = User.scope('all');
        const user = await UserAll.findOne({
          where: {
            id: currentUser.id,
          },
          // throwError: true,
        });
        if (enable) {
          if (user) {
            if (user.twoFAEnable) {
              throw new ResourceAlreadyExists({ resource: translate('two_factor') });
            }
            const secret = await twoFactorAction.confirmSecret(ctx, user.id, token);
            user.twoFASecret = secret;
            user.twoFAEnable = true;
            await user.save();
          }
        } else if (user) {
          if (!user.twoFAEnable) {
            throw new NotFoundError({ type: 'two_factor', resource: translate('two_factor') });
          }
          await twoFactorAction.verifyToken(user.twoFASecret, token);
          user.twoFAEnable = false;
          await user.save();
        }
        await RemoteCache.del('sessions', currentSession.accessToken);
      }
      ctx.body = {};
    },
  ]),

  getPersonalInfo: async (ctx: HttpContext) => {
    const { currentUser, translate } = ctx;
    if (currentUser) {
      const user = await User.findOne({
        where: {
          id: currentUser.id,
        },
        // throwError: true,
      });
      if (!user) {
        throw new NotFoundError({ type: 'user', resource: translate('user') });
      }
      ctx.body = user?.toJSON();
    }
  },

  getUserPermissions: async (ctx: HttpContext) => {
    const { currentUser } = ctx;
    if (currentUser) {
      const userPermissions = await permissionAction.getUserAllPermission(ctx, currentUser.id);
      ctx.body = userPermissions || {};
    }
  },

  resetPassword: compose([
    apiValidator.body({
      token: Joi.string().required(),
      password: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, translate } = ctx;
      const { token, password } = validatedData?.body;
      return sequelize.transaction(async (transaction: Transaction) => {
        const verification = await Verification.findOne({
          where: {
            token,
          },
          transaction,
          // throwError: true,
        });
        if (!verification) {
          throw new NotFoundError({ type: 'verification', resource: translate('verification') });
        }
        if (!verification.userId) {
          throw new NotFoundError({ type: 'user', resource: translate('user') });
        }
        if (moment().isAfter(moment(verification.expiresAt))) {
          throw new VerificationTokenExpired();
        }
        await userAction.updateUser(ctx, verification.userId, { password }, transaction);
        await verification.destroy({ transaction });
        ctx.body = {};
      });
    },
  ]),

  changePersonalInfo: compose([
    apiValidator.body({
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().required(),
    }),
    async (ctx: HttpContext) => {
      const { currentUser, validatedData, translate } = ctx;
      if (currentUser) {
        const body = validatedData?.body;
        const { oldPassword, newPassword } = body;
        const UserAll = User.scope('all');
        const user = await UserAll.findOne({
          where: {
            id: currentUser.id,
          },
          // throwError: true,
        });
        if (!user) {
          throw new NotFoundError({ type: 'user', resource: translate('user') });
        }
        const result = await encryptAction.verify(oldPassword, user.password);
        if (!result) {
          throw new IncorrectPassword();
        }
        return sequelize.transaction(async (transaction: Transaction) => {
          await userAction.updateUser(ctx, currentUser.id, { password: newPassword }, transaction);
          ctx.body = user?.toJSON();
        });
      }
      return null;
    },
  ]),

  deleteUser: async (ctx: HttpContext) => {
    const { params } = ctx;
    const { userId } = params;
    await commonAction.deleteRecord(ctx, 'User', { id: userId });
    ctx.body = {};
  },
};
