import compose from 'koa-compose';
import path from 'path';
import fileAction from '@actions/file';
import { HttpContext } from '@http/interface';
import { rootPath, folderPaths } from '@utils/folder/paths';
import { NotFoundError } from '@utils/errors';

const { temporaryFile, temporaryImage, temporaryVideo, htmlImage } = folderPaths;

export default {
  uploadTemporaryFile: compose([
    fileAction.upload(path.resolve(rootPath, temporaryFile), null, 20).single('file'),
    async (ctx: HttpContext) => {
      const { translate, request } = ctx;
      if (!request.file) {
        throw new NotFoundError({ type: 'file', resource: translate('file') });
      }
      ctx.body = `/${temporaryFile}/${request.file.filename}`;
    },
  ]),
  uploadTemporaryImage: compose([
    fileAction.upload(path.resolve(rootPath, temporaryImage), 'image/*', 20).single('file'),
    async (ctx: HttpContext) => {
      const { translate, request } = ctx;
      if (!request.file) {
        throw new NotFoundError({ type: 'file', resource: translate('file') });
      }
      ctx.body = `/${temporaryImage}/${request.file.filename}`;
    },
  ]),
  uploadTemporaryVideo: compose([
    fileAction.upload(path.resolve(rootPath, temporaryVideo), 'video/*', 100).single('file'),
    async (ctx: HttpContext) => {
      const { translate, request } = ctx;
      if (!request.file) {
        throw new NotFoundError({ type: 'file', resource: translate('file') });
      }
      ctx.body = `/${temporaryVideo}/${request.file.filename}`;
    },
  ]),
  uploadHtmlImage: compose([
    fileAction.upload(path.resolve(rootPath, htmlImage), 'image/*', 20).single('file'),
    async (ctx: HttpContext) => {
      const { translate, request } = ctx;
      if (!request.file) {
        throw new NotFoundError({ type: 'file', resource: translate('file') });
      }
      ctx.body = `/${htmlImage}/${request.file.filename}`;
    },
  ]),
};
