import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import sequelize from '@database';
import Query from '@database/models/Query';
import { Transaction } from 'sequelize/types';
import commonAction from '@actions/common';
import translationAction from '@actions/translation';
import { NotFoundError } from '@utils/errors';
import cJoi from '@utils/custom-joi';

export default {
  getQueryTypes: async (ctx: HttpContext) => {
    ctx.body = Query.rawAttributes.type.values;
  },

  getQueryList: commonAction.getList({
    main: {
      model: 'Query',
      withTranslation: {},
    },
    sorts: {
      id: 'Query.id',
      type: 'Query.type',
      title: 'QueryTranslation.title',
      createdAt: 'Query.createdAt',
      updatedAt: 'Query.updatedAt',
    },
    search: ['Query.type', 'QueryTranslation.title', 'Query.content'],
    filter: {
      type: {
        validator: Joi.string(),
        mapper: 'Query.type',
      },
      createdAt: {
        validator: cJoi.timeRange(),
        mapper: 'Query.createdAt',
      },
      updatedAt: {
        validator: cJoi.timeRange(),
        mapper: 'Query.updatedAt',
      },
    },
  }),

  createQuery: compose([
    apiValidator.body({
      type: Joi.string().required(),
      content: Joi.string().required(),
      variable: Joi.object(),
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string().required(),
      }).required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData } = ctx;
      const body = validatedData?.body;
      const { translations } = body;
      delete body.translations;
      return sequelize.transaction(async (transaction: Transaction) => {
        const query = await Query.create(body, { transaction });
        await translationAction.upsertTranslation(ctx, {
          model: 'Query',
          referenceId: query.id,
        }, translations, transaction);
        ctx.body = query;
      });
    },
  ]),

  // if (!query) {
  //   throw new NotFoundError({ type: 'resource', resource: translate('resource') });
  // }
  updateQuery: compose([
    apiValidator.body({
      type: Joi.string(),
      content: Joi.string(),
      variable: Joi.object(),
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string().required(),
      }),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData, translate } = ctx;
      const { queryId } = params;
      const body = validatedData?.body;
      const { translations } = body;
      delete body.translations;
      return sequelize.transaction(async (transaction) => {
        const query = await Query.findOne({
          where: {
            id: queryId,
          },
          // throwError: true,
        });
        if (!query) {
          throw new NotFoundError({ type: 'resource', resource: translate('resource') });
        }
        Object.assign(query, body);
        console.log('query: ', query);
        await query.save({ transaction });
        if (translations) {
          console.log('translations: ', translations);
          await translationAction.upsertTranslation(ctx, {
            model: 'Query',
            referenceId: queryId,
          }, translations, transaction);
        }
        ctx.body = {};
      });
    },
  ]),
};
