import compose from 'koa-compose';
import Joi, { Schema } from 'joi';
import moment from 'moment';
import apiValidator from '@http/middleware/api-validator';
import { NotFoundError, NotificationAlreadyInProcess } from '@utils/errors';
import cJoi from '@utils/custom-joi';
import { HttpContext } from '@http/interface';
import ContentDefinition from '@database/models/NotificationContent';
import TargetDefinition from '@database/models/NotificationTarget';
import translationAction from '@actions/translation';
import handlerAction from '@actions/handler';
import joiAction from '@actions/joi';
import notificationAction from '@actions/notification';
import commonAction from '@actions/common';
import sequelize from '@database';

interface messageTypes {
  id: string,
  paramsValue?: string,
}

const { NOTIFICATION_EDIT_DEADLINE } = process.env;
const targetTypes = TargetDefinition.rawAttributes.type.values;
const contentChoices = ContentDefinition.rawAttributes.type.values;
const contentTypes: {[key: string]: Schema } = {};
if (contentChoices) {
  for (const contentChoice of contentChoices) {
    contentTypes[contentChoice] = Joi.object({
      message: Joi.object({
        id: Joi.string().required(),
        paramsValue: Joi.object(),
      }).required(),
    });
  }
}

export default {
  getUserQueries: commonAction.getList({
    main: {
      model: 'Query',
      withTranslation: {},
      condition: {
        query: 'Query.type = \'userId\'',
      },
    },
    withCount: false,
    sorts: {
      title: 'QueryTranslation.title',
    },
  }),

  getTargetTypes: (ctx: HttpContext) => {
    ctx.body = targetTypes;
  },

  getContentTypes: (ctx: HttpContext) => {
    ctx.body = contentChoices;
  },

  getNotificationList: commonAction.getList({
    main: {
      model: 'Notification',
      withTranslation: {},
      joins: [
        {
          model: 'NotificationContent',
          on: { left: 'NotificationContent.notificationId', right: 'Notification.id' },
        },
        {
          model: 'NotificationTarget',
          on: { left: 'NotificationTarget.notificationId', right: 'Notification.id' },
        },
      ],
    },
    sorts: {
      id: 'Notification.id',
      title: 'NotificationTranslation.title',
      isSystem: 'NotificationTranslation.isSystem',
      pushedAt: 'NotificationTranslation.pushedAt',
      createdAt: 'Notification.createdAt',
      updatedAt: 'Notification.updatedAt',
    },
    search: ['NotificationTranslation.title'],
    filter: {
      isSystem: {
        validator: Joi.boolean(),
        mapper: 'Notification.isSystem',
      },
      pushedAt: {
        validator: cJoi.timeRange(),
        mapper: 'Notification.pushedAt',
      },
      createdAt: {
        validator: cJoi.timeRange(),
        mapper: 'Notification.createdAt',
      },
      updatedAt: {
        validator: cJoi.timeRange(),
        mapper: 'Notification.updatedAt',
      },
    },
    afterQuery: async (ctx: HttpContext, data: any) => {
      const newData = { ...data };
      newData.rows = newData.rows.map((record: any) => {
        record.content = {};
        for (const content of record.notification_contents) {
          const { type, messageId, messageParams } = content;
          const message: messageTypes = { id: messageId };
          if (messageParams) {
            message.paramsValue = messageParams;
          }
          record.content[type] = { message };
        }
        delete record.notification_contents;
        record.target = {};
        for (const target of record.notification_targets) {
          const { type, reference } = target;
          record.target.type = type;
          if (reference) {
            record.target.reference = reference;
          }
        }
        delete record.notification_targets;
        return record;
      });
      return newData;
    },
  }),

  createNotification: compose([
    apiValidator.body({
      pushedAt: Joi.date().greater('now').required(),
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string().required(),
      }).required(),
      content: Joi.object(contentTypes).required(),
      target: Joi.object({
        type: Joi.string().valid(...(targetTypes || [])).required(),
        reference: Joi.any().when('type', {
          switch: [
            {
              is: 'user',
              then: Joi.object({
                queryId: Joi.string().required(),
                variableValues: Joi.object(),
              }).required(),
            },
            { is: 'fcm_key', then: Joi.string().required() },
            {
              is: 'role',
              then: Joi.array().items(
                Joi.string().required(),
              ).required(),
            },
          ],
          otherwise: Joi.any(),
        }),
      }).required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, models, translate, languageId } = ctx;
      const { Notification, NotificationContent, NotificationTarget, Query, Message } = models;
      const { pushedAt, translations, content, target } = validatedData?.body;
      return sequelize.transaction(async (transaction) => {
        // Create Notification
        const notification = await Notification.create({ pushedAt }, { transaction });
        await translationAction.upsertTranslation(ctx, {
          model: 'Notification',
          referenceId: notification.id,
        }, translations, transaction);
        // Create Notification Content
        const notificationContents = [];
        for (const type of Object.keys(content)) {
          const { message } = content[type];
          const { id, paramsValue = {} } = message;
          const selectedMessage = await Message.findOne({
            transaction,
            where: {
              id,
            },
          });
          if (!selectedMessage) {
            throw new NotFoundError({ type: 'message', resource: translate('message') });
          }
          const { handlers } = selectedMessage;
          if (handlers) {
            for (const handlerId of handlers) {
              const handlerParamValue = { ...paramsValue[handlerId] };
              for (const key of Object.keys(handlerParamValue)) {
                if (handlerParamValue[key] === 'system') {
                  delete handlerParamValue[key];
                }
              }
              await handlerAction.checkHandlerParams(ctx, languageId, 'message', handlerId, handlerParamValue, true);
            }
          }
          notificationContents.push({
            notificationId: notification.id,
            type,
            messageId: id,
            messageParams: paramsValue,
          });
        }
        await NotificationContent.bulkCreate(notificationContents, { transaction });
        // Create Notification Target
        if (target.type === 'user') {
          const { reference } = target;
          const { queryId, variableValues } = reference;
          const selectedQuery = await Query.findOne({
            transaction,
            where: {
              id: queryId,
            },
          });
          if (!selectedQuery) {
            throw new NotFoundError({ type: 'query', resource: translate('query') });
          }
          const { variable } = selectedQuery;
          const variableValidator: { [key: string]: Schema } = {};
          for (const key of Object.keys(variable)) {
            const joiType = variable[key];
            variableValidator[key] = Joi.types()[joiType].required();
          }
          await joiAction.validate(ctx, languageId, Joi.object({ ...variableValidator }), variableValues, true);
        }
        const notificationTarget = await NotificationTarget.create({
          notificationId: notification.id,
          ...target,
        }, { transaction });
        ctx.body = {
          notification,
          notificationContents,
          notificationTarget,
        };
        // Response to frontend
        await notificationAction.upsertJob({
          id: notification.id,
          pushedAt: notification.pushedAt,
        }, [{
          type: notificationTarget.type,
          reference: notificationTarget.reference,
        }], notificationContents);
        ctx.body = notification;
      });
    },
  ]),

  updateNotification: compose([
    apiValidator.body({
      pushedAt: Joi.date().greater('now'),
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string().required(),
      }),
      content: Joi.object(contentTypes),
      target: Joi.object({
        type: Joi.string().valid(...(targetTypes || [])).required(),
        reference: Joi.any().when('type', {
          switch: [
            {
              is: 'user',
              then: Joi.object({
                queryId: Joi.string(),
                variableValues: Joi.object(),
              }).required(),
            },
            { is: 'fcm_key', then: Joi.string().required() },
            {
              is: 'role',
              then: Joi.array().items(
                Joi.string().required(),
              ).required(),
            },
          ],
          otherwise: Joi.any(),
        }),
      }),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, models, params, translate, languageId } = ctx;
      const { notificationId } = params;
      const { Notification, NotificationContent, NotificationTarget, Query, Message } = models;
      const body = validatedData?.body;
      const { translations, content, target } = body;
      delete body.translations;
      delete body.content;
      delete body.target;
      return sequelize.transaction(async (transaction) => {
        const notification = await Notification.findOne({
          where: {
            id: notificationId,
          },
          transaction,
        });
        if (!notification) {
          throw new NotFoundError({ type: 'notification', resource: translate('notification') });
        }
        if (moment().isAfter(moment(notification.pushedAt).subtract(NOTIFICATION_EDIT_DEADLINE, 'minutes'))) {
          throw new NotificationAlreadyInProcess();
        }
        Object.assign(notification, body);
        await notification.save({ transaction });
        if (translations) {
          await translationAction.upsertTranslation(ctx, {
            model: 'Notification',
            referenceId: notificationId,
          }, translations, transaction);
        }
        let notificationContents = [];
        if (content) {
          for (const type of Object.keys(content)) {
            const { message } = content[type];
            const { id, paramsValue } = message;
            const selectedMessage = await Message.findOne({
              transaction,
              where: {
                id,
              },
            });
            if (!selectedMessage) {
              throw new NotFoundError({ type: 'message', resource: translate('message') });
            }
            const { handlers } = selectedMessage;
            if (handlers) {
              for (const handlerId of handlers) {
                const handlerParamValue = { ...paramsValue[handlerId] };
                if (paramsValue[handlerId]) {
                  for (const key of Object.keys(paramsValue[handlerId])) {
                    if (handlerParamValue[key] === 'system') {
                      delete handlerParamValue[key];
                    }
                  }
                }
                await handlerAction.checkHandlerParams(ctx, languageId, 'message', handlerId, handlerParamValue, true);
              }
            }
            const notificationContent = {
              notificationId: notification.id,
              type,
              messageId: id,
              messageParams: selectedMessage.handlers ? paramsValue : null,
            };
            notificationContents.push(notificationContent);
            await NotificationContent.upsert(notificationContent, {
              transaction,
            });
          }
        } else {
          notificationContents = await NotificationContent.findAll({
            where: {
              notificationId: notification.id,
            },
            transaction,
          });
        }
        let notificationTarget;
        if (target) {
          await NotificationTarget.destroy({
            where: {
              notificationId: notification.id,
            },
            transaction,
          });
          if (target.type === 'user') {
            const { reference } = target;
            const { queryId, variableValues } = reference;
            const selectedQuery = await Query.findOne({
              transaction,
              where: {
                id: queryId,
              },
            });
            if (!selectedQuery) {
              throw new NotFoundError({ type: 'query', resource: translate('query') });
            }
            const { variable } = selectedQuery;
            const variableValidator: { [key: string]: Schema } = {};
            for (const key of Object.keys(variable)) {
              const joiType = variable[key];
              variableValidator[key] = Joi.types()[joiType].required();
            }
            await joiAction.validate(ctx, languageId, Joi.object({ ...variableValidator }), variableValues, true);
          }
          notificationTarget = {
            ...target,
          };
          await NotificationTarget.upsert({
            notificationId: notification.id,
            ...target,
          }, {
            transaction,
          });
        } else {
          notificationTarget = await NotificationTarget.findOne({
            where: {
              notificationId: notification.id,
            },
            transaction,
          });
        }
        await notificationAction.upsertJob(notification, [notificationTarget], notificationContents);
        ctx.body = notification;
      });
    },
  ]),

  deleteNotification: async (ctx: HttpContext) => {
    const { params } = ctx;
    const { notificationId } = params;
    return sequelize.transaction(async (transaction) => {
      await commonAction.deleteRecord(ctx, 'Notification', {
        id: notificationId,
      }, undefined, transaction);
      await translationAction.deleteTranslation({
        model: 'Notification',
        referenceId: notificationId,
      }, transaction);
      ctx.body = {};
    });
  },
};
