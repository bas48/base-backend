import compose from 'koa-compose';
import Joi from 'joi';
import { HttpContext } from '@http/interface';
import apiValidator from '@http/middleware/api-validator';
import moment from 'moment';
import userAction from '@actions/user';
import commonAction from '@actions/common';
import sessionAction from '@actions/session';
import encryptAction from '@actions/encrypt';
import permissionAction from '@actions/permission';
import twoFactorAction from '@actions/two-factor';
import socialAction from '@actions/social';
import sequelize from '@database';
import { RemoteCache } from '@utils/cache';
import Session from '@database/models/Session';
import Verification from '@database/models/Verification';
import User from '@database/models/User';
import SocialDefinition from '@database/models/Social';
import { NotFoundError, VerificationRequired, IncorrectPassword, UserAlreadyBanned, VerificationTokenExpired, ResourceAlreadyExists } from '@utils/errors';
import { Transaction } from 'sequelize/types';
import cJoi from '@utils/custom-joi';

const socialTypes = SocialDefinition.rawAttributes.type.values;

const sessionExpireTime = 30; // Minute

export default {
  socialRegister: async (ctx: HttpContext) => {
    ctx.body = {};
  },
  socialLogin: async (ctx: HttpContext) => {
    ctx.body = {};
  },
};
