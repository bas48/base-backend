import compose from 'koa-compose';
import Joi from 'joi';
import apiValidator from '@http/middleware/api-validator';
import cJoi from '@utils/custom-joi';
import sequelize from '@database';
import commonAction from '@actions/common';
import translationAction from '@actions/translation';
import handlerAction from '@actions/handler';
import fileAction from '@actions/file';
import { NotFoundError, PermissionDenied } from '@utils/errors';
import { folderPaths } from '@utils/folder/paths';
import { HttpContext } from '@http/interface';

export default {
  getMessageHandlers: (ctx: HttpContext) => {
    const messageHandlers = handlerAction.getHandlers(ctx, 'message');
    ctx.body = messageHandlers;
  },

  getMessages: commonAction.getList({
    main: {
      model: 'Message',
      withTranslation: {},
    },
    withCount: false,
    sorts: {
      title: 'MessageTranslation.title',
    },
    afterQuery: (ctx, data: any[]) => {
      const messageHandlers = handlerAction.getHandlers(ctx, 'message');
      const messages = data.map((message) => {
        const newMessage = { ...message };
        if (message.handlers) {
          newMessage.handlers = {};
          for (const handlerId of message.handlers) {
            newMessage.handlers[handlerId] = messageHandlers.find((messageHandler) => messageHandler.id === handlerId);
          }
        }
        return newMessage;
      });
      return messages;
    },
  }),

  getUMessageList: commonAction.getList({
    main: {
      model: 'Message',
      withTranslation: {},
    },
    sorts: {
      id: 'Message.id',
      title: 'MessageTranslation.title',
      isSystem: 'Message.isSystem',
      handler: 'Message.handler',
      createdAt: 'Message.createdAt',
      updatedAt: 'Message.updatedAt',
    },
    search: ['MessageTranslation.title', 'MessageTranslation.content'],
    filter: {
      isSystem: {
        validator: Joi.boolean(),
        mapper: 'Message.isSystem',
      },
      createdAt: {
        validator: cJoi.timeRange(),
        mapper: 'Message.createdAt',
      },
      updatedAt: {
        validator: cJoi.timeRange(),
        mapper: 'Message.updatedAt',
      },
    },
    afterQuery: (ctx, data) => {
      data.rows = data.rows.map((message: any) => ({
        ...message,
        handler: (message.handler?.split(',')) || null,
      }));
      return data;
    },
  }),

  createMessage: compose([
    apiValidator.body({
      id: Joi.string().required(),
      handlers: Joi.array().items(Joi.string().required()),
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string().required(),
        content: Joi.string().allow(''),
        imageContent: cJoi.temporaryFile().allow(''),
      }).required(),
    }),
    async (ctx: HttpContext) => {
      const { validatedData, models } = ctx;
      const { id, translations, handlers } = validatedData?.body;
      const { Message } = models;
      return sequelize.transaction(async (transaction) => {
        if (handlers?.length) {
          for (const handlerId of handlers) {
            handlerAction.isHandlerExist(ctx, handlerId, 'message');
          }
        }
        const message = await Message.create({
          id,
          handlers,
        }, { transaction });
        // Store temp image content
        const tempPaths = [];
        for (const languageId of Object.keys(translations)) {
          const { imageContent } = translations[languageId];
          if (imageContent) {
            tempPaths.push(imageContent);
            const { target } = await fileAction.getTransferPath(ctx, imageContent, folderPaths.messageImage);
            translations[languageId].imageContent = target.display;
          }
        }
        await translationAction.upsertTranslation(ctx, {
          model: 'Message',
          referenceId: message.id,
        }, translations, transaction);
        // Store temp image content location to message static folder
        if (tempPaths.length) {
          for (const tempPath of tempPaths) {
            await fileAction.transferFile(ctx, tempPath, folderPaths.messageImage, 'copy');
          }
        }
        ctx.body = message;
      });
    },
  ]),

  updateMessage: compose([
    apiValidator.body({
      handler: Joi.array().items(Joi.string()),
      translations: Joi.object().pattern(/[-\w]+/, {
        title: Joi.string(),
        content: Joi.string().allow(''),
        imageContent: cJoi.temporaryFile().allow('', null),
      }),
    }),
    async (ctx: HttpContext) => {
      const { params, validatedData, models, translate } = ctx;
      const { Message, Translation } = models;
      const { messageId } = params;
      const body = { ...validatedData?.body };
      const translations = { ...body.translations };
      delete body.translations;
      return sequelize.transaction(async (transaction) => {
        if (body.handler) {
          body.handler = body.handler.join(',');
        }
        const message = await Message.findOne({
          where: {
            id: messageId,
          },
          include: [
            {
              model: Translation,
              association: Message.hasMany(Translation,
                {
                  foreignKey: 'referenceId', sourceKey: 'id',
                }),
              where: {
                model: 'Message',
              },
            },
          ],
          transaction,
        });
        if (!message) {
          throw new NotFoundError({ type: 'message', resource: translate('message') });
        }
        Object.assign(message, body);
        await message.save({ transaction });
        const messageDefine = translationAction.translationHandler(message.toJSON());

        // Store temp image content
        const tempPaths = [];
        // Store message image content to remove
        const removePaths = [];
        if (translations) {
          // add some temp path
          // remove some original path
          for (const languageId of Object.keys(translations)) {
            const { imageContent } = translations[languageId];
            if (imageContent !== undefined) {
              const oriImageContent = messageDefine.imageContent?.[languageId];
              if (imageContent && (!oriImageContent || oriImageContent.value !== imageContent)) {
                tempPaths.push(imageContent);
                const { target } = await fileAction.getTransferPath(ctx, imageContent, folderPaths.messageImage);
                translations[languageId].imageContent = target.display;
                if (oriImageContent?.value) {
                  removePaths.push(oriImageContent?.value);
                }
              }
              if (!imageContent && oriImageContent?.value) {
                removePaths.push(oriImageContent.value);
              }
            }
          }
        }
        await translationAction.upsertTranslation(ctx, {
          model: 'Message',
          referenceId: messageId,
        }, translations, transaction);
        // Store temp image content location to message static folder
        if (tempPaths.length) {
          for (const tempPath of tempPaths) {
            await fileAction.transferFile(ctx, tempPath, folderPaths.messageImage);
          }
        }
        // Remove original image content
        if (removePaths.length) {
          for (const removePath of removePaths) {
            await fileAction.removeFile(ctx, removePath);
          }
        }
        ctx.body = { body, tempPaths, removePaths };
      });
    },
  ]),

  deleteMessage: async (ctx: HttpContext) => {
    const { params, models, translate } = ctx;
    const { Message, Translation } = models;
    const { messageId } = params;
    return sequelize.transaction(async (transaction) => {
      const message = await Message.findOne({
        where: {
          id: messageId,
        },
        include: [
          {
            model: Translation,
            association: Message.hasMany(Translation,
              {
                foreignKey: 'referenceId', sourceKey: 'id',
              }),
            where: {
              model: 'Message',
            },
          },
        ],
        transaction,
      });
      if (!message) {
        throw new NotFoundError({ type: 'message', resource: translate('message') });
      }
      if (message.isSystem) {
        throw new PermissionDenied(translate('error:system_message_protect'));
      }
      const removePaths = [];
      const messageDefine = translationAction.translationHandler(message.toJSON());
      const { imageContent } = messageDefine;
      if (imageContent) {
        for (const languageId of Object.keys(imageContent)) {
          const { value } = imageContent[languageId];
          removePaths.push(value);
        }
      }
      await message.destroy({ transaction });
      await translationAction.deleteTranslation({
        model: 'Message',
        referenceId: messageId,
      }, transaction);
      if (removePaths.length) {
        for (const removePath of removePaths) {
          await fileAction.removeFile(ctx, removePath);
        }
      }
      ctx.body = {};
    });
  },
};
