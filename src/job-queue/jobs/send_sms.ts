import { Job, DoneCallback } from 'bull';
import phoneAction from '@actions/phone';

const sendSms = async (job: Job, done: DoneCallback) => {
  const { data } = job;
  const { phone, message } = data;
  try {
    await phoneAction.sendSms(phone, message);
  } catch (error) {
    console.log('Send sms Error:', error);
    job.moveToFailed(data, true);
  }
  done();
};

export default sendSms;
