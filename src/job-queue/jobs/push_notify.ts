import { Job, DoneCallback } from 'bull';
import firebaseAction from '@actions/firebase';

const pushNotify = async (job: Job, done: DoneCallback) => {
  const { data } = job;
  const { targets, message } = data;
  try {
    const { successTokens, failedTokens } = await firebaseAction.pushMessage(targets, message);
    console.log(`Success Token: ${successTokens}\nFailed Token: ${failedTokens}`);
  } catch (error) {
    console.log('Push notify Error:', error);
    job.moveToFailed(data, true);
  }
  done();
};

export default pushNotify;
