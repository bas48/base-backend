import { Job, DoneCallback } from 'bull';

const sendEmail = async (job: Job, done: DoneCallback) => {
  const { data } = job;
  // send message to user inbox here
  try {
    console.log('Send message to user inbox job processing!', data);
  } catch (error) {
    console.log('Inbox Error:', error);
    job.moveToFailed(data, true);
  }
  done();
};

export default sendEmail;
