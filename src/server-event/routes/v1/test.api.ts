import { API } from '@server-event/interface';
import TestController from '@server-event/controllers/test';

const apiList: API[] = [
  {
    path: '/test',
    uniqueKey: 'id',
    joinController: TestController.testJoin,
    leaveController: TestController.testLeave,
  },
];

export default apiList;
