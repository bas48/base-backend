import { API } from '@server-event/interface';
import DevController from '@server-event/controllers/dev';

const apiList: API[] = [
  {
    path: '/dev',
    joinController: DevController.devJoin,
    leaveController: DevController.devLeave,
  },
];
export default apiList;
