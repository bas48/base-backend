export interface Schedule {
  scheduleTime: string;
  controllerFunc: Function;
}
