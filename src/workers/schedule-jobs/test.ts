import { Schedule } from '@workers/interface';
import testController from '@workers/controllers/test';

const schedules: Schedule[] = [
  {
    scheduleTime: '* 0 * * * *',
    controllerFunc: testController.testSchedule,
  },
];

export default schedules;
