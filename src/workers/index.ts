import '@utils/config';
import schedule from 'node-schedule';
import scheduleJobs from '@workers/schedule-jobs';
import initContext from '@utils/context';

const init = async () => {
  const ctx = await initContext();
  for (const scheduleJob of scheduleJobs) {
    const { scheduleTime, controllerFunc } = scheduleJob;
    schedule.scheduleJob(scheduleTime, () => controllerFunc(ctx));
  }
};

init();
