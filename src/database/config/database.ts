import dotenv from 'dotenv';

dotenv.config();

const {
  DB_USER, DB_PASSWORD, DB_NAME, TEST_DB_NAME, DB_HOST, DB_PORT, DB_TYPE, NODE_ENV,
} = process.env;

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    port: Number(DB_PORT),
    dialect: DB_TYPE,
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: TEST_DB_NAME || `${DB_NAME}-${NODE_ENV}`,
    host: DB_HOST,
    port: Number(DB_PORT),
    dialect: DB_TYPE,
  },
  production: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    port: Number(DB_PORT),
    dialect: DB_TYPE,
  },
};
