import { QueryInterface } from 'sequelize';

const permissions = ['user', 'role', 'resource', 'permission', 'translation', 'notification', 'message', 'query', 'private-file', 'constant'];

export const up = async (queryInterface: QueryInterface) => {
  await queryInterface.bulkInsert('permissions', permissions.map((permission) => ({ id: permission })));
};
export const down = async (queryInterface: QueryInterface, Sequelize: any) => {
  const { Op } = Sequelize;
  await queryInterface.bulkDelete('permissions',
    {
      id: {
        [Op.in]: permissions,
      },
    });
};
