import { Sequelize, DataTypes, QueryInterface } from 'sequelize';

export const up = async (queryInterface: QueryInterface) => {
  await queryInterface.createTable('queries', {
    id: {
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      type: DataTypes.BIGINT,
    },
    type: {
      allowNull: false,
      type: DataTypes.ENUM('userId'),
    },
    content: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    variable: {
      allowNull: true,
      type: DataTypes.JSON,
    },
    createdAt: {
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      type: DataTypes.DATE,
    },
    updatedAt: {
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      type: DataTypes.DATE,
    },
  });
};
export const down = async (queryInterface: QueryInterface) => {
  await queryInterface.dropTable('queries');
};
