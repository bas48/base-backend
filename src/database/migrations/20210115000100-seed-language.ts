import { Op, QueryInterface } from 'sequelize';

const languages = [
  {
    id: 'en-US',
    name: 'English(United States)',
  },
  {
    id: 'zh-TW',
    name: '中文（台灣）',
  },
  {
    id: 'zh-HK',
    name: '中文（香港）',
  },
  {
    id: 'zh-CN',
    name: '中文（中国）',
  },
];
export const up = async (queryInterface: QueryInterface) => {
  await queryInterface.bulkInsert('languages', languages);
};
export const down = async (queryInterface: QueryInterface) => {
  await queryInterface.bulkDelete('languages', { id: { [Op.in]: languages.map((language) => language.id) } });
};
