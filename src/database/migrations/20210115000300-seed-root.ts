import { QueryInterface } from 'sequelize';

const { bcrypt, sha256 } = require('../../utils/encryptions');

export const up = async (queryInterface: QueryInterface, Sequelize: any) => {
  const { QueryTypes } = Sequelize;
  const { SELECT } = QueryTypes;
  const password = await bcrypt(sha256('1234'));
  await queryInterface.bulkInsert('users', [
    {
      id: 'root',
      name: 'root',
      password,
    },
  ]);
  // Insert role permission
  const permissions = await queryInterface.sequelize.query('SELECT id FROM permissions', { type: SELECT });
  await queryInterface.bulkInsert('user_permissions', permissions.map((permission: any) => ({
    permissionId: permission.id,
    userId: 'root',
    right: 'write',
  })));
};
export const down = async (queryInterface: QueryInterface) => {
  await queryInterface.bulkDelete('users', { id: 'root' });
};
