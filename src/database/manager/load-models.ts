import path from 'path';
import fileAction from '@actions/file';

const modelPath = path.resolve(__dirname, '../models');

const models = fileAction.loadFolderFiles(modelPath);

process.stdout.write('loaded model:');
Object.keys(models).forEach((modelName) => {
  process.stdout.write(`${modelName} `);
});
process.stdout.write('\n');

export default models;
