import 'module-alias/register';
import seederAction from '@actions/seeder';
import { QueryInterface } from 'sequelize';

const model = 'Constant';
const modelDefine = 'constants';
const primaryKey = 'key';
const records = [
  {
    key: 'test_1',
    value: '5',
    type: 'number',
    translations: {
      'en-US': {
        title: 'Test 1',
      },
      'zh-TW': {
        title: 'Test 1 TW',
      },
    },
  },
  {
    key: 'test_2',
    value: 'true',
    type: 'boolean',
    translations: {
      'en-US': {
        title: 'Test 2',
      },
      'zh-TW': {
        title: 'Test 2 TW',
      },
    },
  },
  {
    key: 'test_3',
    value: 'abc',
    type: 'string',
    translations: {
      'en-US': {
        title: 'Test 3',
      },
      'zh-TW': {
        title: 'Test 3 TW',
      },
    },
  },
];

export const up = async (queryInterface: QueryInterface) => seederAction.insertRecords(queryInterface, model, modelDefine, primaryKey, records);
export const down = async (queryInterface: QueryInterface, Sequelize: any) => seederAction.removeRecords(queryInterface, Sequelize, model, primaryKey, records);
