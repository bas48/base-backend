import { UserInstance } from '@database/models/User';
import FilePathHandler from '@database/utils/file-path';
import { folderPaths } from '@utils/folder/paths';

const userIconHandler = new FilePathHandler(folderPaths.userIcon, { width: 600, height: 600 });

const hooks = {
  beforeCreate: async (instance: UserInstance) => {
    if (instance.icon) {
      const { display } = await userIconHandler.transfer(instance.icon);
      instance.icon = display;
    }
  },
  beforeBulkCreate: async (instances: UserInstance[]) => {
    for (let i = 0; i < instances.length; i += 1) {
      const { display } = await userIconHandler.transfer(instances[i].icon);
      instances[i].icon = display;
    }
  },
  beforeUpdate: async (instance: UserInstance | any) => {
    const { _previousDataValues } = instance;
    const prevIcon = _previousDataValues.icon;
    if (instance.icon !== prevIcon) {
      if (instance.icon) {
        const { display } = await userIconHandler.transfer(instance.icon);
        instance.icon = display;
      }
      if (prevIcon) {
        try {
          await userIconHandler.remove(prevIcon);
        } catch (error) {
          console.log('Remove file error:', error);
        }
      }
    }
  },
  afterDestroy: async (instance: UserInstance) => {
    try {
      await userIconHandler.remove(instance.icon);
    } catch (error) {
      console.log('Remove file error:', error);
    }
  },
};

export default hooks;
