import { TranslationInstance } from '@database/models/Translation';

const hooks = {
  afterFind: (instances: TranslationInstance | TranslationInstance[]) => {
    if (Array.isArray(instances)) {
      for (let i = 0; i < instances.length; i += 1) {
        const { textContent } = instances[i];
        if (textContent) {
          instances[i].content = textContent;
        }
        instances[i].textContent = null;
      }
    } else if (instances) {
      if (instances.textContent) {
        instances.content = instances.textContent;
      }
      instances.textContent = null;
    }
  },
  beforeCreate: (instance: TranslationInstance) => {
    if (instance?.content?.length > 255) {
      instance.textContent = instance.content;
      instance.content = null;
    }
  },
  beforeBulkCreate: (instances: TranslationInstance[]) => {
    for (let i = 0; i < instances.length; i += 1) {
      if (instances[i].content?.length > 255) {
        instances[i].textContent = instances[i].content;
        instances[i].content = null;
      }
    }
  },
  beforeUpdate: (instance: TranslationInstance) => {
    const { content, textContent } = instance;
    if (content?.length > 255) {
      instance.textContent = content;
      instance.content = null;
    } else if (textContent?.length <= 255) {
      instance.content = textContent;
      instance.textContent = null;
    } else if (content) {
      instance.textContent = null;
    } else if (textContent) {
      instance.content = null;
    }
  },
};

export default hooks;
