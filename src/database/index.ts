import { Sequelize } from 'sequelize';
import '@utils/config';
import logger from '@utils/logger';

const {
  NODE_ENV, DB_TYPE, DB_HOST, DB_PORT, DB_NAME, TEST_DB_NAME, DB_USER, DB_PASSWORD, DB_POOL_MAX,
} = process.env;

const sequelize = new Sequelize({
  dialect: DB_TYPE === 'mysql' || DB_TYPE === 'postgres' || DB_TYPE === 'sqlite' || DB_TYPE === 'mariadb' || DB_TYPE === 'mssql' ? DB_TYPE : 'mysql',
  host: DB_HOST,
  port: Number(DB_PORT),
  username: DB_USER,
  password: DB_PASSWORD,
  database: NODE_ENV === 'test' ? TEST_DB_NAME : DB_NAME,
  logging: (msg) => logger.info(msg),
  pool: {
    max: Number(DB_POOL_MAX || 5),
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

export default sequelize;
