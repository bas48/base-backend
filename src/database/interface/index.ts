import { ModelCtor } from 'sequelize/types';
import { ConstantInstance } from '@database/models/Constant';
import { FCMInstance } from '@database/models/FCM';
import { IdManagerInstance } from '@database/models/IdManager';
import { LanguageInstance } from '@database/models/Language';
import { MessageInstance } from '@database/models/Message';
import { NotificationInstance } from '@database/models/Notification';
import { NotificationContentInstance } from '@database/models/NotificationContent';
import { NotificationTargetInstance } from '@database/models/NotificationTarget';
import { PermissionInstance } from '@database/models/Permission';
import { QueryInstance } from '@database/models/Query';
import { ResourceInstance } from '@database/models/Resource';
import { RoleInstance } from '@database/models/Role';
import { RolePermissionInstance } from '@database/models/RolePermission';
import { SessionInstance } from '@database/models/Session';
import { SessionPermissionInstance } from '@database/models/SessionPermission';
import { SmsCountInstance } from '@database/models/SmsCount';
import { SocialInstance } from '@database/models/Social';
import { TranslationInstance } from '@database/models/Translation';
import { UserInstance } from '@database/models/User';
import { UserRoleInstance } from '@database/models/UserRole';
import { VerificationInstance } from '@database/models/Verification';

export interface Models {
  Constant: ModelCtor<ConstantInstance>,
  FCM: ModelCtor<FCMInstance>,
  IdManager: ModelCtor<IdManagerInstance>,
  Language: ModelCtor<LanguageInstance>,
  Message: ModelCtor<MessageInstance>,
  Notification: ModelCtor<NotificationInstance>,
  NotificationContent: ModelCtor<NotificationContentInstance>,
  NotificationTarget: ModelCtor<NotificationTargetInstance>,
  Permission: ModelCtor<PermissionInstance>,
  Query: ModelCtor<QueryInstance>,
  Resource: ModelCtor<ResourceInstance>,
  Role: ModelCtor<RoleInstance>,
  RolePermission: ModelCtor<RolePermissionInstance>,
  Session: ModelCtor<SessionInstance>,
  SessionPermission: ModelCtor<SessionPermissionInstance>,
  SmsCount: ModelCtor<SmsCountInstance>,
  Social: ModelCtor<SocialInstance>,
  Translation: ModelCtor<TranslationInstance>,
  User: ModelCtor<UserInstance>,
  UserRole: ModelCtor<UserRoleInstance>,
  Verification: ModelCtor<VerificationInstance>,
  [modelName: string]: ModelCtor<any>,
}
