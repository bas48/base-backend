/* eslint-disable camelcase */
import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Language from '@database/models/Language';
import { SocialInstance } from '@database/models/Social';
import { FCMInstance } from '@database/models/FCM';
import { SessionInstance } from '@database/models/Session';
import { UserPermissionInstance } from '@database/models/UserPermission';
import { UserRoleInstance } from '@database/models/UserRole';
import hooks from '@database/hooks/user';

export interface UserInstance extends Model {
  id: string;
  icon: string;
  name: string;
  email: string;
  phone: string;
  password: string;
  twoFAEnable: boolean;
  twoFASecret: string;
  banned: boolean;
  languageId: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  socials?: SocialInstance[];
  fcms?: FCMInstance[];
  sessions?: SessionInstance[];
  user_permissions?: UserPermissionInstance[];
  user_roles?: UserRoleInstance[];
}

const User = sequelize.define<UserInstance>('users', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  icon: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  name: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  email: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  phone: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  password: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  twoFAEnable: {
    allowNull: true,
    defaultValue: false,
    type: DataTypes.BOOLEAN,
  },
  twoFASecret: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  banned: {
    allowNull: false,
    defaultValue: false,
    type: DataTypes.BOOLEAN,
  },
  languageId: {
    allowNull: true,
    type: DataTypes.STRING,
    references: {
      model: 'languages',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  deletedAt: {
    allowNull: true,
    type: DataTypes.DATE,
  },
}, {
  paranoid: true,
  defaultScope: {
    attributes: { exclude: ['password', 'twoFASecret'] },
  },
  scopes: {
    all: {},
  },
  hooks,
});

Language.hasMany(User, {
  foreignKey: 'languageId',
  sourceKey: 'id',
});
User.belongsTo(Language, {
  foreignKey: 'languageId',
  targetKey: 'id',
});

export default User;
