import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import User, { UserInstance } from '@database/models/User';

export interface SocialInstance extends Model {
  userId: string;
  type: 'facebook' | 'google';
  referenceId: string;
  createdAt: Date;
  updatedAt: Date;
  user?: UserInstance;
}

const Social = sequelize.define<SocialInstance>('socials', {
  userId: {
    allowNull: false,
    type: DataTypes.STRING,
    primaryKey: true,
    references: {
      model: 'users',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  type: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.ENUM('facebook', 'google'),
  },
  referenceId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

User.hasMany(Social, {
  foreignKey: 'userId',
  sourceKey: 'id',
});
Social.belongsTo(User, {
  foreignKey: 'userId',
  targetKey: 'id',
});

export default Social;
