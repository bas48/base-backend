import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Language, { LanguageInstance } from '@database/models/Language';
import hooks from '@database/hooks/translation';

export interface TranslationInstance extends Model {
  model: string;
  referenceId: string;
  languageId: string;
  field: string;
  content: string;
  textContent: string;
  variable: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  language?: LanguageInstance;
}

const Translation = sequelize.define<TranslationInstance>('translations', {
  model: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  referenceId: {
    allowNull: true,
    primaryKey: true,
    defaultValue: 'null',
    type: DataTypes.STRING,
  },
  languageId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
    references: {
      model: 'languages',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  field: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  content: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  textContent: {
    allowNull: true,
    type: DataTypes.TEXT,
  },
  variable: {
    allowNull: true,
    type: DataTypes.STRING,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  deletedAt: {
    allowNull: true,
    type: DataTypes.DATE,
  },
}, {
  paranoid: true,
  hooks,
});

Translation.belongsTo(Language, {
  foreignKey: 'languageId',
  targetKey: 'id',
});
Language.hasMany(Translation, {
  foreignKey: 'languageId',
  sourceKey: 'id',
});

export default Translation;
