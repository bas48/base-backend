/* eslint-disable camelcase */
import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import { NotificationContentInstance } from '@database/models/NotificationContent';

export interface MessageInstance extends Model {
  id: string;
  isSystem: boolean;
  handlers: string[];
  createdAt: Date;
  updatedAt: Date;
  notification_contents?: NotificationContentInstance[];
}

const Message = sequelize.define<MessageInstance>('messages', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
  },
  isSystem: {
    allowNull: false,
    defaultValue: false,
    type: DataTypes.BOOLEAN,
  },
  handlers: {
    allowNull: true,
    type: DataTypes.STRING,
    get() {
      const value = this.getDataValue('handlers');
      return value?.split(';');
    },
    set(value: string[]) {
      if (value?.length) {
        this.setDataValue('handlers', value.join(';'));
      }
    },
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Message;
