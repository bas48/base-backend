import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Permission, { PermissionInstance } from '@database/models/Permission';
import User, { UserInstance } from '@database/models/User';

export interface UserPermissionInstance extends Model {
  permissionId: string;
  userId: string;
  right: 'read' | 'write' | 'banned';
  createdAt: Date;
  updatedAt: Date;
  permission?: PermissionInstance;
  user?: UserInstance;
}

const UserPermission = sequelize.define<UserPermissionInstance>('user_permissions', {
  permissionId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
    references: {
      model: 'permissions',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  userId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING,
    references: {
      model: 'users',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  right: {
    allowNull: false,
    type: DataTypes.ENUM('read', 'write', 'banned'),
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

Permission.hasMany(UserPermission, {
  foreignKey: 'permissionId',
  sourceKey: 'id',
});
UserPermission.belongsTo(Permission, {
  foreignKey: 'permissionId',
  targetKey: 'id',
});
User.hasMany(UserPermission, {
  foreignKey: 'userId',
  sourceKey: 'id',
});
UserPermission.belongsTo(User, {
  foreignKey: 'userId',
  targetKey: 'id',
});

export default UserPermission;
