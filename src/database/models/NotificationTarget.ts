import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import Notification, { NotificationInstance } from '@database/models/Notification';

export interface NotificationTargetInstance extends Model {
  notificationId: number;
  type: 'all' | 'all_user' | 'all_guest' | 'user' | 'fcm_key' | 'role';
  reference: JSON;
  createdAt: Date;
  updatedAt: Date;
  notification?: NotificationInstance;
}

const NotificationTarget = sequelize.define<NotificationTargetInstance>('notification_targets', {
  notificationId: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.BIGINT,
    references: {
      model: 'notifications',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  },
  type: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.ENUM('all', 'all_user', 'all_guest', 'user', 'fcm_key', 'role'),
  },
  reference: {
    allowNull: true,
    type: DataTypes.JSON,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

Notification.hasMany(NotificationTarget, {
  foreignKey: 'notificationId',
  sourceKey: 'id',
});
NotificationTarget.belongsTo(Notification, {
  foreignKey: 'notificationId',
  targetKey: 'id',
});

export default NotificationTarget;
