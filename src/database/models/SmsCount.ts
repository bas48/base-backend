import { DataTypes, Model } from 'sequelize';
import sequelize from '@database';

export interface SmsCountInstance extends Model {
  id: number;
  provider: string;
  count: number;
  date: Date;
}

const SmsCount = sequelize.define<SmsCountInstance>('sms_count', {
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.INTEGER,
  },
  provider: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  count: {
    allowNull: false,
    type: DataTypes.INTEGER,
  },
  date: {
    allowNull: false,
    type: DataTypes.DATEONLY,
  },
});

export default SmsCount;
