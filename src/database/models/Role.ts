/* eslint-disable camelcase */
import { Sequelize, DataTypes, Model } from 'sequelize';
import sequelize from '@database';
import { RolePermissionInstance } from '@database/models/RolePermission';
import { UserRoleInstance } from '@database/models/UserRole';

export interface RoleInstance extends Model {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  role_permissions?: RolePermissionInstance[];
  user_roles?: UserRoleInstance[];
}

const Role = sequelize.define<RoleInstance>('roles', {
  id: {
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    type: DataTypes.BIGINT,
  },
  createdAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
  updatedAt: {
    allowNull: false,
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    type: DataTypes.DATE,
  },
});

export default Role;
