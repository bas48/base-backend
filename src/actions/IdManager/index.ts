import sequelize from '@database';
import IdManager from '@database/models/IdManager';

export default {
  pad(num: number, size: number): string {
    let s = num.toString();
    while (s.length < size) s = '0'.concat(s);
    return s;
  },

  getIdHead(): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = 0; i < 2; i += 1) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  },

  async getNewId(): Promise<string> {
    const newId = await sequelize.transaction(async (transaction) => {
      const idHead = this.getIdHead();
      const [idManager]: any = await IdManager.findOrCreate({
        where: {
          idHead,
        },
        defaults: {
          idHead,
        },
        transaction,
      });
      const id = idHead + this.pad(idManager.idBody, 5);
      await idManager.increment({ idBody: 1 }, { transaction });
      return id;
    });
    return newId;
  },
};
