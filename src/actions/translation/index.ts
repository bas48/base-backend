import { RemoteCache } from '@utils/cache';
import { convertObjToTrueObj } from '@utils/object-convertor';
import { Context } from '@utils/interface';
import { Transaction, WhereOptions } from 'sequelize/types';
import Translation, { TranslationInstance } from '@database/models/Translation';

export interface FilterType {
  [key: string]: any,
}

export interface TranslationContent {
  [key: string]: string;
}

export interface TranslationType {
  [key: string]: TranslationContent;
}

export interface RecordType {
  model: string,
  field: string,
  referenceId: Array<string>,
  languageId: string,
  variable?: string,
  textContent?: string,
  content?: string,
}

// const supportedPlatform: string[] = process.env.SUPPORTED_PLATFORM!.split(',');
// RemoteCache.subscribe('translation-default-source', supportedPlatform);
// RemoteCache.subscribe('translation-db-source', supportedPlatform);

export default {
  async setDefaultTranslations(platform: string, source: any): Promise<void> {
    const convertedTranslations = convertObjToTrueObj('', source);
    await RemoteCache.set('translation-default-source', platform, convertedTranslations);
  },

  async getDefaultTranslations(platforms: string[]): Promise<string> {
    const defaultTranslations: any = {};
    for (const platform of platforms) {
      const defaultTranslation = await RemoteCache.get('translation-default-source', platform);
      if (defaultTranslation) {
        defaultTranslations[platform] = defaultTranslation;
      }
    }
    return defaultTranslations;
  },

  async getList(ctx: Context, page: number, orderBy: string, direction: string, limit: number, search?: string, filter: { platform?: string, field?: string} = {}) {
    let selectedPlatform: string[] = process.env.SUPPORTED_PLATFORM!.split(',');
    if (filter.platform) {
      selectedPlatform = [filter.platform];
    }
    const dbSources = await this.getDBSource(selectedPlatform);
    const defaultSources: { [key: string]: any} = await this.getDefaultSource(selectedPlatform);
    const sourceObj: { [key: string]: any} = {};
    for (const platform of Object.keys(defaultSources)) {
      sourceObj[platform] = {};
      const searchedKeys: { [key: string]: any} = {};
      if (search) {
        for (const key of Object.keys(defaultSources[platform])) {
          const keyArray = key.split('.');
          const field = keyArray.splice(1, keyArray.length - 1).join('.');
          const defaultValue = typeof defaultSources[platform][key] === 'string' ? defaultSources[platform][key] : '';
          const dbValue = dbSources[platform]?.[key] && typeof dbSources[platform][key] === 'string' ? dbSources[platform][key] : '';
          if (defaultValue.includes(search) || dbValue.includes(search)) {
            searchedKeys[field] = true;
          }
        }
      }
      for (const key of Object.keys(defaultSources[platform])) {
        const keyArray = key.split('.');
        const languageId = keyArray[0];
        const field = keyArray.splice(1, keyArray.length - 1).join('.');
        if ((!search || searchedKeys[field]) && (!filter.field || filter.field === field)) {
          if (!sourceObj[platform][field]) {
            sourceObj[platform][field] = {};
          }
          sourceObj[platform][field][languageId] = {};
          sourceObj[platform][field][languageId].customized = false;
          sourceObj[platform][field][languageId].value = dbSources?.[platform]?.[key] || defaultSources[platform][key];
          const variables = [];
          const regx = /{{(.*?)}}/g;
          let variable = regx.exec(defaultSources[platform][key]);
          while (variable) {
            variables.push(variable[1]);
            variable = regx.exec(defaultSources[platform][key]);
          }
          regx.lastIndex = 0;
          if (variables.length) {
            sourceObj[platform][field][languageId].variable = variables.toString();
          }
        }
      }
    }
    const listRecords = [];

    for (const platform of Object.keys(sourceObj)) {
      for (const field of Object.keys(sourceObj[platform])) {
        const record = { platform, field, content: sourceObj[platform][field] };
        listRecords.push(record);
      }
    }
    listRecords.sort((a: any, b: any) => {
      if (direction === 'ASC') {
        return (a[orderBy] > b[orderBy]) ? 1 : -1;
      }
      return (a[orderBy] < b[orderBy]) ? 1 : -1;
    });
    const count = listRecords.length;
    const start = (page - 1) * limit;
    let end = page * limit;
    if (end > count) {
      end = count;
    }
    return {
      count,
      rows: listRecords.slice(start, end),
    };
  },

  getContentVariables(content: any): string | undefined {
    if (!content) {
      return undefined;
    }
    return (content.match(/{{([\w]+)}}/igm) || []).map((v: string) => v.substr(2, v.length - 4)).join(',');
  },

  async upsertTranslation(ctx: Context, filter: FilterType, translations: TranslationType, transaction: Transaction): Promise<TranslationType> {
    const newRecords = [];
    const existRecords = await Translation.findAll({ where: filter, transaction, paranoid: false });
    for (const languageId of Object.keys(translations)) {
      const languageValue = translations[languageId];
      for (const field of Object.keys(languageValue)) {
        const content: string = languageValue[field];
        const variable = this.getContentVariables(content);
        let created = false;
        for (const existRecord of existRecords) {
          if (existRecord.deletedAt) {
            await existRecord.restore();
          }
          if (existRecord.languageId === languageId && existRecord.field === field) {
            created = true;
            existRecord.content = content;
            if (variable) {
              existRecord.variable = variable;
            }
            await existRecord.save({ transaction });
          }
        }
        if (!created) {
          newRecords.push({
            model: filter.model,
            field,
            referenceId: filter.referenceId,
            languageId,
            content,
            variable,
          });
        }
      }
    }
    await Translation.bulkCreate(newRecords, { transaction });
    return translations;
  },

  async updateDefaultTranslationSource(platform: string, source: any) {
    const convertedSource = convertObjToTrueObj(undefined, source);
    // console.log('Set Default Source:', platform, convertedSource);
    RemoteCache.set('translation-default-source', platform, convertedSource);
  },

  translationHandler(record: { [key: string]: any, translations ?: TranslationInstance[] }, presetLanguageId ?: string): {
    [x: string]: any;
    translations?: TranslationInstance[] | undefined;
  } {
    const { translations } = record;
    const newRecord = { ...record };
    delete newRecord.translations;
    if (translations) {
      for (const translation of translations) {
        const { languageId, field, content, textContent, variable } = translation;
        if (!newRecord[field]) {
          newRecord[field] = {};
        }
        if (presetLanguageId && presetLanguageId === languageId) {
          newRecord[field] = content || textContent;
        } else if (!presetLanguageId) {
          newRecord[field][languageId] = {};
          newRecord[field][languageId].value = content || textContent;
          newRecord[field][languageId].variable = variable;
        }
      }
    }
    return newRecord;
  },

  convertTranslationRecords(model: string, referenceId: Array<string>, translations: TranslationType, textFilter: boolean): RecordType[] {
    const translationRecords = [];
    for (const languageId of Object.keys(translations)) {
      const languageValue = translations[languageId];
      for (const field of Object.keys(languageValue)) {
        const content = languageValue[field];
        const variable = this.getContentVariables(content);
        const record: RecordType = {
          model,
          field,
          referenceId,
          languageId,
          variable,
        };
        if (textFilter && content.length > 255) {
          record.textContent = content;
        } else {
          record.content = content;
        }
        translationRecords.push(record);
      }
    }
    return translationRecords;
  },

  async deleteTranslation(filter: WhereOptions, transaction: Transaction) {
    await Translation.destroy({ where: filter, transaction });
  },

  async getDefaultSource(platforms: string[]) {
    const defaultSources: { [key: string]: any} = {};
    for (const platform of platforms) {
      const defaultSource = await RemoteCache.get('translation-default-source', platform);
      if (defaultSource) {
        defaultSources[platform] = defaultSource;
      }
    }
    return defaultSources;
  },

  async getDBSource(selectedPlatform: string[], byRedis: boolean = true) {
    const dbSources: any = {};
    const unsavedPlatform = byRedis ? [] : selectedPlatform;
    if (byRedis) {
      for (let i = 0; i < selectedPlatform.length; i += 1) {
        const source = await RemoteCache.get('translation-db-source', selectedPlatform[i]);
        if (source) {
          dbSources[selectedPlatform[i]] = source;
        } else {
          unsavedPlatform.push(selectedPlatform[i]);
        }
      }
    }
    if (unsavedPlatform.length) {
      const dbRecords = await Translation.findAll({
        where: {
          model: unsavedPlatform,
        },
      });
      for (const platform of unsavedPlatform) {
        dbSources[platform] = {};
        for (const dbRecord of dbRecords) {
          const { languageId, field, content } = dbRecord;
          if (platform === dbRecord.model) {
            dbSources[platform][`${languageId}.${field}`] = content;
          }
        }
      }
      RemoteCache.mset('translation-db-source', dbSources);
    }
    return dbSources;
  },
};
