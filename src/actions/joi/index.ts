import { Schema } from 'joi';
import { InvalidValidation } from '@utils/errors';
import { getTranslationSource } from '@utils/translation';
import { convertObjToTrueObj } from '@utils/object-convertor';
import { Context } from '@utils/interface';

export default {
  async validate(ctx: Context, languageId: string, schema: Schema, data = {}, throwError = true) {
    const { translate } = ctx;
    const translation = await getTranslationSource(languageId);
    const language = (translation?.joi_errors) || {};
    const convertedObj = convertObjToTrueObj('', language);
    try {
      const { error, value } = schema.validate(data, {
        errors: {
          escapeHtml: true,
        },
        messages: convertedObj,
        abortEarly: false,
        stripUnknown: true,
      });
      if (error) {
        throw error;
      }
      return { isValid: true, validatedData: value };
    } catch (joiError) {
      const oriDetail = joiError.details;
      const details = [];
      if (oriDetail?.length) {
        for (const detail of oriDetail) {
          const { path, message } = detail;
          let customMsg = message;
          for (const key of path) {
            customMsg = customMsg.replace(key, translate(key));
          }
          details.push(customMsg);
        }
      }
      if (throwError) {
        throw new InvalidValidation({
          errors: details,
        });
      }
      return { isValid: false, errorDetail: details };
    }
  },
};
