import { RemoteCache } from '@utils/cache';
import { Context } from '@utils/interface';
import Resource, { ResourceInstance } from '@database/models/Resource';
import fileAction from '@actions/file';
import { NotFoundError } from '@utils/errors';
import { Transaction } from 'sequelize/types';

interface dataType {
  [key: string]: any;
}

export default {
  async updateResource(ctx: Context, resourceId: string, data: dataType, transaction: Transaction): Promise<ResourceInstance | null> {
    const resource = await Resource.findOne({
      where: {
        id: resourceId,
      },
      // throwError: true,
      transaction,
    });
    if (data.permissions && resource) {
      await RemoteCache.del('resource-permission', `${resource.method}-${resource.path}`);
    }
    Object.assign(resource, data);
    if (resource) {
      await resource.save({ transaction });
    }
    return resource;
  },

  async deleteResource(ctx: Context, resourceId: string, transaction: Transaction): Promise<void> {
    const { translate } = ctx;
    const resource = await Resource.findOne({
      where: {
        id: resourceId,
      },
      // throwError: true,
      transaction,
    });
    if (!resource) {
      throw new NotFoundError({ resource: translate('resource') });
    }
    if (resource.type === 'file') {
      await fileAction.removeFile(ctx, resource.path);
    }
    await resource.destroy({ transaction });
  },
};
