import firebaseAdmin from '@lib/firebase';

export default {
  async pushMessage(tokens: string[], data: any) {
    const response = await firebaseAdmin.messaging().sendMulticast({
      data,
      tokens,
    });
    if (response.failureCount > 0) {
      const successTokens: string[] = [];
      const failedTokens: string[] = [];
      response.responses.forEach((resp, idx) => {
        if (!resp.success) {
          failedTokens.push(tokens[idx]);
        } else {
          successTokens.push(tokens[idx]);
        }
      });
      return {
        successTokens,
        failedTokens,
      };
    }
    return { successTokens: tokens };
  },
};
