/* eslint-disable no-undef */
import axios from 'axios';
import path from 'path';
import { createHmac } from 'crypto';
import { OAuth2Client } from 'google-auth-library';
import { Transaction } from 'sequelize/types';
import { SocialLoginError, InternalServerError, VerificationRequired } from '@utils/errors';
import userAction from '@actions/user';
import fileAction from '@actions/file';
import verificationAction from '@actions/verification';
import { rootPath, folderPaths } from '@utils/folder/paths';
import { HttpContext } from '@http/interface';

const { temporaryImage } = folderPaths;
const { FACEBOOK_APP_SECRET, GOOGLE_CLIENT_ID } = process.env;

export default {
  google: async (ctx: HttpContext, accessToken: string, transaction: Transaction) => {
    const { models, request } = ctx;
    const { query } = request;
    const { User, Social } = models;
    let res;
    try {
      const client = new OAuth2Client(GOOGLE_CLIENT_ID);
      const ticket = await client.verifyIdToken({
        idToken: accessToken,
        audience: GOOGLE_CLIENT_ID,
      });
      res = ticket.getPayload();
    } catch (error) {
      const { message } = error;
      throw new SocialLoginError({
        message,
      });
    }
    let user;
    // eslint-disable-next-line camelcase
    const { sub, email_verified, email, name, picture } = res;
    const social = await Social.findOne({
      where: {
        type: 'google',
        referenceId: sub,
      },
      include: [
        {
          model: User,
          required: true,
        },
      ],
      transaction,
    });
    if (social) {
      // eslint-disable-next-line prefer-destructuring
      user = social.user;
    } else {
      // eslint-disable-next-line camelcase
      if (email_verified) {
        user = await User.findOne({
          where: {
            email,
          },
          transaction,
        });
        if (user) {
          const { emailToken } = query;
          if (!emailToken) {
            throw new VerificationRequired({
              missingToken: [{
                type: 'email',
                reference: email,
                verifyRequired: true,
              }],
            });
          }
          await verificationAction.checkVerificationRecord(ctx, Array.isArray(emailToken) ? emailToken[0] : emailToken, email, transaction);
        } else {
          const icon = await fileAction.download(picture, path.resolve(rootPath, temporaryImage));
          user = await userAction.createUser({ name, icon, email }, transaction);
        }
      } else {
        const icon = await fileAction.download(picture, path.resolve(rootPath, temporaryImage));
        user = await userAction.createUser({ name, icon }, transaction);
      }
      await Social.create({
        userId: user.id,
        type: 'google',
        referenceId: res.sub,
      }, { transaction });
    }
    return user;
  },

  async facebook(ctx: HttpContext, accessToken: string, transaction: Transaction) {
    const { models, query } = ctx;
    const { User, Social } = models;
    let res;
    const appsecretProof = createHmac('sha256', FACEBOOK_APP_SECRET).update(accessToken).digest('hex');
    try {
      res = (await axios.get('https://graph.facebook.com/v2.12/me', {
        params: {
          fields: 'first_name,last_name,email,verified',
          access_token: accessToken,
          appsecret_proof: appsecretProof,
        },
      })).data;
    } catch (error) {
      const { response } = error;

      if (response) {
        const { message, type, code } = response?.data?.error;
        throw new SocialLoginError({
          message,
          type,
          code,
        });
      } else {
        throw new InternalServerError(error);
      }
    }

    let user;
    // eslint-disable-next-line camelcase
    const { id, email, first_name, last_name } = res;
    const social = await Social.findOne({
      where: {
        type: 'facebook',
        referenceId: id,
      },
      include: [
        {
          model: User,
          required: true,
        },
      ],
      transaction,
    });
    if (social) {
      // eslint-disable-next-line prefer-destructuring
      user = social.user;
    } else {
      user = await User.findOne({
        where: {
          email,
        },
        transaction,
      });
      if (user) {
        const { emailToken } = query;
        if (!emailToken) {
          throw new VerificationRequired({
            missingToken: [{
              type: 'email',
              reference: email,
              verifyRequired: true,
            }],
          });
        }
        await verificationAction.checkVerificationRecord(ctx, Array.isArray(emailToken) ? emailToken[0] : emailToken, email, transaction);
      } else {
        const pictureRes = (await axios.get(`https://graph.facebook.com/v2.12/me/picture?redirect=false&type=large&access_token=${accessToken}`)).data.data;
        const icon = await fileAction.download(pictureRes.url, path.resolve(rootPath, temporaryImage));
        // eslint-disable-next-line camelcase
        user = await userAction.createUser({ name: `${first_name} ${last_name}`, icon, email }, transaction);
      }
      await Social.create({
        userId: user.id,
        type: 'facebook',
        referenceId: id,
      }, { transaction });
    }
    return user;
  },
};
