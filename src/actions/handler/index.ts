import path from 'path';
import Joi, { Schema } from 'joi';
import fileAction from '@actions/file';
import joiAction from '@actions/joi';
import { NotFoundError } from '@utils/errors';
import { Context } from '@utils/interface';

export interface Handler {
  params?: { [key: string]: Schema };
  returnKeys: string[];
  // eslint-disable-next-line no-unused-vars
  validate?: (ctx: Context, value: any) => void;
  // eslint-disable-next-line no-unused-vars
  call: (ctx: Context, params: any) => { [key: string]: string };
}

export interface HandlerDefine {
  id: string;
  title: string;
  handlerType: string;
  returnKeys: string[];
  params: {
    [key: string]: {
      title: string,
      type: string,
    },
  },
}

export interface HandlerContainer {
  [type: string]: Handler;
}

const currentFolder = path.join(__dirname, '');
const handlerContainers: { [type: string]: HandlerContainer } = fileAction.loadFolderFiles(currentFolder, ['index.ts', 'index.js']);

export default {
  getHandlers: (ctx: Context, selectedType?: string) => {
    const { translate } = ctx;
    const selectedHandlers = [];
    let handlerTypes = Object.keys(handlerContainers);
    if (selectedType) {
      handlerTypes = handlerTypes.filter((handlerType) => handlerType === selectedType);
    }
    for (const handlerType of handlerTypes) {
      const handlerContainer = handlerContainers[handlerType];
      for (const handlerId of Object.keys(handlerContainer)) {
        const handler = handlerContainer[handlerId];
        const { params, returnKeys } = handler;
        const convertedParams: { [key: string]: { title: string, type: any } } = {};
        if (params) {
          for (const key of Object.keys(params)) {
            const paramType = handler.params?.[key]?.type;
            convertedParams[key] = {
              title: translate(`handler:${handlerType}.${handlerId}.params.${key}.title`),
              type: paramType,
            };
          }
        }
        const handlerDefine: HandlerDefine = {
          id: handlerId,
          title: translate(`handler:${handlerType}.${handlerId}.title`),
          handlerType,
          returnKeys,
          params: convertedParams,
        };
        selectedHandlers.push(handlerDefine);
      }
    }
    return selectedHandlers;
  },
  async checkHandlerParams(ctx: Context, languageId: string, handlerType: string, handlerId: string, paramsValue: any, throwError = true) {
    const { translate } = ctx;
    const handler = handlerContainers[handlerType][handlerId];
    if (!handler) {
      if (throwError) {
        throw new NotFoundError({ type: 'handler', resources: translate('handler') });
      }
      return false;
    }
    const { params, validate } = handler;
    if (params) {
      await joiAction.validate(ctx, languageId, Joi.object({ ...params }), paramsValue, throwError);
    }
    if (validate) {
      await validate(ctx, paramsValue);
    }
    return true;
  },
  isHandlerExist(ctx: Context, handlerId: string, selectedType?: string) {
    const { translate } = ctx;
    const handlers = this.getHandlers(ctx, selectedType);
    const found = !!handlers.find((handler: { id: string }) => handler.id === handlerId);
    if (!found) {
      throw new NotFoundError({ type: 'handler', resources: translate('handler') });
    }
  },
};
