import { HandlerContainer } from '@actions/handler';
import { Context } from '@utils/interface';
import Joi from 'joi';

const messageHandlers: HandlerContainer = {
  getFullName: {
    params: {
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
    },
    returnKeys: ['fullName', 'languageId'],
    call: (ctx: Context, params) => {
      const { languageId } = ctx;
      const { firstName, lastName } = params;
      return {
        fullName: `${firstName} ${lastName}`,
        languageId,
      };
    },
  },
};

export default messageHandlers;
