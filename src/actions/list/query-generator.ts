/* eslint-disable no-case-declarations */
import moment from 'moment';
import { QueryTypes } from 'sequelize';
import sequelize from '@database';
import { HttpContext } from '@http/interface';
import { Option, Filter, Join, Condition, TranslationSetting, MainModel } from '@actions/list';
import { Models } from '@database/interface';

interface UsedTables {
  [tableName: string]: boolean;
}

interface SortParam {
  orderBy: string,
  direction: string,
}

const getPrimaryKeys = async (models: Models, model: string) => {
  const primaryKeys = await models[model].describe().then((schema: any) => (
    Object.keys(schema).filter((field) => (schema[field].primaryKey))
  ));
  return primaryKeys;
};

const getSearchQueries = (search: string[]): {
  searchQuery: string, searchUsedTables: UsedTables,
} => {
  const searchUsedTables: UsedTables = {};
  const searchConditions = [];
  for (const searchMap of search) {
    const table = searchMap.split('.')[0];
    const field = searchMap.split('.')[1];
    if (table.includes('Translation')) {
      searchConditions.push(`(${table}.field = '${field}' AND (${table}.content LIKE :query_search OR ${table}.textContent LIKE :query_search))`);
      const originalTable = table.split('Translation')[0];
      searchUsedTables[originalTable] = true;
    } else {
      searchConditions.push(`${searchMap} LIKE :query_search`);
    }
    searchUsedTables[table] = true;
  }
  const searchQuery = `(${searchConditions.join(' OR ')})`;
  return { searchQuery, searchUsedTables };
};

const getFilterQueries = (filterValues: { [key: string]: any }, filter: Filter): {
  filterQueries: string[], filterQueryValues: { [key: string]: string }, filterUsedTables: UsedTables
} => {
  const filterUsedTables: UsedTables = {};
  const filterQueries: string[] = [];
  const filterQueryValues: { [key: string]: string } = {};
  for (const key of Object.keys(filterValues)) {
    const { validator, mapper, action, actions } = filter[key];
    const filterType = validator.describe().type;
    const keyValue = filterValues[key];
    if (actions) {
      const actionMappers = actions(keyValue);
      actionMappers.forEach((actionMapper, index) => {
        const { action: signalAction, value } = actionMapper;
        filterQueryValues[`${key}_${index}`] = value;
        filterQueries.push(`${mapper} ${signalAction} :${key}_${index}`);
      });
    } else {
      switch (filterType) {
        case 'timeRange':
          const from = moment(keyValue.from).format();
          const to = moment(keyValue.to).format();
          filterQueryValues[`${key}_from`] = from;
          filterQueryValues[`${key}_to`] = to;
          filterQueries.push(`${mapper} > :${key}_from AND ${mapper} < :${key}_to`);
          break;
        case 'array':
          let query = `${mapper} IN (`;
          for (let i = 0; i < keyValue.length; i += 1) {
            const arrayKey = `${key}_array_${i}`;
            filterQueryValues[arrayKey] = keyValue[i];
            if (i !== 0) {
              query = `${query},`;
            }
            query = `${query}:${arrayKey}`;
          }
          query = `${query})`;
          filterQueries.push(query);
          break;
        default:
          filterQueryValues[key] = keyValue;
          filterQueries.push(`${mapper} ${action || '='} :${key}`);
          break;
      }
    }
    const usedTable = mapper.split('.')[0];
    if (usedTable.includes('Translation')) {
      const originalTable = usedTable.split('Translation')[0];
      filterUsedTables[originalTable] = true;
    }
    filterUsedTables[usedTable] = true;
  }
  return { filterQueries, filterQueryValues, filterUsedTables };
};

const getSortQuery = (mainKeys: string[], sortParams: SortParam[], sorts?: { [key: string]: string }): {
  sortQuery: string;
  sortUsedTables: UsedTables;
} => {
  let sortCols = [`${mainKeys[0]} ASC`];
  const sortUsedTables: UsedTables = {};
  if (sorts && sortParams) {
    sortCols = sortParams.map((sortParam: SortParam) => {
      const { orderBy, direction } = sortParam;
      const table = sorts[orderBy].split('.')[0];
      if (table.includes('Translation')) {
        const sortTable = `${table}Sort`;
        const oriTable = table.split('Translation')[0];
        sortUsedTables[oriTable] = true;
        sortUsedTables[table] = true;
        sortUsedTables[sortTable] = true;
        return `${sortTable}.content ${direction}`; // removed min
      }
      sortUsedTables[table] = true;
      return `${sorts[orderBy]} ${direction}`; // removed min
    });
  }
  return {
    sortQuery: sortCols.join(','),
    sortUsedTables,
  };
};

const getConditionQuery = async (ctx: HttpContext, condition: Condition): Promise<{
  conditionQuery: string; conditionQueryValues: { [key: string]: any };
}> => {
  const { query, getQueryValues } = condition;
  let conditionQuery = query;
  const conditionQueryValues: { [key: string]: any } = {};
  if (getQueryValues) {
    const response = await getQueryValues(ctx);
    if (response) {
      for (const key of Object.keys(response)) {
        conditionQueryValues[`condition_${key}`] = response[key];
      }
      conditionQuery = query.replace(/:/g, ':condition_');
    }
  }
  return { conditionQuery, conditionQueryValues };
};

const getModelTranslation = async (
  ctx: HttpContext,
  model: string,
  translationSetting: TranslationSetting,
  usedTables: UsedTables,
  sorts?: {
    [key: string]: string;
  },
  sortParams?: SortParam[],
): Promise<{
  translationQuery: string;
  translationSort: string;
  translationQueryValues: { [key: string]: string };
} | null> => {
  const { presetLanguage, required, sourceKey, condition } = translationSetting;
  const transModel = `${model}Translation`;
  const sortModel = `${transModel}Sort`;
  if (required || (required && condition) || usedTables[transModel] || usedTables[sortModel]) {
    const joinType = required ? 'INNER JOIN' : 'LEFT OUTER JOIN';
    let translationQuery: string = `${joinType} translations AS ${transModel} ON ${transModel}.referenceId = ${model}.${sourceKey || 'id'} AND ${transModel}.model = '${model}'`;
    let translationQueryValues: { [key: string]: string } = {};
    let translationSort = '';

    if (presetLanguage) {
      translationQuery = `${translationQuery} AND ${transModel}.languageId = :query_language`;
    }

    if (required && condition) {
      const { conditionQuery, conditionQueryValues } = await getConditionQuery(ctx, condition);
      translationQuery = `${translationQuery} AND ${conditionQuery}`;
      if (conditionQueryValues) {
        translationQueryValues = { ...conditionQueryValues };
      }
    }

    let transField;
    if (sortParams && sorts) {
      sortParams.forEach((sortParam: SortParam) => {
        const { orderBy } = sortParam;
        const selectSortModel = sorts[orderBy].split('.')[0];
        const sortField = sorts[orderBy].split('.')[1];
        if (selectSortModel === transModel) {
          transField = sortField;
        }
      });
    }

    if (usedTables[sortModel]) {
      translationSort = `${translationQuery} AND ${transModel}.field = '${transField}'`;
      if (!presetLanguage) {
        translationSort = `${translationSort} AND ${transModel}.languageId = :query_language`;
      }
      translationSort = translationSort.replace(new RegExp(transModel, 'g'), sortModel);
    }
    return {
      translationQuery,
      translationSort,
      translationQueryValues,
    };
  }
  return null;
};

const getSubJoinQueries = async (ctx: HttpContext, joins: Join[], usedTables: UsedTables, sorts?: { [key: string]: string }, sortParams?: SortParam[], showDeleted?: boolean): Promise<{
  joinQuery: string;
  joinQueryValues: { [key: string]: string },
}> => {
  const { models } = ctx;
  let joinQuery: string = '';
  let joinQueryValues: { [key: string]: string } = {};
  for (const join of joins) {
    const { model, as, condition, required, on, withTranslation, showDeletedEnable } = join;
    if (join.joins) {
      const joinState = await getSubJoinQueries(ctx, join.joins, usedTables, sorts, sortParams, showDeleted);
      joinQuery = joinState.joinQuery;
      if (joinState.joinQueryValues) {
        joinQueryValues = { ...joinQueryValues, ...joinState.joinQueryValues };
      }
    }
    if (withTranslation) {
      const { translationQuery, translationSort, translationQueryValues } = (await getModelTranslation(ctx, model, withTranslation, usedTables, sorts, sortParams)) || {};
      if (translationQuery) {
        joinQuery = `${joinQuery} ${translationQuery}`;
      }
      if (translationSort) {
        joinQuery = `${joinQuery} ${translationSort}`;
      }
      if (translationQueryValues) {
        joinQueryValues = { ...joinQueryValues, ...translationQueryValues };
      }
    }
    if (required || (required && condition) || usedTables[as || model] || joinQuery) {
      const tableName = models[model].getTableName();
      const joinType = (required && 'INNER JOIN') || 'LEFT OUTER JOIN';
      let fromQuery = `${joinType} ${tableName} AS ${as || model} ON ${on.left} = ${on.right}`;
      if (models[model].options.paranoid && !(showDeletedEnable && showDeleted)) {
        fromQuery = `${fromQuery} AND ${as || model}.deletedAt IS NULL`;
      }
      if (required && condition) {
        const { conditionQuery, conditionQueryValues } = await getConditionQuery(ctx, condition);
        fromQuery = `${fromQuery} AND ${conditionQuery}`;
        if (conditionQueryValues) {
          joinQueryValues = { ...joinQueryValues, ...conditionQueryValues };
        }
      }
      joinQuery = `${fromQuery} ${joinQuery}`;
    }
  }
  return {
    joinQuery,
    joinQueryValues,
  };
};

const getJoinQueries = async (ctx: HttpContext, main: MainModel, usedTables: UsedTables, sorts?: { [key: string]: string }, sortParams?: SortParam[], showDeleted ?: boolean): Promise<{
  joinQuery: string;
  joinQueryValues: { [key: string]: string },
  conditionQuery ?: string;
}> => {
  const { models } = ctx;
  const { model, as, joins, withTranslation, showDeletedEnable } = main;
  const tableName = models[model].getTableName();
  let joinQuery: string = `${tableName} AS ${as || model}`;
  const conditionQuery = !models[model].options.paranoid || (showDeletedEnable && showDeleted) ? null : `${as || model}.deletedAt IS NULL`;
  let joinQueryValues: { [key: string]: string } = {};
  if (joins) {
    const joinState = await getSubJoinQueries(ctx, joins, usedTables, sorts, sortParams, showDeleted);
    joinQuery = `${joinQuery} ${joinState.joinQuery}`;
    if (joinState.joinQueryValues) {
      joinQueryValues = { ...joinQueryValues, ...joinState.joinQueryValues };
    }
  }
  if (withTranslation) {
    const { translationQuery, translationSort, translationQueryValues } = (await getModelTranslation(ctx, model, withTranslation, usedTables, sorts, sortParams)) || {};
    if (translationQuery) {
      joinQuery = `${joinQuery} ${translationQuery}`;
    }
    if (translationSort) {
      joinQuery = `${joinQuery} ${translationSort}`;
    }
    if (translationQueryValues) {
      joinQueryValues = { ...joinQueryValues, ...translationQueryValues };
    }
  }
  return {
    joinQuery,
    joinQueryValues,
    conditionQuery,
  };
};

export default async (ctx: HttpContext, option: Option, query: any):
  Promise<{ targets: object[], count?: any }> => {
  const { models, languageId } = ctx;
  const { main, sorts, search, filter, withCount = true, exportSetting } = option;
  const { page, sort: sortParams, limit, search: searchValue, filter: filterValue, showDeleted, exportData } = query;
  const withLimit = limit && !(exportSetting && exportData);
  let queryValues: any = {
    query_language: languageId,
  };
  if (withLimit) {
    queryValues.query_offset = (page - 1) * limit;
    queryValues.query_limit = limit;
  }
  let usedTables: UsedTables = {};
  const conditionQueries: string[] = [];
  const mainKeys = (await getPrimaryKeys(models, main.model)).map((primaryKey) => (`${main.model}.${primaryKey}`));
  const { sortQuery, sortUsedTables } = getSortQuery(mainKeys, sortParams, sorts);
  usedTables = { ...usedTables, ...sortUsedTables };
  if (search && searchValue) {
    const { searchQuery, searchUsedTables } = getSearchQueries(search);
    conditionQueries.push(searchQuery);
    queryValues = {
      ...queryValues,
      query_search: `%${searchValue}%`,
    };
    usedTables = { ...usedTables, ...searchUsedTables };
  }
  if (filter && filterValue && Object.keys(filterValue).length) {
    const { filterQueries, filterQueryValues, filterUsedTables } = getFilterQueries(filterValue, filter);
    conditionQueries.push(...filterQueries);
    queryValues = { ...queryValues, ...filterQueryValues };
    usedTables = { ...usedTables, ...filterUsedTables };
  }
  if (main.condition) {
    const { conditionQuery, conditionQueryValues } = await getConditionQuery(ctx, main.condition);
    conditionQueries.push(conditionQuery);
    queryValues = { ...queryValues, ...conditionQueryValues };
  }
  const { joinQuery, joinQueryValues, conditionQuery } = await getJoinQueries(ctx, main, usedTables, sorts, sortParams, showDeleted);
  if (conditionQuery) {
    conditionQueries.push(conditionQuery);
  }
  queryValues = { ...queryValues, ...joinQueryValues };
  let rawQuery = `From ${joinQuery}`;
  if (conditionQueries?.length) {
    rawQuery = `${rawQuery} WHERE ${conditionQueries.join(' AND ')}`;
  }
  let targetQuery = `
  SELECT ${mainKeys.join(',')}
  ${rawQuery}
  GROUP BY ${mainKeys.join(',')}
  ORDER BY ${sortQuery || `${mainKeys[0]} ASC`}`;
  if (withLimit) {
    targetQuery = `${targetQuery} LIMIT :query_limit OFFSET :query_offset`;
  }
  const targets = await sequelize.query(targetQuery, { replacements: queryValues, type: QueryTypes.SELECT });
  if (withCount && !(exportSetting && exportData)) {
    const countQuery = `SELECT COUNT (DISTINCT ${mainKeys.join(',')}) as count ${rawQuery}`;
    const count = await sequelize.query(countQuery, { replacements: queryValues, type: QueryTypes.SELECT });
    return { targets, ...count?.length && count[0] };
  }
  return { targets };
};
