import { Schema } from 'joi';
import { parse } from 'json2csv';
import { Transaction, FindAttributeOptions, Association } from 'sequelize/types';
import { HttpContext } from '@http/interface';
import requestValidator from '@actions/list/request-validator';
import queryGenerator from '@actions/list/query-generator';
import sequelizeContractor from '@actions/list/sequelize-contractor';
import Jobs from '@job-queue/job-define';

export interface Condition {
  query: string,
  // eslint-disable-next-line no-unused-vars
  getQueryValues?: (ctx: HttpContext) => { [key: string]: any };
  tableNameFormat?: 'singular' | 'plural',
}

export interface Action {
  action: '=' | '<=>' | '<>' | '!=' | '>' | '>=' | '<' | '<=';
  value: string;
}

export interface Filter {
  [key: string]: {
    validator: Schema;
    mapper: string;
    action ?: Action['action'];
    // eslint-disable-next-line no-unused-vars
    actions ?: (validatorValue: any) => Action[];
  }
}

export interface TranslationSetting {
  presetLanguage ?: boolean;
  required ?: boolean;
  sourceKey ?: string;
  condition?: Condition;
}

export interface Join {
  model: string;
  showDeletedEnable ?: boolean;
  as?: string;
  scope?: string;
  condition?: Condition;
  joins?: Join[];
  on: { left: string; right: string };
  required?: boolean;
  withTranslation?: TranslationSetting;
  attributes?: FindAttributeOptions;
    // eslint-disable-next-line no-unused-vars
  association ?: (ctx: HttpContext) => Association;
}

export interface MainModel {
  model: string;
  showDeletedEnable ?: boolean;
  as?: string;
  scope?: string;
  condition?: Condition;
  joins?: Join[];
  withTranslation?: TranslationSetting;
  attributes?: FindAttributeOptions;
}

export interface Option {
  main: MainModel;
  transaction ?: Transaction;
  sorts ?: {
    [key: string]: string;
  };
  search ?: string[];
  filter?: Filter;
  withCount ?: boolean;
  extra ?: { [key: string]: Schema }
  // eslint-disable-next-line no-unused-vars
  afterQuery?: (ctx: HttpContext, results: any) => any;
  exportSetting?: {
    type: 'email';
    optionId: string;
    // eslint-disable-next-line no-unused-vars
    sendTo: (ctx: HttpContext) => Promise<string> | string;
    fileName?: string;
    // eslint-disable-next-line no-unused-vars
    dataReconstruct?: (ctx: HttpContext, results: any[]) => any[];
  } | {
    type: 'download';
    fileName ?: string;
    // eslint-disable-next-line no-unused-vars
    dataReconstruct?: (ctx: HttpContext, results: any[]) => any[];
  };
}

export const getResult = async (ctx: HttpContext, option: Option, query: any) => {
  const { afterQuery, withCount = true, exportSetting } = option;
  const { exportData } = query;
  const { targets, count } = await queryGenerator(ctx, option, query);
  const rows = (targets?.length && await sequelizeContractor(ctx, option, query, targets)) || [];
  let result = withCount && !(exportSetting && exportData) ? { rows, count: count || 0 } : rows;
  if (afterQuery) {
    result = await afterQuery(ctx, result);
  }
  return result;
};

export const getList = async (ctx: HttpContext, option: Option) => {
  const { languageId, currentSession, currentUser } = ctx;
  const { exportSetting } = option;
  const query = await requestValidator(ctx, option);
  const { exportData } = query;
  let response;
  if (exportSetting && exportData) {
    const { fileName = 'export', dataReconstruct } = exportSetting;
    if (exportSetting.type === 'download') {
      let results = await getResult(ctx, option, query);
      if (dataReconstruct && Array.isArray(results)) {
        results = dataReconstruct(ctx, results);
      }
      let csv;
      if (Array.isArray(results) && results.length) {
        const example = results[0];
        const fields = Object.keys(example);
        try {
          csv = parse(results, { fields });
        } catch (err) {
          console.error('Download csv error:', err);
        }
      }
      response = { type: 'download', file: csv, fileName } || [];
    } else if (exportSetting.type === 'email') {
      const target = await exportSetting.sendTo(ctx);
      const queue = Jobs.get_list_export;
      queue.add({
        optionId: exportSetting.optionId,
        query,
        contextOption: {
          languageId,
          currentUser,
          currentSession,
        },
        target,
        fileName,
      });
      response = { type: 'email', target };
      ctx.body = response;
    }
  } else {
    response = await getResult(ctx, option, query);
  }
  return response;
};

export default {
  getList,
};
