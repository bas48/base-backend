import Joi, { Schema } from 'joi';
import apiValidator from '@http/middleware/api-validator';
import { HttpContext } from '@http/interface';
import { Option } from '@actions/list';
import { ExportNotSupport } from '@utils/errors';

export default async (ctx: HttpContext, option: Option) => {
  const { sorts, filter, extra, exportSetting } = option;
  let schema: {
    page: Schema;
    limit: Schema;
    orderBy: Schema;
    direction: Schema;
    sort: Schema;
    search: Schema;
    filter: Schema;
    languageId: Schema;
    showDeleted: Schema;
    exportData: Schema;
    [key: string]: Schema
  } = {
    page: Joi.number().min(1).default(1),
    limit: Joi.number().min(1),
    orderBy: Joi.string(),
    direction: Joi.string().valid('ASC', 'DESC'),
    sort: Joi.array().items(Joi.object({
      orderBy: Joi.string().required(),
      direction: Joi.string().valid('ASC', 'DESC').default('ASC').required(),
    })),
    search: Joi.string(),
    filter: Joi.object(),
    languageId: Joi.string(),
    showDeleted: Joi.boolean(),
    exportData: Joi.boolean(),
  };
  if (sorts && Object.keys(sorts).length) {
    const sortKeys = Object.keys(sorts);
    schema.orderBy = Joi.string().valid(...sortKeys);
    schema.direction = Joi.string().valid('ASC', 'DESC');
    schema.sort = Joi.array().items(Joi.object({
      orderBy: Joi.string().valid(...sortKeys).required(),
      direction: Joi.string().valid('ASC', 'DESC').required(),
    }));
  }
  if (filter) {
    const validators: {
      [key: string]: Schema;
    } = {};
    for (const key of Object.keys(filter)) {
      validators[key] = filter[key].validator;
    }
    schema.filter = Joi.object().keys(validators);
  }
  if (extra) {
    schema = { ...schema, ...extra };
  }
  await apiValidator.query(schema)(ctx, async () => {});
  const { validatedData } = ctx;
  const query = validatedData?.query;
  if (query.exportData && !exportSetting) {
    throw new ExportNotSupport();
  }
  if (query.orderBy && query.direction) {
    const { sort = [] } = query;
    query.sort = [{
      orderBy: query.orderBy,
      direction: query.direction,
    }, ...sort];
  }
  return query;
};
