import moment from 'moment';
import { sendEmail, EmailOptions, Attachment } from '@lib/email';
import { Context } from '@utils/interface';

const templateVariables = {
  moment,
  env: process.env,
};

interface EmailData {
   subject: string;
   content: string;
}

export default {
  async sendEmail(ctx: Context, receivers: string[], data: EmailData, templateId: string = 'default', attachments: Attachment[] = []) {
    return this.sendEmailByRawOptions(ctx, {
      template: templateId,
      message: {
        to: receivers,
        attachments,
      },
      locals: data,
    });
  },

  async sendEmailByRawOptions(ctx: Context, options: EmailOptions) {
    options = { ...options };
    options.template = options.template || 'default';
    options.locals = {
      ...options.locals,
      ...templateVariables,
    };
    if (!options.locals.t) {
      options.locals.t = ctx.translate;
    }
    return sendEmail(options);
  },
};
