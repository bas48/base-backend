import { Transaction } from 'sequelize';
import { NotFoundError } from '@utils/errors';
import { Context } from '@utils/interface';
import IdManagerAction from '@actions/IdManager';
import User, { UserInstance } from '@database/models/User';
import encryptAction from '@actions/encrypt';

interface UserData {
  name: string;
  icon: string;
  email ?: string;
  phone ?: string;
  password ?: string ;
}

interface UserUpdateData {
  name?: string;
  icon?: string;
  email?: string;
  phone?: string;
  password?: string;
  banned?: boolean;
}

export default {
  async createUser(data: UserData, transaction: Transaction): Promise<UserInstance> {
    const { name, icon, email, phone, password } = data;
    const id = await IdManagerAction.getNewId();
    const user = await User.create({
      id,
      name,
      icon,
      email,
      phone,
      password: password && await encryptAction.encrypt(password),
    }, { transaction });
    return user;
  },

  async updateUser(ctx: Context, userId: string, data: UserUpdateData, transaction: Transaction): Promise<UserInstance> {
    const { translate } = ctx;
    const { password } = data;
    const UserAll = User.scope('all');
    const user = await UserAll.findOne({
      where: {
        id: userId,
      },
      transaction,
    });
    if (!user) {
      throw new NotFoundError({ resource: translate('user') });
    }
    if (password) {
      Object.assign(user, { ...data, password: await encryptAction.encrypt(password) });
    } else {
      Object.assign(user, data);
    }
    await user.save({ transaction });
    return user;
  },
};
