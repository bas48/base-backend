import { Op, FindOptions, Transaction } from 'sequelize';
import { RemoteCache } from '@utils/cache';
import { NotFoundError } from '@utils/errors';
import { Context } from '@utils/interface';
import UserRole, { UserRoleInstance } from '@database/models/UserRole';
import UserPermission from '@database/models/UserPermission';
import RolePermission from '@database/models/RolePermission';
import Resource from '@database/models/Resource';

type Right = 'read' | 'write' | 'banned';

interface PermissionRecord {
  permissionId: string,
  right: Right,
}

interface PermissionObject {
  [permissionId: string]: Right,
}

export default {
  countRight(right: Right): number {
    switch (right) {
      case 'read':
        return 1;
      case 'write':
        return 2;
      case 'banned':
        return 3;
      default:
        return 0;
    }
  },

  arrayToObject(array: {permissionId: string, right: Right }[]): PermissionObject {
    const object: PermissionObject = {};
    for (const item of array) {
      if (!object[item.permissionId] || this.countRight(item.right) > this.countRight(object[item.permissionId])) {
        object[item.permissionId] = item.right;
      }
    }
    return object;
  },

  async getUserAllPermission(ctx: Context, userId: string, transaction?: Transaction): Promise<PermissionObject> {
    const userPermissions = await this.getUserPermissions(ctx, userId, transaction);

    let userRoles = await RemoteCache.get('user-role', userId);
    if (!userRoles) {
      userRoles = await UserRole.findAll({
        transaction,
        where: {
          userId,
        },
      });
      userRoles = userRoles.map((userRole: UserRoleInstance) => userRole.roleId);
      await RemoteCache.set('user-role', userId, userRoles, { expire: 60 });
    }
    if (userRoles?.length) {
      const rolePermissions = await this.getRolePermissions(ctx, userRoles, transaction);
      let rolesPermissions: PermissionRecord[] = [];
      for (const roleId of Object.keys(rolePermissions)) {
        rolesPermissions = [...rolesPermissions, ...rolePermissions[roleId]];
      }
      return this.arrayToObject([...userPermissions, ...rolesPermissions]);
    }
    return this.arrayToObject(userPermissions);
  },

  async getRolePermissions(ctx: Context, roleIds: string[], transaction?: Transaction): Promise<{
    [key: string]: PermissionRecord[];
  }> {
    const cachedRolePermissions = await RemoteCache.mget('role-permissions', roleIds);
    const rolePermissions: {
      [roleId: string]: {
        permissionId: string;
        right: 'read'|'write';
      }[];
    } = {};
    const remainIds = [];
    for (let i = 0; i < cachedRolePermissions.length; i += 1) {
      const roleId = roleIds[i];
      if (cachedRolePermissions[i]) {
        rolePermissions[roleId] = cachedRolePermissions[i];
      } else {
        remainIds.push(roleId);
      }
    }
    if (remainIds.length) {
      const setting: FindOptions = {
        where: {
          roleId: {
            [Op.in]: remainIds,
          },
        },
        transaction,
      };
      const permissions = await RolePermission.findAll(setting);
      for (const roleId of remainIds) {
        if (!rolePermissions[roleId]) {
          rolePermissions[roleId] = [];
        }
        for (const permission of permissions) {
          if (String(permission.roleId) === String(roleId)) {
            rolePermissions[roleId].push({
              permissionId: permission.permissionId,
              right: permission.right,
            });
          }
        }
      }
      RemoteCache.mset('role-permissions', rolePermissions);
    }
    return rolePermissions;
  },

  async getUserPermissions(ctx: Context, userId: string, transaction?: Transaction): Promise<{
    permissionId: string,
    right: Right,
  }[]> {
    let userPermissions = await RemoteCache.get('user-permissions', userId);
    if (!userPermissions) {
      const setting: FindOptions = {
        where: {
          userId,
        },
        transaction,
      };
      userPermissions = await UserPermission.findAll(setting);
      userPermissions = userPermissions.map((userPermission: PermissionRecord) => ({
        permissionId: userPermission.permissionId,
        right: userPermission.right,
      }));
      await RemoteCache.set('user-permissions', userId, userPermissions);
    }
    return userPermissions;
  },

  async getUsersPermissions(ctx: Context, userIds: string[]): Promise<{
    [userId: string]: {
        permissionId: string;
        right: 'read' | 'write' | 'banned';
    }[]
  }> {
    const cachedUserPermissions = await RemoteCache.mget('user-permissions', userIds);
    const userPermissions: {
      [userId: string]: {
        permissionId: string;
        right: 'read' | 'write' | 'banned';
      }[],
    } = {};
    const remainIds = [];
    for (let i = 0; i < cachedUserPermissions.length; i += 1) {
      const userId = userIds[i];
      if (cachedUserPermissions[i]) {
        userPermissions[userId] = cachedUserPermissions[i];
      } else {
        remainIds.push(userId);
      }
    }
    if (remainIds.length) {
      const permissions = await UserPermission.findAll({
        where: {
          userId: {
            [Op.in]: remainIds,
          },
        },
      });
      for (const userId of remainIds) {
        if (!userPermissions[userId]) {
          userPermissions[userId] = [];
        }
        for (const permission of permissions) {
          if (permission.userId === userId) {
            userPermissions[userId].push({
              permissionId: permission.permissionId,
              right: permission.right,
            });
          }
        }
      }
      RemoteCache.mset('user-permissions', userPermissions);
    }
    return userPermissions;
  },

  async getResourcePermissions(ctx: Context, path: string, method: string): Promise<{
    [key: string]: Right;
  }[]> {
    let resourcePermissions = await RemoteCache.get('resource-permission', `${method}-${path}`);
    if (!resourcePermissions) {
      const resource = await Resource.findOne({
        where: {
          path,
          method,
          active: true,
        },
      });
      if (!resource) {
        const { translate } = ctx;
        throw new NotFoundError({ type: 'resource', resource: translate('resource') });
      }
      resourcePermissions = resource.permissions;
      await RemoteCache.set('resource-permission', `${method}-${path}`, resourcePermissions || [], { expire: 1800 });
    }
    return resourcePermissions;
  },

  removeOverSessionPermission(fullSessionPermission: PermissionRecord[], userPermissions: PermissionObject): PermissionObject {
    const sessionPermissions = this.arrayToObject(fullSessionPermission);
    for (const permissionId of Object.keys(sessionPermissions)) {
      if (!userPermissions[permissionId]) {
        delete sessionPermissions[permissionId];
      } else if (this.countRight(sessionPermissions[permissionId]) < this.countRight(userPermissions[permissionId])) {
        sessionPermissions[permissionId] = userPermissions[permissionId];
      }
    }
    return sessionPermissions;
  },

  comparePermission(requiredPermission: { [key: string]: Right }, userPermissions: PermissionObject): {
    success: boolean,
    needPermission?: { [permissionId: string]: Right },
  } {
    const needPermission: {
      [permissionId: string]: Right
    } = {};
    for (const permissionId of Object.keys(requiredPermission)) {
      if (!userPermissions[permissionId] || userPermissions[permissionId] === 'banned' || this.countRight(userPermissions[permissionId]) < this.countRight(requiredPermission[permissionId])) {
        needPermission[permissionId] = requiredPermission[permissionId];
      }
    }
    if (Object.keys(needPermission).length) {
      return {
        success: false,
        needPermission,
      };
    }
    return {
      success: true,
    };
  },
};
