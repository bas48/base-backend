import moment from 'moment';
import { RemoteCache } from '@utils/cache';
import { AccessTokenExpired, UserAlreadyBanned, NotFoundError } from '@utils/errors';
import { Context } from '@utils/interface';
import User from '@database/models/User';
import SessionPermission from '@database/models/SessionPermission';
import Session, { SessionInstance } from '@database/models/Session';
import { v4 } from 'uuid';
import { Transaction } from 'sequelize/types';

const { SESSION_EXPIRE_TIME } = process.env;

const sessionExpireTime = Number(SESSION_EXPIRE_TIME) || 30; // Minute

export default {
  async getSessionByAccessToken(ctx: Context, accessToken: string): Promise<SessionInstance> {
    let session = await RemoteCache.get('sessions', accessToken);
    if (!session) {
      session = await Session.findOne({
        include: [
          {
            model: User,
            required: true,
          },
          {
            model: SessionPermission,
            required: false,
          },
        ],
        where: {
          accessToken,
        },
      });
      const { translate } = ctx;
      if (!session) {
        throw new NotFoundError({ type: 'session', resource: translate('session') });
      }
      await RemoteCache.set('sessions', accessToken, session.toJSON(), { expire: sessionExpireTime * 60 });
    }
    if (session && moment().isAfter(moment(session.expiresAt))) {
      throw new AccessTokenExpired();
    }
    if (session.user.banned) {
      throw new UserAlreadyBanned();
    }
    return session;
  },

  async clearUserSessions(ctx: Context, userId: string, type: string, transaction: Transaction): Promise<void> {
    const sessions = await Session.findAll({
      where: {
        userId,
        type,
      },
      transaction,
    });
    if (sessions.length) {
      await RemoteCache.del('sessions', sessions.map((s) => s.accessToken));
    }
    await Session.destroy({
      where: {
        userId,
        type,
      },
      transaction,
    });
  },

  async refreshToken(ctx: Context, refreshToken: string, transaction: Transaction): Promise<SessionInstance> {
    const { translate } = ctx;
    const session = await Session.findOne({
      where: {
        refreshToken,
      },
      // throwError: true,
      transaction,
    });
    if (!session) {
      throw new NotFoundError({ type: 'session', resource: translate('session') });
    }
    await RemoteCache.del('sessions', session.accessToken);
    session.accessToken = v4();
    session.expiresAt = new Date(Date.now() + (sessionExpireTime * 60 * 1000));
    await session.save({ transaction });
    return session;
  },
};
