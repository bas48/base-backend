import fs from 'fs-extra';
import axios from 'axios';
import fileType from 'file-type';
import { v4 } from 'uuid';
import path from 'path';
import sharp, { ResizeOptions } from 'sharp';
import { IncomingMessage } from 'http';
import multer from '@koa/multer';
import { InvalidFileType, NotFoundError, FileTransferError, FileCannotRemove } from '@utils/errors';
import { Context } from '@utils/interface';

export interface ResizeOpt { width?: number, height?: number, option?: ResizeOptions }

export default {
  getFilePathDetail(filePath: string): {
    fileName: string,
    directory: string,
    fullPath: string,
  } {
    const fileName = filePath.replace(/^.*[\\/]/, '');
    const directory = filePath.replace(/^\//, '').match(/(.*)[/\\]/)[1] || '';
    const fullPath = path.resolve(directory, fileName);
    return { fileName, directory, fullPath };
  },
  async isFileExists(ctx: Context, filePath: string) {
    const { translate } = ctx;
    try {
      await fs.stat(filePath);
    } catch (error) {
      throw new NotFoundError({ type: 'file', resource: translate('file'), path: filePath });
    }
  },
  async getTransferPath(ctx: Context, originalDisplay: string, targetDirectory: string): Promise<{
    original: { display: string, directory: string, fullPath: string, fileName: String },
    target: { display: string, directory: string, fullPath: string, fileName: String },
  }> {
    const { fileName, directory: originalDirectory, fullPath: originalFullPath } = this.getFilePathDetail(originalDisplay);
    const original = {
      display: originalDisplay.replace(/^\/?/, '/'),
      directory: originalDirectory,
      fullPath: originalFullPath,
      fileName,
    };
    const target = {
      display: `/${targetDirectory}/${fileName}`,
      directory: targetDirectory,
      fullPath: path.resolve(targetDirectory, fileName),
      fileName,
    };
    await this.isFileExists(ctx, original.fullPath);
    return {
      original,
      target,
    };
  },
  async transferFile(ctx: Context, originalDisplay: string, targetDirectory: string, action: 'move' | 'copy' = 'move', resize?: ResizeOpt): Promise<{
    display: string,
    directory: string,
    fullPath: string,
    fileName: string,
  }> {
    const { original, target } = await this.getTransferPath(ctx, originalDisplay, targetDirectory);
    try {
      if (resize) {
        const { width, height, option } = resize;
        await this.imageResize(original.fullPath, target.fullPath, { width, height }, option);
      } else {
        switch (action) {
          case 'move':
            await fs.move(original.fullPath, target.fullPath);
            break;
          case 'copy':
            await fs.copy(original.fullPath, target.fullPath);
            break;
          default:
            break;
        }
      }
    } catch (error) {
      console.log('Error:', error);
      if (error) {
        throw new FileTransferError({ path: error.path });
      }
    }
    return target;
  },
  loadFolderFiles(folderPath: string, exceptFiles?: string[], loadContent: boolean = true): string[] | any {
    const files = fs.readdirSync(folderPath)
      .filter((file) => (file.indexOf('.') !== 0) && (file.slice(-3) === '.ts' || file.slice(-3) === '.js') && (!exceptFiles || exceptFiles.indexOf(file) < 0));
    if (loadContent) {
      const loadedFiles: any = {};
      for (const file of files) {
        // eslint-disable-next-line global-require, import/no-dynamic-require
        const localFile = require(path.join(folderPath, file));
        loadedFiles[file.split('.')[0]] = localFile.default;
      }
      return loadedFiles;
    }
    return files;
  },

  // size unit MB
  upload(filePath: string, type: string, maxSize ?: number): multer.Instance {
    const fileFilter = (req: IncomingMessage, file: multer.File, cb: Function) => {
      const { mimetype } = file;
      if (type) {
        const fileMainType = type.split('/')[0];
        const mimeMainType = mimetype.split('/')[0];
        const checkAll = !!type.includes('/*');
        if ((checkAll && fileMainType !== mimeMainType) || (!checkAll && type !== mimetype)) {
          cb(new InvalidFileType());
        }
      }
      cb(null, true);
    };
    const storage = multer.diskStorage({
      destination(req: IncomingMessage, file: multer.File, cb: Function) {
        cb(null, filePath);
      },
      filename(req: IncomingMessage, file: multer.File, cb: Function) {
        cb(null, `${v4()}.${file.mimetype.split('/')[1]}`);
      },
    });
    const limits = (maxSize && { fileSize: maxSize * 1024 * 1024 }) || {};
    return multer({ storage, fileFilter, limits });
  },

  async removeFile(ctx: Context, filePath: string): Promise<void> {
    const fileName = filePath.replace(/^.*[\\/]/, '');
    const fileDirectory = filePath.replace(/^\//, '').match(/(.*)[/\\]/)[1] || '';
    const fullPath = path.resolve(fileDirectory, fileName);
    await this.isFileExists(ctx, fullPath);
    const removePaths = [fullPath];
    await this.removeStaticFiles(ctx, removePaths);
  },

  async removeStaticFiles(ctx: Context, removePaths: string[]): Promise<void> {
    for (const removePath of removePaths) {
      try {
        await fs.remove(removePath);
      } catch (err) {
        throw new FileCannotRemove({ path: removePath });
      }
    }
  },

  async download(fileUrl: string, targetFolder: string) {
    const file = (await axios.get(fileUrl, {
      responseType: 'arraybuffer',
    })).data;
    const type = await fileType.fromBuffer(file);
    const fileName = `${v4()}.${type?.ext}`;
    const filePath = `/${path.resolve(targetFolder, fileName)}`;
    await fs.writeFile(filePath, file);
    return filePath;
  },

  async imageResize(filePath: string, targetPath: string, size: { width?: number, height?: number }, option?: ResizeOptions) {
    const { width = null, height = null } = size;
    const { mime } = await fileType.fromFile(filePath);
    const {
      fit = 'contain',
      position = 'center',
      background = (mime === 'image/png' && { r: 255, g: 255, b: 255, alpha: 0 }) || { r: 255, g: 255, b: 255 },
    } = option || {};
    await sharp(filePath).resize(width, height, {
      ...option,
      fit,
      position,
      background,
    }).toFile(targetPath);
  },
};
