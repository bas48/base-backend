import { NotFoundError } from '@utils/errors';

export default {
  throwNotFoundError(instances: any | any[], errorMessage: string) {
    if (!instances || (Array.isArray(instances) && !instances.length)) {
      throw new NotFoundError({ resource: errorMessage });
    }
  },
};
