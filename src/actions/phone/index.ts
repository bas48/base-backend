import moment from 'moment';
import smsLib from '@lib/sms';
import sequelize from '@database';

export default {
  async sendSms(phone: string, message: string) {
    let result: any = { success: true, provider: 'Test' };
    if (process.env.NODE_ENV === 'production') {
      result = await smsLib.sendSms(phone, message, {});
    }
    if (result.success) {
      await this.incrementSMSCount(result.provider);
    }
    return result;
  },

  async incrementSMSCount(provider: string) {
    await sequelize.query(`
      INSERT INTO \`sms_counts\` (provider, count, date)
      VALUES ('${provider}', 1, '${moment().startOf('day').format('YYYY-MM-DD')}')
      ON DUPLICATE KEY
      UPDATE count = count + 1
    `);
  },
};
