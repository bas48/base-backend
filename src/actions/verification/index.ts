import moment from 'moment';
import { ConflictError, NotFoundError, PermissionDenied, VerificationTokenExpired, RequestTooMuch } from '@utils/errors';
import { HttpContext } from '@http/interface';
import { Transaction } from 'sequelize/types';

const { VERIFICATION_CODE_EXPIRE_TIME, RESEND_LIMIT } = process.env;

export default {
  getAuthCode() {
    return String(Math.floor(100000 + (Math.random() * 900000)));
  },

  async checkVerificationRecord(ctx: HttpContext, token: string, reference: string, transaction: any) {
    const { models, translate } = ctx;
    const { Verification } = models;
    const verification = await Verification.findOne({
      where: {
        token,
      },
      transaction,
    });
    if (!verification) {
      throw new NotFoundError({ resource: translate('verification') });
    }
    if (!verification.isVerified) {
      throw new ConflictError({ reason: translate('not_verified') });
    }
    if (verification.type === 'twoFA') {
      const { currentUser } = ctx;
      if (currentUser && verification.userId !== currentUser.id) {
        throw new PermissionDenied({ reason: translate('not_related_user') });
      }
    } else if (verification.reference !== reference) {
      throw new PermissionDenied({ reason: translate('not_related_verification') });
    }
    if (moment().isAfter(moment(verification.expiresAt))) {
      throw new VerificationTokenExpired();
    }
    await verification.destroy({ transaction });
  },

  async createVerification(ctx: HttpContext, type: string, reference: string, userId: string | null, transaction: Transaction) {
    const { models, translate } = ctx;
    const { Verification } = models;
    let verification = await Verification.findOne({
      where: {
        type,
        reference,
      },
      transaction,
    });
    if (verification) {
      const resendTime = moment(verification.expiresAt).subtract(VERIFICATION_CODE_EXPIRE_TIME, 'minutes').add(RESEND_LIMIT, 'minutes');
      if (!verification.isVerified && moment().isBefore(resendTime)) {
        throw new RequestTooMuch({ type: translate(type) });
      }
      verification.isVerified = false;
      verification.authCode = this.getAuthCode();
      verification.expiresAt = moment().add(VERIFICATION_CODE_EXPIRE_TIME, 'minutes').toDate();
      await verification.save({ transaction });
    } else {
      verification = await Verification.create({
        type,
        reference,
        userId: userId || null,
        authCode: this.getAuthCode(),
        expiresAt: moment().add(VERIFICATION_CODE_EXPIRE_TIME, 'minutes').toDate(),
      }, { transaction });
    }
    return verification;
  },

  async getVerification(ctx: HttpContext, type: string, token: string) {
    const { models, translate } = ctx;
    const { Verification } = models;
    const verification = await Verification.findOne({
      where: {
        type,
        token,
        isVerified: true,
      },
    });
    if (!verification) {
      throw new NotFoundError({ type: 'verification', resource: translate('verification') });
    }
    if (moment().isAfter(moment(verification.expiresAt))) {
      throw new VerificationTokenExpired();
    }
    return verification;
  },
};
