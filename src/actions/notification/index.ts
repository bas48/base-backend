import moment from 'moment';
import { JobOptions } from 'bull';
import { JobAlreadyExecuted } from '@utils/errors';
import Jobs from '@job-queue/job-define';

export interface NotificationJobOptions {
  id: number;
  pushedAt: Date,
}

export interface NotificationTarget {
  type: string;
  reference: JSON,
}

export interface NotificationContent {
  notificationId: number;
  type: string;
  messageId: number;
  messageParams: string;
  customContent ?: string;
}

export default {
  async upsertJob(option: NotificationJobOptions, targets: NotificationTarget[], contents: NotificationContent[]) {
    const queue = Jobs.notification;
    const { id, pushedAt } = option;
    const job = await queue.getJob(id);
    console.log('id:', id, job);
    if (job?.finishedOn) {
      throw new JobAlreadyExecuted({ jobId: id });
    }

    // Job data setting
    const opts: JobOptions = { jobId: id };
    const delay = moment.duration(moment(pushedAt).diff(moment())).asMilliseconds();
    if (delay > 0) {
      // opts.delay = delay;
    }
    const data = {
      contents: contents.map((content) => ({
        type: content.type,
        messageId: content.messageId,
        messageParams: content.messageParams,
        customContent: content.customContent,
      })),
      targets: targets.map((target) => ({
        type: target.type,
        reference: target.reference,
      })),
    };
    if (job) {
      await job.remove();
    }
    queue.add(data, opts);
    return { data, opts };
  },
};
